from unittest import TestCase
import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.intersector_class as IntersectorLibrary



class TestIntersection(TestCase):
    def test_Intersection_method(self):
                  #PRIMA INTERSEZIONE : I DUE SEGMENTI SI INTERSECANO SENZA PROLUNGAMENTO
        try:
            inizioLato: PointsLibrary.Points = PointsLibrary.Points(1.0, 1.0)
            fineLato : PointsLibrary.Points = PointsLibrary.Points(2.5, 1.5)
            inizioSegmento: PointsLibrary.Points = PointsLibrary.Points(0.7, 2.0)
            fineSegmento: PointsLibrary.Points = PointsLibrary.Points(3.5, 1.5)
            lato: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato, fineLato)
            segmento: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento, fineSegmento)

            method1 = IntersectorLibrary.Intersector.Intersection(lato, segmento)

            self.assertEqual(method1, False)
        except Exception as ex:
            self.fail()
                   #SECONDA INTERSEZIONE : I DUE SEGMENTI SI INTERSECANO CON PROLUNGAMENTO
        try:
            inizioLato2 : PointsLibrary.Points = PointsLibrary.Points(0.5, 6.0)
            fineLato2: PointsLibrary.Points = PointsLibrary.Points(4.5, 0.1)
            inizioSegmento2: PointsLibrary.Points = PointsLibrary.Points(-2.0, 0.0)
            fineSegmento2: PointsLibrary.Points = PointsLibrary.Points(0.1, 0.25)
            lato2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato2, fineLato2)
            segmento2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento2, fineSegmento2)
            method2 = IntersectorLibrary.Intersector.Intersection(lato2, segmento2)
            self.assertEqual(method2, True)

        except Exception as ex:
            self.fail()
                      # TERZA (NON) INTERSEZIONE: I DUE SEGMENTI SONO PARALLELI DISGIUNTI
        try:
            inizioLato3: PointsLibrary.Points = PointsLibrary.Points(0.5, 6.0)
            fineLato3: PointsLibrary.Points = PointsLibrary.Points(4.5, 6.0)
            inizioSegmento3: PointsLibrary.Points = PointsLibrary.Points(0.5, 8.0)
            fineSegmento3: PointsLibrary.Points = PointsLibrary.Points(5.2, 8.0)
            lato3: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato3, fineLato3)
            segmento3: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento3, fineSegmento3)
            method3 = IntersectorLibrary.Intersector.Intersection(lato3, segmento3)
            self.assertEqual(method3, False)

        except Exception as ex:
            self.fail()
                      #QUARTA (NON) INTERSEZIONE: I DUE SEGMENTI SONO PARALLELI COINCIDENTI
        try:
            inizioLato4: PointsLibrary.Points = PointsLibrary.Points(0.5, 6.0)
            fineLato4: PointsLibrary.Points = PointsLibrary.Points(4.5, 6.0)
            inizioSegmento4: PointsLibrary.Points = PointsLibrary.Points(0.1, 6.0)
            fineSegmento4: PointsLibrary.Points = PointsLibrary.Points(5.5, 6.0)
            lato4: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato4, fineLato4)
            segmento4: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento4, fineSegmento4)
            method4 = IntersectorLibrary.Intersector.Intersection(lato4, segmento4)
            self.assertEqual(method4, False)

        except Exception as ex:
            self.fail()

    def test_PointIntersection_method(self):

        try:
            inizioLato5: PointsLibrary.Points = PointsLibrary.Points(-3.0, 1.0)
            fineLato5: PointsLibrary.Points = PointsLibrary.Points(0.0, 0.0)
            inizioSegmento5: PointsLibrary.Points = PointsLibrary.Points(-1.5, -1.5)
            fineSegmento5: PointsLibrary.Points = PointsLibrary.Points(1.0, 1.0)

            lato5: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato5, fineLato5)
            segmento5: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento5, fineSegmento5)

            punto_trovato5: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato5, segmento5)

            self.assertEqual((punto_trovato5.X - lato5.Fine.X) < 1e-6, True)
            self.assertEqual((punto_trovato5.Y - lato5.Fine.Y) < 1e-6, True)

        except Exception as ex:
            self.fail()


                          # PUNTO D'INIZIO CON PROLUNGAMENTO
        try:
            inizioLato: PointsLibrary.Points = PointsLibrary.Points(0.0, 1.0)
            fineLato: PointsLibrary.Points = PointsLibrary.Points(3.0, 0.0)
            inizioSegmento: PointsLibrary.Points = PointsLibrary.Points(-2.0, -1.0)
            fineSegmento: PointsLibrary.Points = PointsLibrary.Points(-1.0, 0.0)

            lato: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato, fineLato)
            segmento: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento, fineSegmento)

            punto_trovato: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato, segmento)

            self.assertEqual((punto_trovato.X - lato.Inizio.X) < 1e-6, True)
            self.assertEqual((punto_trovato.Y - lato.Inizio.Y) < 1e-6, True)

        except Exception as ex:
            self.fail()

                     # PUNTO FINALE CON PROLUNGAMENTO
        try:
            inizioLato2: PointsLibrary.Points = PointsLibrary.Points(3.0, 0.0)
            fineLato2: PointsLibrary.Points = PointsLibrary.Points(2.0, 4.0)
            inizioSegmento2: PointsLibrary.Points = PointsLibrary.Points(-1.0, -2.0)
            fineSegmento2: PointsLibrary.Points = PointsLibrary.Points(0.0, 0.0)

            lato2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato2, fineLato2)
            segmento2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento2, fineSegmento2)

            punto_trovato2: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato2, segmento2)
            self.assertEqual((punto_trovato2.X - lato2.Fine.X) < 1e-6, True)
            self.assertEqual((punto_trovato2.Y - lato2.Fine.Y) < 1e-6, True)

        except Exception as ex:
            self.fail()

              # PUNTO  GENERICO

        try:
            inizioLato3: PointsLibrary.Points = PointsLibrary.Points(1.0, 0.0)
            fineLato3: PointsLibrary.Points = PointsLibrary.Points(0.0, 1.0)
            inizioSegmento3: PointsLibrary.Points = PointsLibrary.Points(-1.0, -1.0)
            fineSegmento3: PointsLibrary.Points = PointsLibrary.Points(3.0, 3.0)

            lato3: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioLato3, fineLato3)
            segmento3: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento3, fineSegmento3)

            punto_trovato3: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato3, segmento3)
            self.assertEqual((punto_trovato3.Y - 0.5) < 1e-6, True)
            self.assertEqual((punto_trovato3.X - 0.5) < 1e-6, True)

        except Exception as ex:
            self.fail()

from unittest import TestCase

import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.intersector_class as IntersectorLibrary
import src.new_class as NewLibrary

class TestNew(TestCase):
    def test_NewPoints_method(self):

                # 1 TEST POLIGONO VICINI
        points = []
        new_points = []
        num_vertici : int = 6

        points.append(PointsLibrary.Points(1.5, 1.0))
        points.append(PointsLibrary.Points(5.6, 1.5))
        points.append(PointsLibrary.Points(5.5, 4.8))
        points.append(PointsLibrary.Points(4.0, 6.2))
        points.append(PointsLibrary.Points(3.2, 4.2))
        points.append(PointsLibrary.Points(1.0, 4.0))

        points[0] = (PointsLibrary.Points(1.5, 1.0))
        points[1] = (PointsLibrary.Points(5.6, 1.5))
        points[2] = (PointsLibrary.Points(5.5, 4.8))
        points[3] = (PointsLibrary.Points(4.0, 6.2))
        points[4] = (PointsLibrary.Points(3.2, 4.2))
        points[5] = (PointsLibrary.Points(1.0, 4.0))

        self.assertEqual(len(points), 6)
        self.assertEqual(points[0].Y, 1.0)
        self.assertEqual(points[5].X, 1.0)

        inizio: PointsLibrary.Points = PointsLibrary.Points(2.0, 3.7)
        fine: PointsLibrary.Points = PointsLibrary.Points(4.1, 5.9)
        segmento: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio, fine)

        new_points = NewLibrary.newItems.NewPoints(points, segmento)
        self.assertEqual(len(new_points), 12)

        for i in range(0, len(points) - 1):
            self.assertEqual(new_points[i].X, points[i].X)
            self.assertEqual(new_points[i].Y, points[i].Y)


        #creo i lati che vengono imtersecati (mi servono poi per testare i punti di intersezione
        lato2_3: SegmentLibrary.Segment = SegmentLibrary.Segment(points[2], points[3])
        lato3_4: SegmentLibrary.Segment = SegmentLibrary.Segment(points[3], points[4])
        lato4_5: SegmentLibrary.Segment = SegmentLibrary.Segment(points[4], points[5])
        lato5_0: SegmentLibrary.Segment = SegmentLibrary.Segment(points[5], points[0])

        punto6: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato2_3, segmento)
        punto7: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato3_4, segmento)
        punto8: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato4_5, segmento)
        punto9: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato5_0, segmento)

        self.assertEqual(new_points[6].X - punto6.X < 1e-6, True)
        self.assertEqual(new_points[6].Y - punto6.Y < 1e-6, True)
        self.assertEqual(new_points[7].X - punto7.X < 1e-6, True)
        self.assertEqual(new_points[7].Y - punto7.Y < 1e-6, True)
        self.assertEqual(new_points[8].X - punto8.X < 1e-6, True)
        self.assertEqual(new_points[8].Y - punto8.Y < 1e-6, True)
        self.assertEqual(new_points[9].X - punto9.X < 1e-6, True)
        self.assertEqual(new_points[9].Y - punto9.Y < 1e-6, True)

        # controllo punto iniziale e finale
        self.assertEqual(new_points[10].X, fine.X)
        self.assertEqual(new_points[10].Y, fine.Y)
        self.assertEqual(new_points[11].X, inizio.X)
        self.assertEqual(new_points[11].Y, inizio.Y)

        # TEST POLIGONO D'AURIA
        pointsComplesso = []
        newPointsComplesso = []
        numVerticiComplesso: int = 10

        pointsComplesso.append(PointsLibrary.Points(2.0, -2.0))
        pointsComplesso.append(PointsLibrary.Points(0.0, -1.0))
        pointsComplesso.append(PointsLibrary.Points(3.0, 1.0))
        pointsComplesso.append(PointsLibrary.Points(0.0, 2.0))
        pointsComplesso.append(PointsLibrary.Points(3.0, 2.0))
        pointsComplesso.append(PointsLibrary.Points(3.0, 3.0))
        pointsComplesso.append(PointsLibrary.Points(-1.0, 3.0))
        pointsComplesso.append(PointsLibrary.Points(-3.0, 1.0))
        pointsComplesso.append(PointsLibrary.Points(0.0, 0.0))
        pointsComplesso.append(PointsLibrary.Points(-3.0, -2.0))

        pointsComplesso[0] = (PointsLibrary.Points(2.0, -2.0))
        pointsComplesso[1] = (PointsLibrary.Points(0.0, -1.0))
        pointsComplesso[2] = (PointsLibrary.Points(3.0, 1.0))
        pointsComplesso[3] = (PointsLibrary.Points(0.0, 2.0))
        pointsComplesso[4] = (PointsLibrary.Points(3.0, 2.0))
        pointsComplesso[5] = (PointsLibrary.Points(3.0, 3.0))
        pointsComplesso[6] = (PointsLibrary.Points(-1.0, 3.0))
        pointsComplesso[7] = (PointsLibrary.Points(-3.0, 1.0))
        pointsComplesso[8] = (PointsLibrary.Points(0.0, 0.0))
        pointsComplesso[9] = (PointsLibrary.Points(-3.0, -2.0))

        self.assertEqual(len(pointsComplesso), 10)
        self.assertEqual(pointsComplesso[0].Y, -2.0)
        self.assertEqual(pointsComplesso[2].X, 3.0)

        inizioComplesso: PointsLibrary.Points = PointsLibrary.Points(-2.0, -2.0)
        fineComplesso: PointsLibrary.Points = PointsLibrary.Points(1.0, 1.0)
        segmentoComplesso: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioComplesso, fineComplesso)

        newPointsComplesso = NewLibrary.newItems.NewPoints(pointsComplesso, segmentoComplesso)

        self.assertEqual(len(newPointsComplesso), 17)

        for i in range(0, len(pointsComplesso)):
            self.assertEqual(newPointsComplesso[i].X, pointsComplesso[i].X)
            self.assertEqual(newPointsComplesso[i].Y, pointsComplesso[i].Y)

        # testo i punti di intersezione
        self.assertEqual(newPointsComplesso[12].X, pointsComplesso[5].X)
        self.assertEqual(newPointsComplesso[12].Y, pointsComplesso[5].Y)
        self.assertEqual(newPointsComplesso[13].X, pointsComplesso[8].X)
        self.assertEqual(newPointsComplesso[13].Y, pointsComplesso[8].Y)

        #Testo i punti di intersezione: per farlo, creo prima i lati che vengono intersecati,
        # ne calcolo l'intersezione con il segmento con il metodo PointIntersecrtiion e
        # confronto con il vettore newPoints.

        lato2_3Complesso: SegmentLibrary.Segment = SegmentLibrary.Segment(pointsComplesso[2], pointsComplesso[3])
        lato3_4Complesso: SegmentLibrary.Segment = SegmentLibrary.Segment(pointsComplesso[3], pointsComplesso[4])
        lato9_0Complesso: SegmentLibrary.Segment = SegmentLibrary.Segment(pointsComplesso[9], pointsComplesso[0])

        punto10: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato2_3Complesso, segmentoComplesso)
        punto11: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato3_4Complesso, segmentoComplesso)
        punto12: PointsLibrary.Points = IntersectorLibrary.Intersector.PointIntersection(lato9_0Complesso, segmentoComplesso)

        self.assertEqual(newPointsComplesso[10].X - punto10.X < 1e-6, True)
        self.assertEqual(newPointsComplesso[10].Y - punto10.Y < 1e-6, True)
        self.assertEqual(newPointsComplesso[11].X - punto11.X < 1e-6, True)
        self.assertEqual(newPointsComplesso[11].Y - punto11.Y < 1e-6, True)
        self.assertEqual(newPointsComplesso[14].X - punto12.X < 1e-6, True)
        self.assertEqual(newPointsComplesso[14].Y - punto12.Y < 1e-6, True)

        # Controllo infine il punto iniziale e finale del segmento, cioe se
        # sono stati inseriti correttamente in newpoints. Qui uso l'uguaglianza
        # perche tanto nel metodo implementato abbiamo copiato in newpoints

        self.assertEqual(newPointsComplesso[15].X, fineComplesso.X, True)
        self.assertEqual(newPointsComplesso[15].Y, fineComplesso.Y, True)
        self.assertEqual(newPointsComplesso[16].X, inizioComplesso.X, True)
        self.assertEqual(newPointsComplesso[16].Y, inizioComplesso.Y, True)




    def test_NewPolygon_Vertices_method(self):

        # TESTO RETTANGOLO
        vertici = []
        nuovivertex = []
        points = []
        points.append(PointsLibrary.Points(1.0, 1.0))
        points.append(PointsLibrary.Points(5.0, 1.0))
        points.append(PointsLibrary.Points(5.0, 3.1))
        points.append(PointsLibrary.Points(1.0, 3.1))

        points[0] = (PointsLibrary.Points(1.0, 1.0))
        points[1] = (PointsLibrary.Points(5.0, 1.0))
        points[2] = (PointsLibrary.Points(5.0, 3.1))
        points[3] = (PointsLibrary.Points(1.0, 3.1))

        inizio0: PointsLibrary.Points = PointsLibrary.Points(2.0, 1.2)
        fine0: PointsLibrary.Points = PointsLibrary.Points(4.0, 3.0)

        segmento0: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio0, fine0)

        for i in range(0, 4):
            vertici.append(i)

        self.assertEqual(len(vertici), 4)

        nuovivertex = NewLibrary.newItems.NewPolygonVertices(points, vertici, segmento0)
        self.assertEqual(len(nuovivertex), 8)

        points.clear()


        #TESTO PENTAGONO
        points_pentag = []
        vertici_pentag = []
        nuovivertex_pentag = []

        points_pentag.append(PointsLibrary.Points(2.5, 1.0))
        points_pentag.append(PointsLibrary.Points(4.0, 2.1))
        points_pentag.append(PointsLibrary.Points(3.4, 4.2))
        points_pentag.append(PointsLibrary.Points(1.6, 4.2))
        points_pentag.append(PointsLibrary.Points(1.0, 2.1))

        points_pentag[0] = (PointsLibrary.Points(2.5, 1.0))
        points_pentag[1] = (PointsLibrary.Points(4.0, 2.1))
        points_pentag[2] = (PointsLibrary.Points(3.4, 4.2))
        points_pentag[3] = (PointsLibrary.Points(1.6, 4.2))
        points_pentag[4] = (PointsLibrary.Points(1.0, 2.1))

        inizio_pentag: PointsLibrary.Points = PointsLibrary.Points(1.4, 2.75)
        fine_pentag: PointsLibrary.Points = PointsLibrary.Points(3.6, 2.2)

        segmento_pentag: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio_pentag, fine_pentag)
        for i in range(0, 5):
            vertici_pentag.append(i)

        self.assertEqual(len(vertici_pentag), 5)
        nuovivertex_pentag = NewLibrary.newItems.NewPolygonVertices(points_pentag, vertici_pentag, segmento_pentag)

        self.assertEqual(len(nuovivertex_pentag), 9)


         #POLIGONO VICINI
        vertici_poligono = []
        nuovi_vertici = []
        points = []

        points.append(PointsLibrary.Points(1.5, 1.0))
        points.append(PointsLibrary.Points(5.6, 1.5))
        points.append(PointsLibrary.Points(5.5, 4.8))
        points.append(PointsLibrary.Points(4.0, 6.2))
        points.append(PointsLibrary.Points(3.2, 4.2))
        points.append(PointsLibrary.Points(1.0, 4.0))

        points[0] = (PointsLibrary.Points(1.5, 1.0))
        points[1] = (PointsLibrary.Points(5.6, 1.5))
        points[2] = (PointsLibrary.Points(5.5, 4.8))
        points[3] = (PointsLibrary.Points(4.0, 6.2))
        points[4] = (PointsLibrary.Points(3.2, 4.2))
        points[5] = (PointsLibrary.Points(1.0, 4.0))

        inizio: PointsLibrary.Points = PointsLibrary.Points(2.0, 3.7)
        fine: PointsLibrary.Points = PointsLibrary.Points(4.1, 5.9)
        segmento: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio, fine)

        for i in range(0, 6):
            vertici_poligono.append(i)

        self.assertEqual(len(vertici_poligono), 6)

        nuovi_vertici = NewLibrary.newItems.NewPolygonVertices(points, vertici_poligono, segmento)

        self.assertEqual(len(nuovi_vertici), 12)

                  # POLIGONO D'AURIA

        vertici_poligono_complesso = []
        nuovi_vertici_complesso = []

        points.clear()

        points.append(PointsLibrary.Points(2.0, -2.0))
        points.append(PointsLibrary.Points(0.0, -1.0))
        points.append(PointsLibrary.Points(3.0, 1.0))
        points.append(PointsLibrary.Points(0.0, 2.0))
        points.append(PointsLibrary.Points(3.0, 2.0))
        points.append(PointsLibrary.Points(3.0, 3.0))
        points.append(PointsLibrary.Points(-1.0, 3.0))
        points.append(PointsLibrary.Points(-3.0, 1.0))
        points.append(PointsLibrary.Points(0.0, 0.0))
        points.append(PointsLibrary.Points(-3.0, -2.0))

        points[0] = (PointsLibrary.Points(2.0, -2.0))
        points[1] = (PointsLibrary.Points(0.0, -1.0))
        points[2] = (PointsLibrary.Points(3.0, 1.0))
        points[3] = (PointsLibrary.Points(0.0, 2.0))
        points[4] = (PointsLibrary.Points(3.0, 2.0))
        points[5] = (PointsLibrary.Points(3.0, 3.0))
        points[6] = (PointsLibrary.Points(-1.0, 3.0))
        points[7] = (PointsLibrary.Points(-3.0, 1.0))
        points[8] = (PointsLibrary.Points(0.0, 0.0))
        points[9] = (PointsLibrary.Points(-3.0, -2.0))

        inizioComplesso: PointsLibrary.Points = PointsLibrary.Points(-2.0, -2.0)
        fineComplesso: PointsLibrary.Points = PointsLibrary.Points(1.0, 1.0)
        segmentoComplesso: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioComplesso, fineComplesso)

        for i in range(0, 10):  # da 0 a 9
            vertici_poligono_complesso.append(i)

        self.assertEqual(len(vertici_poligono_complesso), 10)

        nuovi_vertici_complesso = NewLibrary.newItems.NewPolygonVertices(points,
                                                                                vertici_poligono_complesso,
                                                                                segmentoComplesso)
        self.assertEqual(len(nuovi_vertici_complesso), 17)

        self.assertEqual(nuovi_vertici_complesso[12], 5)
        self.assertEqual(nuovi_vertici_complesso[13], 8)
        self.assertEqual(nuovi_vertici_complesso[14], 12)
        self.assertEqual(nuovi_vertici_complesso[15], 13)
        self.assertEqual(nuovi_vertici_complesso[16], 14)


        #TESTO OTTAGONO
        points_ottag = []
        vertici_ottagono = []
        nuovi_vertici_ottagono = []

        points_ottag.append(PointsLibrary.Points(1.0, -3.0))
        points_ottag.append(PointsLibrary.Points(3.0, -1.0))
        points_ottag.append(PointsLibrary.Points(3.0, 1.0))
        points_ottag.append(PointsLibrary.Points(1.0, 3.0))
        points_ottag.append(PointsLibrary.Points(-1.0, 3.0))
        points_ottag.append(PointsLibrary.Points(-4.0, 1.0))
        points_ottag.append(PointsLibrary.Points(-4.0, -1.0))
        points_ottag.append(PointsLibrary.Points(-1.0, -3.0))

        points_ottag[0] = (PointsLibrary.Points(1.0, -3.0))
        points_ottag[1] = (PointsLibrary.Points(3.0, -1.0))
        points_ottag[2] = (PointsLibrary.Points(3.0, 1.0))
        points_ottag[3] = (PointsLibrary.Points(1.0, 3.0))
        points_ottag[4] = (PointsLibrary.Points(-1.0, 3.0))
        points_ottag[5] = (PointsLibrary.Points(-4.0, 1.0))
        points_ottag[6] = (PointsLibrary.Points(-4.0, -1.0))
        points_ottag[7] = (PointsLibrary.Points(-1.0, -3.0))

        inizio_ottag: PointsLibrary.Points = PointsLibrary.Points(1.0, -2.0)
        fine_ottag = PointsLibrary.Points(1.0, 1.0)
        segmento_ottag: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio_ottag, fine_ottag)

        for i in range(0, 8):  # da 0 a 9
            vertici_ottagono.append(i)

        self.assertEqual(len(vertici_ottagono), 8)

        nuovi_vertici_ottagono = NewLibrary.newItems.NewPolygonVertices(points_ottag,
                                                                                vertici_ottagono,
                                                                                segmento_ottag)
        self.assertEqual(len(nuovi_vertici_ottagono), 12)


           #TESTO TRIANGOLO
        points_triang = []
        vertici_triang = []
        nuovi_vertici_triang = []

        points_triang.append(PointsLibrary.Points(-2.0, -2.0))
        points_triang.append(PointsLibrary.Points(2.0, -2.0))
        points_triang.append(PointsLibrary.Points(0.0, 4.1))

        points_triang[0] = (PointsLibrary.Points(-2.0, -2.0))
        points_triang[1] = (PointsLibrary.Points(2.0, -2.0))
        points_triang[2] = (PointsLibrary.Points(0.0, 4.1))

        inizioTriang: PointsLibrary.Points = PointsLibrary.Points(-1.0, -1.0)
        fineTriang: PointsLibrary.Points = PointsLibrary.Points(0.0, 0.0)
        segmento_triang: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioTriang, fineTriang)

        for i in range(0, 3):
            vertici_triang.append(i)

        self.assertEqual(len(vertici_triang), 3)

        nuovi_vertici_triang = NewLibrary.newItems.NewPolygonVertices(points_triang,
                                                                                vertici_triang,
                                                                                segmento_triang)
        self.assertEqual(len(nuovi_vertici_triang), 7)

from unittest import TestCase

import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.vector2d_class as VectorLibrary
import src.operation_class as OperationLibrary

class test_points(TestCase):
    def test_computeDistance(self):
        try:
            point1 = PointsLibrary.Points(1.0, 2.0)
            point2 = PointsLibrary.Points(2.5, 3.0)
            distance = OperationLibrary.Operation.ComputeDistance(point1, point2)
            self.assertEqual(distance - 1.802775638 < 1e-6, True)
        except Exception as ex:
            self.fail()


class Test_segment(TestCase):
    def test_tangentVersor(self):

        try:
            inizio = PointsLibrary.Points(2.0, 2.5)
            fine = PointsLibrary.Points(4.5, 6.0)
            segment = SegmentLibrary.Segment(inizio, fine)
            versore_trovato = OperationLibrary.Operation.TangentVersor(segment)

            self.assertEqual((versore_trovato.X - 2.5) < 1e-6, True)
            self.assertEqual((versore_trovato.Y - 3.5) < 1e-6, True)
        except Exception as ex:
            self.fail()

    def test_normalVersor(self):

        try:
            inizio = PointsLibrary.Points(2.0, 2.5)
            fine = PointsLibrary.Points(4.5, 6.0)
            segment = SegmentLibrary.Segment(inizio, fine)
            versore_trovato = OperationLibrary.Operation.TangentVersor(segment)
            normale_trovato = OperationLibrary.Operation.NormalVersor(segment)

            self.assertEqual((normale_trovato.X + versore_trovato.Y) < 1e-6, True)
            self.assertEqual((normale_trovato.Y - versore_trovato.X) < 1e-6, True)
        except Exception as ex:
            self.fail()


    def test_Concave_method(self):

                        # 1 TESTO UN PUNTO CONCAVO: IL 4 DEL POLIGONO DI VICINI
        try:
            inizio: PointsLibrary.Points = PointsLibrary.Points(3.2, 4.2)
            fine: PointsLibrary.Points = PointsLibrary.Points(4.0, 6.2)
            edge: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio, fine)
            inizio2: PointsLibrary.Points = PointsLibrary.Points(1.0, 4.0)
            fine2: PointsLibrary.Points = PointsLibrary.Points(3.2, 4.2)
            next_edge: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio2, fine2)
            inizioSegmento: PointsLibrary.Points = PointsLibrary.Points(2.0, 3.7)
            fineSegmento: PointsLibrary.Points = PointsLibrary.Points(4.1, 5.9)
            segment: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento, fineSegmento)

            self.assertEqual(OperationLibrary.Operation.Concave(edge, next_edge, segment), True)

        except Exception as ex:
            self.fail()

                     # 2 TESTO UN PUNTO CONVESSO: IL 3 DEL POLIGONO DI VICINI
        try:
            inizio3: PointsLibrary.Points = PointsLibrary.Points(4.0, 6.2)
            fine3: PointsLibrary.Points = PointsLibrary.Points(5.5, 4.8)
            edge2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio3, fine3)
            inizio4: PointsLibrary.Points = PointsLibrary.Points(3.2, 4.2)
            fine4: PointsLibrary.Points = PointsLibrary.Points(4.0, 6.2)
            next_edge2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio4, fine4)
            inizioSegmento2: PointsLibrary.Points = PointsLibrary.Points(2.0, 3.7)
            fineSegmento2: PointsLibrary.Points = PointsLibrary.Points(4.1, 5.9)
            segment2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioSegmento2, fineSegmento2)

            self.assertEqual(OperationLibrary.Operation.Concave(edge2, next_edge2, segment2), False)

        except Exception as ex:
            self.fail()


class test_vector(TestCase):
    def test_dotProduct(self):

        try:
            vettore1 = VectorLibrary.Vector2d(2.0, 1.0)
            vettore2 = VectorLibrary.Vector2d(3.0, 2.0)
            dotProduct = OperationLibrary.Operation.DotProduct(vettore1, vettore2)
            self.assertEqual(dotProduct, 8.0)
        except Exception as ex:
            self.fail()


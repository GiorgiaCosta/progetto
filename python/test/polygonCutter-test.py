from unittest import TestCase

import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.polygonCutter_class as CutterLibrary
import src.operation_class as OperationLibrary


class TestCuttedPolygon(TestCase):
    def test_ConvexPolygon_method(self):

        numero_test: float = 1
                           # TESTO IL RETTANGOLO.

        points = []
        cutted_polygon = [[]]

        points.append(PointsLibrary.Points(1.0, 1.0))
        points.append(PointsLibrary.Points(5.0, 1.0))
        points.append(PointsLibrary.Points(5.0, 3.1))
        points.append(PointsLibrary.Points(1.0, 3.1))

        points[0] = (PointsLibrary.Points(1.0, 1.0))
        points[1] = (PointsLibrary.Points(5.0, 1.0))
        points[2] = (PointsLibrary.Points(5.0, 3.1))
        points[3] = (PointsLibrary.Points(1.0, 3.1))

        inizio: PointsLibrary.Points = PointsLibrary.Points(2.0, 1.2)
        fine: PointsLibrary.Points = PointsLibrary.Points(4.0, 3.0)

        segmento: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio, fine)

        polygon_vertices = []

        for i in range(0, len(points)):
            polygon_vertices.append(i)

        print("I punti, vertici e poligoni ottenuti dal poligono convesso ")
        print(numero_test)
        print(" sono:\n")

        numero_test = numero_test + 1
        cutted_polygon = CutterLibrary.CutPolygon.cuttedConvexPolygons(points, polygon_vertices, segmento)

                   #controllo il primo poligono
        self.assertEqual(len(cutted_polygon[1]), 6)
        self.assertEqual(cutted_polygon[1][0], 0)
        self.assertEqual(cutted_polygon[1][1], 4)
        self.assertEqual(cutted_polygon[1][2], 7)
        self.assertEqual(cutted_polygon[1][3], 6)
        self.assertEqual(cutted_polygon[1][4], 5)
        self.assertEqual(cutted_polygon[1][5], 3)

                     # controllo il secondo poligono
        self.assertEqual(len(cutted_polygon[2]), 6)
        self.assertEqual(cutted_polygon[2][0], 4)
        self.assertEqual(cutted_polygon[2][1], 1)
        self.assertEqual(cutted_polygon[2][2], 2)
        self.assertEqual(cutted_polygon[2][3], 5)
        self.assertEqual(cutted_polygon[2][4], 6)
        self.assertEqual(cutted_polygon[2][5], 7)

                      #TESTO IL PENTAGONO
        points_pentag = []
        cutted_polygon_pentag = []

        points_pentag.append(PointsLibrary.Points(2.5, 1.0))
        points_pentag.append(PointsLibrary.Points(4.0, 2.1))
        points_pentag.append(PointsLibrary.Points(3.4, 4.2))
        points_pentag.append(PointsLibrary.Points(1.6, 4.2))
        points_pentag.append(PointsLibrary.Points(1.0, 2.1))

        points_pentag[0] = (PointsLibrary.Points(2.5, 1.0))
        points_pentag[1] = (PointsLibrary.Points(4.0, 2.1))
        points_pentag[2] = (PointsLibrary.Points(3.4, 4.2))
        points_pentag[3] = (PointsLibrary.Points(1.6, 4.2))
        points_pentag[4] = (PointsLibrary.Points(1.0, 2.1))

        inizio_pentag: PointsLibrary.Points = PointsLibrary.Points(3.6, 2.2)
        fine_pentag: PointsLibrary.Points = PointsLibrary.Points(1.4, 2.75)

        segmento_pentag: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio_pentag, fine_pentag)

        polygon_vertices_pentag = []
        for i in range(0, len(points_pentag)):
            polygon_vertices_pentag.append(i)

        print("I punti, vertici e poligoni ottenuti dal poligono convesso ")
        print(numero_test)
        print(" sono:\n")

        numero_test = numero_test + 1
        cutted_polygon_pentag = CutterLibrary.CutPolygon.cuttedConvexPolygons(points_pentag, polygon_vertices_pentag,
                                                                         segmento_pentag)

        # controllo il primo poligono
        self.assertEqual(len(cutted_polygon_pentag[1]), 6)
        self.assertEqual(cutted_polygon_pentag[1][0], 0)
        self.assertEqual(cutted_polygon_pentag[1][1], 1)
        self.assertEqual(cutted_polygon_pentag[1][2], 6)
        self.assertEqual(cutted_polygon_pentag[1][3], 7)
        self.assertEqual(cutted_polygon_pentag[1][4], 5)
        self.assertEqual(cutted_polygon_pentag[1][5], 4)

        #controllo il secondo poligono
        self.assertEqual(len(cutted_polygon_pentag[2]), 6)
        self.assertEqual(cutted_polygon_pentag[2][0], 1)
        self.assertEqual(cutted_polygon_pentag[2][1], 2)
        self.assertEqual(cutted_polygon_pentag[2][2], 3)
        self.assertEqual(cutted_polygon_pentag[2][3], 5)
        self.assertEqual(cutted_polygon_pentag[2][4], 7)
        self.assertEqual(cutted_polygon_pentag[2][5], 6)


                    #TESTO UN ALTRO POLIGONO CONVESSO CON 4 LATI
        points_generico = []
        cutted_polygon_generico = [[]]

        points_generico.append(PointsLibrary.Points(-2.0, -2.0))
        points_generico.append(PointsLibrary.Points(2.0, -2.0))
        points_generico.append(PointsLibrary.Points(2.0, 2.0))
        points_generico.append(PointsLibrary.Points(-2.0, 2.0))

        points_generico[0] = (PointsLibrary.Points(-2.0, -2.0))
        points_generico[1] = (PointsLibrary.Points(2.0, -2.0))
        points_generico[2] = (PointsLibrary.Points(2.0, 2.0))
        points_generico[3] = (PointsLibrary.Points(-2.0, 2.0))

        inizioGenerico: PointsLibrary.Points = PointsLibrary.Points(1.0, -1.0)
        fineGenerico: PointsLibrary.Points = PointsLibrary.Points(-1.0, +1.0)

        segmento_generico:  SegmentLibrary.Segment = SegmentLibrary.Segment(inizioGenerico, fineGenerico)

        polygon_vertices_generico = []

        for i in range(0, len(points_generico)):
            polygon_vertices_generico.append(i)

        print("I punti, vertici e poligoni ottenuti dal poligono convesso ")
        print(numero_test)
        print(" sono:\n")

        numero_test = numero_test + 1
        cutted_polygon_generico = CutterLibrary.CutPolygon.cuttedConvexPolygons(points_generico, polygon_vertices_generico,
                                                                         segmento_generico)

        # controllo il primo poligono
        self.assertEqual(len(cutted_polygon_generico[1]), 5)
        self.assertEqual(cutted_polygon_generico[1][0], 0)
        self.assertEqual(cutted_polygon_generico[1][1], 1)
        self.assertEqual(cutted_polygon_generico[1][2], 4)
        self.assertEqual(cutted_polygon_generico[1][3], 5)
        self.assertEqual(cutted_polygon_generico[1][4], 3)

        # controllo il secondo poligono
        self.assertEqual(len(cutted_polygon_generico[2]), 5)
        self.assertEqual(cutted_polygon_generico[2][0], 1)
        self.assertEqual(cutted_polygon_generico[2][1], 2)
        self.assertEqual(cutted_polygon_generico[2][2], 3)
        self.assertEqual(cutted_polygon_generico[2][3], 5)
        self.assertEqual(cutted_polygon_generico[2][4], 4)


                                   #  TESTO OTTAGONO
        points_ottag = []
        polygon_vertices_ottag = []
        cutted_polygon_ottag = [[]]

        points_ottag.append(PointsLibrary.Points(1.0, -3.0))
        points_ottag.append(PointsLibrary.Points(3.0, -1.0))
        points_ottag.append(PointsLibrary.Points(3.0, 1.0))
        points_ottag.append(PointsLibrary.Points(1.0, 3.0))
        points_ottag.append(PointsLibrary.Points(-1.0, 3.0))
        points_ottag.append(PointsLibrary.Points(-4.0, 1.0))
        points_ottag.append(PointsLibrary.Points(-4.0, -1.0))
        points_ottag.append(PointsLibrary.Points(-1.0, -3.0))

        points_ottag[0] = (PointsLibrary.Points(1.0, -3.0))
        points_ottag[1] = (PointsLibrary.Points(3.0, -1.0))
        points_ottag[2] = (PointsLibrary.Points(3.0, 1.0))
        points_ottag[3] = (PointsLibrary.Points(1.0, 3.0))
        points_ottag[4] = (PointsLibrary.Points(-1.0, 3.0))
        points_ottag[5] = (PointsLibrary.Points(-4.0, 1.0))
        points_ottag[6] = (PointsLibrary.Points(-4.0, -1.0))
        points_ottag[7] = (PointsLibrary.Points(-1.0, -3.0))

        inizio_ottag: PointsLibrary.Points = PointsLibrary.Points(1.0, -2.0)
        fine_ottag = PointsLibrary.Points(1.0, 1.0)
        segmento_ottag: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio_ottag, fine_ottag)


        for i in range(0, len(points_ottag)):
            polygon_vertices_ottag.append(i)

        print("I punti, vertici e poligoni ottenuti dal poligono convesso ")
        print(numero_test)
        print(" sono:\n")

        numero_test = numero_test + 1
        cutted_polygon_ottag = CutterLibrary.CutPolygon.cuttedConvexPolygons(points_ottag, polygon_vertices_ottag,
                                                                         segmento_ottag)

        #controllo il primo poligono
        self.assertEqual(len(cutted_polygon_ottag[1]), 6)
        self.assertEqual(cutted_polygon_ottag[1][0], 0)
        self.assertEqual(cutted_polygon_ottag[1][1], 1)
        self.assertEqual(cutted_polygon_ottag[1][2], 2)
        self.assertEqual(cutted_polygon_ottag[1][3], 3)
        self.assertEqual(cutted_polygon_ottag[1][4], 8)
        self.assertEqual(cutted_polygon_ottag[1][5], 9)

        #controllo il secondo poligono
        self.assertEqual(len(cutted_polygon_ottag[2]), 8)
        self.assertEqual(cutted_polygon_ottag[2][0], 3)
        self.assertEqual(cutted_polygon_ottag[2][1], 4)
        self.assertEqual(cutted_polygon_ottag[2][2], 5)
        self.assertEqual(cutted_polygon_ottag[2][3], 6)
        self.assertEqual(cutted_polygon_ottag[2][4], 7)
        self.assertEqual(cutted_polygon_ottag[2][5], 0)
        self.assertEqual(cutted_polygon_ottag[2][6], 9)
        self.assertEqual(cutted_polygon_ottag[2][7], 8)




                                      #  TESTO IL TRIANGOLO   #
        points_triang = []
        cutted_polygon_triang = [[]]

        points_triang.append(PointsLibrary.Points(-2.0, -2.0))
        points_triang.append(PointsLibrary.Points(2.0, -2.0))
        points_triang.append(PointsLibrary.Points(0.0, 4.1))

        points_triang[0] = (PointsLibrary.Points(-2.0, -2.0))
        points_triang[1] = (PointsLibrary.Points(2.0, -2.0))
        points_triang[2] = (PointsLibrary.Points(0.0, 4.1))

        inizioTriang: PointsLibrary.Points = PointsLibrary.Points(0.0, 0.0)   # iniziale e finale da invertire
        fineTriang: PointsLibrary.Points = PointsLibrary.Points(-1.0, -1.0)
        segmento_triang: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioTriang, fineTriang)

        polygon_vertices_triang = []

        for i in range(0, len(points_triang)):
            polygon_vertices_triang.append(i)

        print("I punti, vertici e poligoni ottenuti dal poligono convesso ")
        print(numero_test)
        print(" sono:\n")

        numero_test = numero_test + 1
        cutted_polygon_triang = CutterLibrary.CutPolygon.cuttedConvexPolygons(points_triang, polygon_vertices_triang,
                                                                         segmento_triang)
        # controllo il primo poligono
        self.assertEqual(len(cutted_polygon_triang[1]), 5)
        self.assertEqual(cutted_polygon_triang[1][0], 0)
        self.assertEqual(cutted_polygon_triang[1][1], 1)
        self.assertEqual(cutted_polygon_triang[1][2], 3)
        self.assertEqual(cutted_polygon_triang[1][3], 4)
        self.assertEqual(cutted_polygon_triang[1][4], 5)

        # controllo il secondo poligono
        self.assertEqual(len(cutted_polygon_triang[1]), 5)
        self.assertEqual(cutted_polygon_triang[2][0], 3)
        self.assertEqual(cutted_polygon_triang[2][1], 2)
        self.assertEqual(cutted_polygon_triang[2][2], 0)
        self.assertEqual(cutted_polygon_triang[2][3], 5)
        self.assertEqual(cutted_polygon_triang[2][4], 4)

    def test_ConcavePolygon_method(self):

        # TESTO POLIGONO VICINI
        numero_test: int = 0
        points = []
        vertici_poligono = []
        cutted_conc_polygon = []
        lato_intersezione = []
        num_vertici: int = 6

        points.append(PointsLibrary.Points(1.5, 1.0))
        points.append(PointsLibrary.Points(5.6, 1.5))
        points.append(PointsLibrary.Points(5.5, 4.8))
        points.append(PointsLibrary.Points(4.0, 6.2))
        points.append(PointsLibrary.Points(3.2, 4.2))
        points.append(PointsLibrary.Points(1.0, 4.0))

        points[0] = (PointsLibrary.Points(1.5, 1.0))
        points[1] = (PointsLibrary.Points(5.6, 1.5))
        points[2] = (PointsLibrary.Points(5.5, 4.8))
        points[3] = (PointsLibrary.Points(4.0, 6.2))
        points[4] = (PointsLibrary.Points(3.2, 4.2))
        points[5] = (PointsLibrary.Points(1.0, 4.0))

        inizio: PointsLibrary.Points = PointsLibrary.Points(2.0, 3.7)
        fine: PointsLibrary.Points = PointsLibrary.Points(4.1, 5.9)
        segmento: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio, fine)

        for i in range(0, len(points)):
            vertici_poligono.append(i)

        self.assertEqual(len(vertici_poligono), 6)

        print("I punti, vertici e poligoni ottenuti dal poligono concavo ")
        print(numero_test)
        print(" sono:\n")

        cutted_conc_polygon = CutterLibrary.CutPolygon.cuttedConcavePolygon(points, vertici_poligono, segmento)

            # CONTROLLO CHE IL PUNTO 3 LO PRENDA COME CONVESSO NEL POLIGONO DI VICINI, il 4 come concavo e il 5 come convesso

        self.assertEqual(OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points[4], points[3]),
                                                            SegmentLibrary.Segment(points[5],
                                                                                   points[4]), segmento), True)

        self.assertEqual(OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points[5], points[4]),
                                                            SegmentLibrary.Segment(points[0],
                                                                                   points[5]), segmento), False)

        self.assertEqual(OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points[3], points[2]),
                                                            SegmentLibrary.Segment(points[4],
                                                                                   points[3]), segmento), False)

        self.assertEqual(len(cutted_conc_polygon), 5)

            # Controllo il primo poligono

        self.assertEqual(len(cutted_conc_polygon[1]), 4)
        self.assertEqual(cutted_conc_polygon[1][0], 6)
        self.assertEqual(cutted_conc_polygon[1][1], 3)
        self.assertEqual(cutted_conc_polygon[1][2], 7)
        self.assertEqual(cutted_conc_polygon[1][3], 10)

            # Controllo il secondo poligono

        self.assertEqual(len(cutted_conc_polygon[2]), 10)
        self.assertEqual(cutted_conc_polygon[2][0], 4)
        self.assertEqual(cutted_conc_polygon[2][1], 8)
        self.assertEqual(cutted_conc_polygon[2][2], 11)
        self.assertEqual(cutted_conc_polygon[2][3], 9)
        self.assertEqual(cutted_conc_polygon[2][4], 0)
        self.assertEqual(cutted_conc_polygon[2][5], 1)
        self.assertEqual(cutted_conc_polygon[2][6], 2)
        self.assertEqual(cutted_conc_polygon[2][7], 6)
        self.assertEqual(cutted_conc_polygon[2][8], 10)
        self.assertEqual(cutted_conc_polygon[2][9], 7)

            # Controllo il terzo poligono

        self.assertEqual(len(cutted_conc_polygon[3]), 4)
        self.assertEqual(cutted_conc_polygon[3][0], 8)
        self.assertEqual(cutted_conc_polygon[3][1], 5)
        self.assertEqual(cutted_conc_polygon[3][2], 9)
        self.assertEqual(cutted_conc_polygon[3][3], 11)

            ###############
            # POLIGONO COMPLESSO

        vertici_poligono_complesso = []
        nuovi_vertici_complesso = []
        points_complesso = []
        cutted_conc_polygon_complesso = [[]]

        points_complesso.append(PointsLibrary.Points(2.0, -2.0))
        points_complesso.append(PointsLibrary.Points(0.0, -1.0))
        points_complesso.append(PointsLibrary.Points(3.0, 1.0))
        points_complesso.append(PointsLibrary.Points(0.0, 2.0))
        points_complesso.append(PointsLibrary.Points(3.0, 2.0))
        points_complesso.append(PointsLibrary.Points(3.0, 3.0))
        points_complesso.append(PointsLibrary.Points(-1.0, 3.0))
        points_complesso.append(PointsLibrary.Points(-3.0, 1.0))
        points_complesso.append(PointsLibrary.Points(0.0, 0.0))
        points_complesso.append(PointsLibrary.Points(-3.0, -2.0))

        points_complesso[0] = (PointsLibrary.Points(2.0, -2.0))
        points_complesso[1] = (PointsLibrary.Points(0.0, -1.0))
        points_complesso[2] = (PointsLibrary.Points(3.0, 1.0))
        points_complesso[3] = (PointsLibrary.Points(0.0, 2.0))
        points_complesso[4] = (PointsLibrary.Points(3.0, 2.0))
        points_complesso[5] = (PointsLibrary.Points(3.0, 3.0))
        points_complesso[6] = (PointsLibrary.Points(-1.0, 3.0))
        points_complesso[7] = (PointsLibrary.Points(-3.0, 1.0))
        points_complesso[8] = (PointsLibrary.Points(0.0, 0.0))
        points_complesso[9] = (PointsLibrary.Points(-3.0, -2.0))

        inizio_complesso: PointsLibrary.Points = PointsLibrary.Points(-1.5, -1.5)
        fine_complesso: PointsLibrary.Points = PointsLibrary.Points(1.0, 1.0)
        segmento_complesso: SegmentLibrary.Segment = SegmentLibrary.Segment(inizio_complesso, fine_complesso)

        for i in range(0, 10):
            vertici_poligono_complesso.append(i)

        print("I punti, vertici e poligoni ottenuti dal poligono complesso concavo ")
        print(numero_test)
        print(" sono:\n")

        cutted_conc_polygon_complesso = CutterLibrary.CutPolygon.cuttedConcavePolygon(points_complesso,
                                                                                          vertici_poligono_complesso,
                                                                                          segmento_complesso)
        self.assertEqual(
            OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points_complesso[3], points_complesso[2]),
                                               SegmentLibrary.Segment(points_complesso[4], points_complesso[3]),
                                               segmento_complesso), True)
        self.assertEqual(
            OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points_complesso[4], points_complesso[3]),
                                               SegmentLibrary.Segment(points_complesso[5], points_complesso[4]),
                                               segmento_complesso), False)
        self.assertEqual(
            OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points_complesso[5], points_complesso[4]),
                                               SegmentLibrary.Segment(points_complesso[6], points_complesso[5]),
                                               segmento_complesso), False)
        self.assertEqual(
            OperationLibrary.Operation.Concave(SegmentLibrary.Segment(points_complesso[8], points_complesso[7]),
                                               SegmentLibrary.Segment(points_complesso[9], points_complesso[8]),
                                               segmento_complesso), True)

            # Controllo il primo poligono.

        self.assertEqual(len(cutted_conc_polygon_complesso[1]), 8)
        self.assertEqual(cutted_conc_polygon_complesso[1][0], 3)
        self.assertEqual(cutted_conc_polygon_complesso[1][1], 11)
        self.assertEqual(cutted_conc_polygon_complesso[1][2], 5)
        self.assertEqual(cutted_conc_polygon_complesso[1][3], 6)
        self.assertEqual(cutted_conc_polygon_complesso[1][4], 7)
        self.assertEqual(cutted_conc_polygon_complesso[1][5], 8)
        self.assertEqual(cutted_conc_polygon_complesso[1][6], 13)
        self.assertEqual(cutted_conc_polygon_complesso[1][7], 10)

            # Controllo il secondo poligono.

        self.assertEqual(len(cutted_conc_polygon_complesso[2]), 3)
        self.assertEqual(cutted_conc_polygon_complesso[2][0], 11)
        self.assertEqual(cutted_conc_polygon_complesso[2][1], 4)
        self.assertEqual(cutted_conc_polygon_complesso[2][2], 5)

            # Controllo il terzo poligono.

        self.assertEqual(len(cutted_conc_polygon_complesso[3]), 4)
        self.assertEqual(cutted_conc_polygon_complesso[3][0], 8)
        self.assertEqual(cutted_conc_polygon_complesso[3][1], 9)
        self.assertEqual(cutted_conc_polygon_complesso[3][2], 12)
        self.assertEqual(cutted_conc_polygon_complesso[3][3], 14)

            # Controllo il quarto poligono.

        self.assertEqual(len(cutted_conc_polygon_complesso[4]), 8)
        self.assertEqual(cutted_conc_polygon_complesso[4][0], 10)
        self.assertEqual(cutted_conc_polygon_complesso[4][1], 13)
        self.assertEqual(cutted_conc_polygon_complesso[4][2], 8)
        self.assertEqual(cutted_conc_polygon_complesso[4][3], 14)
        self.assertEqual(cutted_conc_polygon_complesso[4][4], 12)
        self.assertEqual(cutted_conc_polygon_complesso[4][5], 0)
        self.assertEqual(cutted_conc_polygon_complesso[4][6], 1)
        self.assertEqual(cutted_conc_polygon_complesso[4][7], 2)

            # Test poligono generico
        verticiPoligonoGenerico2 = []
        nuoviVerticiGenerico2 = []
        pointsGenerico2 = [[]]

        pointsGenerico2.append(PointsLibrary.Points(-4.0, -3.0))
        pointsGenerico2.append(PointsLibrary.Points(5.0, -3.0))
        pointsGenerico2.append(PointsLibrary.Points(7.0, 4.0))
        pointsGenerico2.append(PointsLibrary.Points(5.0, 6.0))
        pointsGenerico2.append(PointsLibrary.Points(3.5, 1.7))
        pointsGenerico2.append(PointsLibrary.Points(-4.0, 4.0))
        pointsGenerico2.append(PointsLibrary.Points(-5.0, 0.0))

        pointsGenerico2[0] = (PointsLibrary.Points(-4.0, -3.0))
        pointsGenerico2[1] = (PointsLibrary.Points(5.0, -3.0))
        pointsGenerico2[2] = (PointsLibrary.Points(7.0, 4.0))
        pointsGenerico2[3] = (PointsLibrary.Points(5.0, 6.0))
        pointsGenerico2[4] = (PointsLibrary.Points(3.5, 1.7))
        pointsGenerico2[5] = (PointsLibrary.Points(-4.0, 4.0))
        pointsGenerico2[6] = (PointsLibrary.Points(-5.0, 0.0))

        inizioGenerico2: PointsLibrary.Points = PointsLibrary.Points(-2.0, 2.5)
        fineGenerico2: PointsLibrary.Points = PointsLibrary.Points(5.1, 4.2)
        segmentoGenerico2: SegmentLibrary.Segment = SegmentLibrary.Segment(inizioGenerico2, fineGenerico2)

        for i in range(0, 7):
            verticiPoligonoGenerico2.append(i)
        cuttedConcPolygonGenerico2 = [[]]

            # AGGIUNGI STAMPA
        print("I punti, vertici e poligoni ottenuti dal poligono generico concavo ")
        print(numero_test)
        print(" sono:\n")

        cuttedConcPolygonGenerico2 = CutterLibrary.CutPolygon.cuttedConcavePolygon(pointsGenerico2,
                                                                                       verticiPoligonoGenerico2,
                                                                                       segmentoGenerico2)


        # controllo il primo poligono
        self.assertEqual(len(cuttedConcPolygonGenerico2[1]), 4)
        self.assertEqual(cuttedConcPolygonGenerico2[1][0], 7)
        self.assertEqual(cuttedConcPolygonGenerico2[1][1], 3)
        self.assertEqual(cuttedConcPolygonGenerico2[1][2], 8)
        self.assertEqual(cuttedConcPolygonGenerico2[1][3], 11)

        # controllo il secondo poligono
        self.assertEqual(len(cuttedConcPolygonGenerico2[2]), 11)
        self.assertEqual(cuttedConcPolygonGenerico2[2][0], 4)
        self.assertEqual(cuttedConcPolygonGenerico2[2][1], 9)
        self.assertEqual(cuttedConcPolygonGenerico2[2][2], 12)
        self.assertEqual(cuttedConcPolygonGenerico2[2][3], 10)
        self.assertEqual(cuttedConcPolygonGenerico2[2][4], 6)
        self.assertEqual(cuttedConcPolygonGenerico2[2][5], 0)
        self.assertEqual(cuttedConcPolygonGenerico2[2][6], 1)
        self.assertEqual(cuttedConcPolygonGenerico2[2][7], 2)
        self.assertEqual(cuttedConcPolygonGenerico2[2][8], 7)
        self.assertEqual(cuttedConcPolygonGenerico2[2][9], 11)
        self.assertEqual(cuttedConcPolygonGenerico2[2][10], 8)

        # controllo il terzo poligono
        self.assertEqual(len(cuttedConcPolygonGenerico2[3]), 4)
        self.assertEqual(cuttedConcPolygonGenerico2[3][0], 9)
        self.assertEqual(cuttedConcPolygonGenerico2[3][1], 5)
        self.assertEqual(cuttedConcPolygonGenerico2[3][2], 10)
        self.assertEqual(cuttedConcPolygonGenerico2[3][3], 12)




import src.empty_class as empty_class
import src.geometry_class as geometry

if __name__ == "__main__":
    empty_object = empty_class.EmptyClass()
    empty_object.empty_method()

import numpy as np
import math
import src.points_class as PointsLibrary
import src.vector2d_class as VectorLibrary
import src.segment_class as SegmentLibrary
import src.operation_class as OperationLibrary

class IIntersector:


    def PointIntersection(self, edge: SegmentLibrary.Segment, segment: SegmentLibrary.Segment) -> PointsLibrary.Points:
        pass

    def Intersection(self, edge: SegmentLibrary.Segment, segment: SegmentLibrary.Segment) -> bool:
        pass

class Intersector(IIntersector):
    @staticmethod
    def PointIntersection(edge: SegmentLibrary.Segment, segment: SegmentLibrary.Segment) -> PointsLibrary.Points:


        ver: VectorLibrary.Vector2d = OperationLibrary.Operation.TangentVersor(segment)
        ver2: VectorLibrary.Vector2d = OperationLibrary.Operation.TangentVersor(edge)
        mtx_tang = np.array([[ver2.X, (-1.0) * ver.X], [ver2.Y, (-1.0) * ver.Y]])
        mtx_tang_inv = np.array([[(-1.0) * ver.Y, ver.X], [(-1.0) * ver2.Y, ver2.X]])
        det: float = np.linalg.det(mtx_tang)
        b = np.array([segment.Inizio.X - edge.Inizio.X, segment.Inizio.Y - edge.Inizio.Y])

        s = mtx_tang_inv.dot(b)
        s /= det
        tolerance_intersection: float = 1 * math.exp(-6)
        p: PointsLibrary.Points = PointsLibrary.Points(0.0, 0.0)

        if (s[0] > -tolerance_intersection) and (s[0] < tolerance_intersection):
            s[0] = 0.0
            p = edge.Inizio
        elif (s[0] > 1.0 - tolerance_intersection) and (s[0] < 1.0 + tolerance_intersection):
            s[0] = 1.0
            p = edge.Fine
        elif (s[0] > -tolerance_intersection) and (s[0] - 1.0 < tolerance_intersection):
            p = PointsLibrary.Points(edge.Inizio.X + ver2.X * s[0], edge.Inizio.Y + ver2.Y * s[0])

        return p

    @staticmethod
    def Intersection(edge: SegmentLibrary.Segment, segment: SegmentLibrary.Segment) -> bool:

        ver: VectorLibrary.Vector2d = OperationLibrary.Operation.TangentVersor(segment)
        ver2: VectorLibrary.Vector2d = OperationLibrary.Operation.TangentVersor(edge)
        mtx_tang = np.array([[ver2.X, (-1.0) * ver.X], [ver2.Y, (-1.0) * ver.Y]])
        mtx_tang_inversa = np.array([[(-1.0) * ver.Y, ver.X], [(-1.0) * ver2.Y, ver2.X]])
        det = np.linalg.det(mtx_tang)
        b = np.array([segment.Inizio.X - edge.Inizio.X, segment.Inizio.Y - edge.Inizio.Y])
        # s = np.array([0 , 0])
        s = mtx_tang_inversa.dot(b)
        s /= det
        tolerance_intersection: float = 1 * math.exp(-6)
        tolerance_parallelism: float = 1 * math.exp(-6)
        intersection: bool = False
        check: float = tolerance_parallelism * tolerance_parallelism * np.linalg.norm(
            mtx_tang[:, 0:1]) * np.linalg.norm(mtx_tang[:, 0:1]) * np.linalg.norm(mtx_tang[:, 1:2]) * np.linalg.norm(
            mtx_tang[:, 1:2])  # cerca norm e col in np. giusto ??????????????
        if (det * det) >= check:
            if (s[0] > -tolerance_intersection) and (s[0] < tolerance_intersection):
                intersection = True
            if (s[0] > 1.0 - tolerance_intersection) and (s[0] < 1.0 + tolerance_intersection):
                intersection = True
            if (s[0] > -tolerance_intersection) and (s[0] - 1.0 < tolerance_intersection):
                intersection = True
        return intersection

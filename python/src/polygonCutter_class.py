import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.new_class as NewLibrary
import src.intersector_class as IntersectionLibrary
import src.operation_class as OperationLibrary
from copy import copy


class ICutPolygon:

    def CuttedConvexPolygons(self, points: [], polygon_vertices: [], segment: SegmentLibrary.Segment) -> [[]]:
        pass

    def CuttedConcavePolygons(self, points: [], polygon_vertices: [], segment: SegmentLibrary.Segment) -> [[]]:
        pass


class CutPolygon(ICutPolygon):

    @staticmethod
    def cuttedConvexPolygons(points: [], polygon_vertices: [], segment: SegmentLibrary.Segment) -> [[]]:
        polygon1 = []
        polygon2 = []
        cutted_polygon = [[]]
        lato_intersezione = []
        new_points = []
        new_polygonvertices = []

        new_points = NewLibrary.newItems.NewPoints(points, segment)
        new_polygonvertices = NewLibrary.newItems.NewPolygonVertices(points, polygon_vertices, segment)

        # memorizzo lati in cui trovo intersezione
        for i in range(0, len(points)):
            if i == (len(points) - 1):
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[0])
            else:
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[i + 1])

            if IntersectionLibrary.Intersector.Intersection(edge, segment) is True:
                if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                    i].X or IntersectionLibrary.Intersector.PointIntersection(
                        edge, segment).Y != points[i].Y):
                    lato_intersezione.append(i)

        ###########
        # CREO PRIMO POLIGONO
        for i in range(0, lato_intersezione[0] + 1):
            polygon1.append(new_polygonvertices[i])

        polygon1.append(new_polygonvertices[len(polygon_vertices)])  # aggiungo prima intersezione

        if segment.Fine.X > segment.Inizio.X:  # se segmento (e quindi retta) rivolta verso destra
            polygon1.append(new_polygonvertices[len(new_polygonvertices) - 1])  # aggiungo punto iniziale
            polygon1.append(new_polygonvertices[len(new_polygonvertices) - 2])

        if segment.Fine.X < segment.Inizio.X:
            polygon1.append(new_polygonvertices[len(new_polygonvertices) - 2])
            polygon1.append(new_polygonvertices[len(new_polygonvertices) - 1])

        # caso particolare con segmento verticale
        if segment.Fine.X == segment.Inizio.X:
            if segment.Fine.Y < segment.Inizio.Y:
                polygon1.append(new_polygonvertices[len(new_polygonvertices) - 1])
                polygon1.append(new_polygonvertices[len(new_polygonvertices) - 2])
            else:
                polygon1.append(new_polygonvertices[len(new_polygonvertices) - 2])
                polygon1.append(new_polygonvertices[len(new_polygonvertices) - 1])

        # se il poligono polygon1 non parte dallo 0 altrimenti alla fine del poligono1 dell'ottagono mi ripeteva lo 0 (0123890)
        if new_points[len(polygon_vertices) + 1].X != new_points[0].X or new_points[len(polygon_vertices) + 1].Y != \
                new_points[0].Y:
            polygon1.append(new_polygonvertices[len(polygon_vertices) + 1])  # aggiungo seconda intersezione

        # se la seconda (e ultima) intersezione  NON è nel lato finale (es pentagono) allora devo aggiungere i vertici mancanti
        # altrimenti non devo piu aggiungere vertici perche quello successivo sarebbe lo 0 ( gia aggiunto per primo)
        if lato_intersezione[1] != len(polygon_vertices) - 1:
            # devo aggiungere nel pentagono il 4
            edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[lato_intersezione[1]],
                                                                  points[lato_intersezione[1] + 1])  # lato 2-3

            # #se intersezione non nel punto finale del lato
            if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                lato_intersezione[1] + 1].X or IntersectionLibrary.Intersector.PointIntersection(
                    edge, segment).Y != points[lato_intersezione[1] + 1].Y):
                for i in range(lato_intersezione[1], polygon_vertices[len(polygon_vertices) - 1]):
                    polygon1.append(new_polygonvertices[i + 1])

            else:  # se intersezione nel punto finale del segmento allora non aggiungo quel vertice
                for i in range(lato_intersezione[1] + 1, polygon_vertices[len(polygon_vertices) - 1]):
                    polygon1.append(new_polygonvertices[i + 1])

        cutted_polygon.append(polygon1)


        # CREO SECONDO POLIGONO

        # se intersezione non è un vertice
        if new_polygonvertices[len(polygon_vertices)] >= len(polygon_vertices):
            polygon2.append(new_polygonvertices[len(polygon_vertices)])  # aggiungo prima intersezione

        for i in range(lato_intersezione[0], lato_intersezione[1]):
            polygon2.append(new_polygonvertices[i + 1])  # aggiungo punto finale lato

        polygon2.append(new_polygonvertices[len(polygon_vertices) + 1])  # aggiungo seconda intersezione

        # QUA AGGIUNGO I DUE PUNTI IN MODO INVERSO RISPETTO AI DUE IF NEL POLIGONO PRECEDENTE
        if segment.Fine.X > segment.Inizio.X:
            polygon2.append(new_polygonvertices[len(new_polygonvertices) - 2])
            polygon2.append(new_polygonvertices[len(new_polygonvertices) - 1])

        if segment.Fine.X < segment.Inizio.X:
            polygon2.append(new_polygonvertices[len(new_polygonvertices) - 1])
            polygon2.append(new_polygonvertices[len(new_polygonvertices) - 2])

        # caso particolare ottagono con segmento verticale (ottagono)
        if segment.Fine.X == segment.Inizio.X:
            if segment.Fine.Y < segment.Inizio.Y:  # allora aggiungo prima il punto finale

                polygon2.append(new_polygonvertices[len(new_polygonvertices) - 2])
                polygon2.append(new_polygonvertices[len(new_polygonvertices) - 1])
            else:
                polygon2.append(new_polygonvertices[len(new_polygonvertices) - 1])
                polygon2.append(new_polygonvertices[len(new_polygonvertices) - 2])

        cutted_polygon.append(polygon2)

        return cutted_polygon

    @staticmethod
    def cuttedConcavePolygon(points: [], polygon_vertices: [], segment: SegmentLibrary.Segment):
        polygon = []
        cutted_polygon = [[]]
        new_points = []
        new_points = NewLibrary.newItems.NewPoints(points, segment)
        new_polygonVertices = []
        new_polygonVertices = NewLibrary.newItems.NewPolygonVertices(points, polygon_vertices, segment)
        lato_intersezione = []
        num: int = 0

        # memorizzo lati in cui trovo intersezione
        for i in range(0, len(points)):
            if i == (len(points) - 1):
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[0])
            else:
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[i + 1])

            if IntersectionLibrary.Intersector.Intersection(edge, segment) is True:
                if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                    i].X or IntersectionLibrary.Intersector.PointIntersection(
                    edge, segment).Y != points[i].Y):
                    lato_intersezione.append(i)

        next_vertex: int
        num: int = 0
        third_intersection: int
        considero_concavo: bool = True
        controllo : int = 0

        for i in range(0, len(lato_intersezione) - 1):
            edge: SegmentLibrary.Segment = SegmentLibrary.Segment(new_points[lato_intersezione[i] + 1],
                                                                  new_points[lato_intersezione[i]])

            next_edge: SegmentLibrary.Segment = SegmentLibrary.Segment(new_points[lato_intersezione[i] + 2],
                                                                       new_points[lato_intersezione[i] + 1])

            previous_edge: SegmentLibrary.Segment = SegmentLibrary.Segment(new_points[lato_intersezione[i]],
                                                                           new_points[lato_intersezione[i] - 1])

            previous_previous_edge: SegmentLibrary.Segment = SegmentLibrary.Segment(
                new_points[lato_intersezione[i] - 1],
                new_points[lato_intersezione[i] - 2])
            vertice_da_non_considerare: int = 0

            if i > 1:
                if OperationLibrary.Operation.Concave(previous_edge, edge, segment) is False and OperationLibrary.Operation.Concave(
                        previous_previous_edge, previous_edge, segment) is True and new_polygonVertices[i + 1] == \
                        lato_intersezione[i] - 1:
                    vertice_da_non_considerare = 1
            if new_points[len(points) + i].X == new_points[lato_intersezione[i] + 1].X and new_points[
                len(points) + i].Y == \
                    new_points[lato_intersezione[i] + 1].Y and OperationLibrary.Operation.Concave(edge, next_edge, segment) is True:
                considero_concavo = False

            if vertice_da_non_considerare == 0:

                # CASO VERTICE CONCAVO
                if OperationLibrary.Operation.Concave(edge, next_edge, segment) is True and considero_concavo is True:
                    polygon.append(new_polygonVertices[lato_intersezione[i] + 1])  # cambite queste due righe
                    polygon.append(new_polygonVertices[len(polygon_vertices) + 1 + i])

                    if segment.Fine.Y > segment.Inizio.Y:
                        # se punto finale segmento tra ultime due intersezioni nel poligono es 8 e 10
                        if new_points[len(new_points) - 2].X < new_points[len(points) + i + 1].X and new_points[
                            len(new_points) - 2].X > new_points[len(points) + i + 2].X and new_points[
                            len(new_points) - 2].Y > new_points[len(points) + i + 2].Y and new_points[
                            len(new_points) - 2].Y < new_points[len(points) + i + 1].Y:
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                        # controllo con punto iniziale segmento
                        if (new_points[len(new_points) - 1].X < new_points[len(points) + i + 1].X and new_points[
                            len(new_points) - 1].X > new_points[len(points) + i + 2].X and new_points[
                            len(new_points) - 1].Y > new_points[len(points) + i + 2].Y and new_points[
                            len(new_points) - 1].Y < new_points[len(points) + i + 1].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                    if (segment.Fine.Y < segment.Inizio.Y):

                        # se punto finale segmento tra ultime due intersezioni nel poligono es 8 e 10
                        if (new_points[len(new_points) - 2].X < new_points[len(points) + i + 1].X and new_points[
                            len(new_points) - 2].X > new_points[len(points) + i + 2].X and new_points[
                            len(new_points) - 2].Y < new_points[len(points) + i + 2].Y and new_points[
                            len(new_points) - 2].Y > new_points[len(points) + i + 1].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                        # controllo con punto iniziale segmento
                        if (new_points[len(new_points) - 1].X < new_points[len(points) + i + 1].X and new_points[
                            len(new_points) - 1].X > new_points[len(points) + i + 2].X and new_points[
                            len(new_points) - 1].Y < new_points[len(points) + i + 2].Y and new_points[
                            len(new_points) - 1].Y > new_points[len(points) + i + 1].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                    polygon.append(new_polygonVertices[len(polygon_vertices) + i + 2])

                    # devo aggiungere i punti tra le due intersezioni (6 e 7)

                    next_next_intersection: int = new_polygonVertices[len(polygon_vertices) + i + 2]
                    num = lato_intersezione[i + 2] - lato_intersezione[i + 1]
                    next_vertex = lato_intersezione[i + 2] + 1
                    third_intersection = new_polygonVertices[len(polygon_vertices) + i + 3]

                    # es Vicini
                    if lato_intersezione[i + 2] == polygon_vertices[len(polygon_vertices) - 1]:
                        num = lato_intersezione[i - 1] + 1
                        next_vertex = 0
                        third_intersection = new_polygonVertices[len(polygon_vertices) + i - 1]
                        controllo = 1

                    if next_next_intersection < len(polygon_vertices):
                        next_vertex = next_vertex + 1
                        num = num + 1
                        controllo = 1

                    if controllo == 0:
                        num = lato_intersezione[0] + len(points) - lato_intersezione[len(lato_intersezione) - 1] - 2
                        next_vertex = 0
                        third_intersection = new_polygonVertices[len(polygon_vertices) + i - 1]
                        for x in range (lato_intersezione[len(lato_intersezione) - 1], len(polygon_vertices) - 1):
                            polygon.append(lato_intersezione[len(lato_intersezione) - 1] + 1)

                    cicli: int = next_vertex + num

                    for n in range(next_vertex, cicli):
                        polygon.append(n)

                    polygon.append(third_intersection)
                    num_intersezioniInMezzo: int = 0

                    k = len(points)

                    for k in range(len(points),
                                   len(points) + 3):  # quanti punti di intersezione prima dell'8 a parte la prima
                        if new_polygonVertices[k] != third_intersection:
                            num_intersezioniInMezzo = num_intersezioniInMezzo + 1

                    if i == 0:
                        if segment.Fine.Y > segment.Inizio.Y:

                            # se punto finale segmento tra ultime due intersezioni nel poligono es 8 e 10
                            if (new_points[len(new_points) - 2].X < new_points[len(points) + i].X and new_points[
                                len(new_points) - 2].X > new_points[len(points) + num_intersezioniInMezzo].X and
                                    new_points[len(new_points) - 2].Y < new_points[len(points) + i].Y and new_points[
                                        len(new_points) - 2].Y > new_points[len(points) + num_intersezioniInMezzo].Y):
                                polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                            # controllo con punto iniziale segmento
                            if (new_points[len(new_points) - 1].X < new_points[len(points) + i].X and new_points[
                                len(new_points) - 1].X > new_points[len(points) + num_intersezioniInMezzo].X and
                                    new_points[len(new_points) - 1].Y < new_points[len(points) + i].Y and new_points[
                                        len(new_points) - 1].Y > new_points[len(points) + num_intersezioniInMezzo].Y):
                                polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                    if i != 0:
                        if segment.Fine.Y > segment.Inizio.Y:

                            # se punto finale segmento tra ultime due intersezioni nel poligono es 8 e 10
                            if (new_points[len(new_points) - 2].X < new_points[len(points) + i - 1].X and
                                    new_points[len(new_points) - 2].X > new_points[len(points) + i].X and
                                    new_points[len(new_points) - 2].Y > new_points[len(points) + i].Y and
                                    new_points[len(new_points) - 2].Y < new_points[len(points) + i - 1].Y):

                                polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                                # controllo con punto iniziale segmento
                                if (new_points[len(new_points) - 1].X < new_points[len(points) + i - 1].X and
                                        new_points[len(new_points) - 1].X > new_points[len(points) + i].X and
                                        new_points[len(new_points) - 1].Y > new_points[len(points) + i].Y and
                                        new_points[len(new_points) - 1].Y < new_points[len(points) + i - 1].Y):
                                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                            if segment.Fine.Y < segment.Inizio.Y:

                                if (new_points[len(new_points) - 2].X < new_points[len(points) + i - 1].X and
                                        new_points[len(new_points) - 2].X > new_points[len(points) + i].X and
                                        new_points[len(new_points) - 2].Y < new_points[len(points) + i].Y and
                                        new_points[len(new_points) - 2].Y > new_points[len(points) + i - 1].Y):
                                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                                # controllo con punto iniziale segmento
                                if (new_points[len(new_points) - 1].X < new_points[len(points) + i - 1].X and
                                        new_points[len(new_points) - 1].X > new_points[len(points) + i].X and
                                        new_points[len(new_points) - 1].Y < new_points[len(points) + i].Y and
                                        new_points[len(new_points) - 1].Y > new_points[len(points) + i - 1].Y):
                                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                    polygon.append(new_polygonVertices[len(polygon_vertices) + i])
                    cutted_polygon.append(polygon)

                    cutted_polygon[len(cutted_polygon) - 1] = copy(polygon)

                    polygon.clear()

                    # CASO VERTICE CONVESSO

                if OperationLibrary.Operation.Concave(edge, next_edge, segment) is False or (
                        OperationLibrary.Operation.Concave(edge, next_edge, segment) is True and considero_concavo is False):

                    polygon.append(new_polygonVertices[len(polygon_vertices) + i])
                    next_vertex = lato_intersezione[i] + 1
                    num = lato_intersezione[i + 1] - lato_intersezione[i]

                    if new_polygonVertices[len(polygon_vertices) + i] == new_polygonVertices[
                        len(new_polygonVertices) - 3]:
                        next_vertex = 0
                        num = lato_intersezione[0] + 1

                    vertice_coincideConInter: int = 0

                    for j in range(0, len(polygon_vertices)):
                        if new_polygonVertices[len(polygon_vertices) + i] == polygon_vertices[j]:
                            vertice_coincideConInter = 1

                    for n in range(next_vertex + vertice_coincideConInter, next_vertex + num):
                        polygon.append(new_polygonVertices[n])

                    polygon.append(new_polygonVertices[len(polygon_vertices) + i + 1])

                    if segment.Fine.Y > segment.Inizio.Y:

                        # se punto finale segmento tra due intersezioni nel poligono
                        if (new_points[len(new_points) - 2].X > new_points[len(points) + i + 1].X and
                                new_points[len(new_points) - 2].X < new_points[len(points) + i].X and
                                new_points[len(new_points) - 2].Y > new_points[len(points) + i + 1].Y and
                                new_points[len(new_points) - 2].Y < new_points[len(points) + i].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                        # controllo con punto iniziale segmento
                        if (new_points[len(new_points) - 1].X > new_points[len(points) + i + 1].X and
                                new_points[len(new_points) - 1].X < new_points[len(points) + i].X and
                                new_points[len(new_points) - 1].Y > new_points[len(points) + i + 1].Y and
                                new_points[len(new_points) - 1].Y < new_points[len(points) + i].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                    elif segment.Fine.Y < segment.Inizio.Y:
                        # controllo con punto iniziale segmento
                        if (new_points[len(new_points) - 1].X > new_points[len(points) + i + 1].X and
                                new_points[len(new_points) - 1].X < new_points[len(points) + i].X and
                                new_points[len(new_points) - 1].Y > new_points[len(points) + i].Y and
                                new_points[len(new_points) - 1].Y < new_points[len(points) + i + 1].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

                        # se punto finale segmento tra due intersezioni nel poligono
                        if (new_points[len(new_points) - 2].X > new_points[len(points) + i + 1].X and
                                new_points[len(new_points) - 2].X < new_points[len(points) + i].X and
                                new_points[len(new_points) - 2].Y > new_points[len(points) + i].Y and
                                new_points[len(new_points) - 2].Y < new_points[len(points) + i + 1].Y):
                            polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                    cutted_polygon.append(polygon)

                    cutted_polygon[len(cutted_polygon) - 1] = copy(polygon)

                    polygon.clear()


        fineCiclo: int = 0
        for i in range(0, len(cutted_polygon)):

            for k in range(0, len(cutted_polygon[i])):

                if cutted_polygon[i][k] == 0:
                    fineCiclo = 1

        var: int = 0
        numPunti: int = 0

        if fineCiclo == 0:
            # Creo il poligono contenente lo zero.
            polygon.append(new_polygonVertices[len(points)])

            # numPunti: conto numeri punti intersezione tra i segmenti con intersezione 10 e segmento con intersezione 8 cioe il 5 e 1  wuindi numPunti=2
            for var in range(len(points), len(points) + 2):
                if new_points[var + 1].X > new_points[len(points)].X:  #
                    numPunti = numPunti + 1

            if segment.Fine.Y > segment.Inizio.Y:
                if (new_points[len(new_points) - 2].X < new_points[len(points)].X and
                        new_points[len(new_points) - 2].X > new_points[len(points) + numPunti + 1].X and
                        new_points[len(new_points) - 2].Y > new_points[len(points) + numPunti + 1].Y and
                        new_points[len(new_points) - 2].Y < new_points[len(points)].Y):
                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                if (new_points[len(new_points) - 1].X < new_points[len(points)].X and
                        new_points[len(new_points) - 1].X > new_points[len(points) + numPunti + 1].X and
                        new_points[len(new_points) - 1].Y > new_points[len(points) + 1 + numPunti].Y and
                        new_points[len(new_points) - 1].Y < new_points[len(points)].Y):
                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

            polygon.append(new_polygonVertices[len(points) + 1 + numPunti])

            if segment.Fine.Y > segment.Inizio.Y:
                if (new_points[len(new_points) - 2].X < new_points[len(points) + numPunti + 1].X and
                        new_points[len(new_points) - 2].X > new_points[len(points) + 2 + numPunti].X and
                        new_points[len(new_points) - 2].Y > new_points[len(points) + 2 + numPunti].Y and
                        new_points[len(new_points) - 2].Y < new_points[len(points) + 1 + numPunti].Y):
                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 2])

                if (new_points[len(new_points) - 1].X < new_points[len(points) + numPunti + 1].X and
                        new_points[len(new_points) - 1].X > new_points[len(points) + 1 + 1 + numPunti].X and
                        new_points[len(new_points) - 1].Y > new_points[len(points) + 1 + 1 + numPunti].Y and
                        new_points[len(new_points) - 1].Y < new_points[len(points) + 1 + numPunti].Y):
                    polygon.append(new_polygonVertices[len(new_polygonVertices) - 1])

            polygon.append(new_polygonVertices[len(polygon_vertices) + numPunti + 2])

            for var in range(0, lato_intersezione[0] + 1):
                polygon.append(var)

        cutted_polygon.append(polygon)

        cutted_polygon[len(cutted_polygon) - 1] = copy(polygon)

        polygon.clear()

        return cutted_polygon
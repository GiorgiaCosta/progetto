import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.vector2d_class as VectorLibrary
import math

class IOperation:

    # @abstractmethod
    def ComputeDistance(self, point1: PointsLibrary.Points, point2: PointsLibrary.Points) -> float:
        pass

    # @abstractmethod
    def TangentVersor(self, segment: SegmentLibrary.Segment) -> VectorLibrary.Vector2d:
        pass

    # @abstractmethod
    def NormalVersor(self, segment: SegmentLibrary.Segment) -> VectorLibrary.Vector2d:
        pass

    # @abstractmethod
    def Concave(self, edge: SegmentLibrary.Segment, next_edge: SegmentLibrary.Segment, segment: SegmentLibrary.Segment) -> bool:
        pass

    # @abstractmethod
    def DotProduct(self, v1: VectorLibrary.Vector2d, v2: VectorLibrary.Vector2d) -> float:
        pass


class Operation:

    @staticmethod
    def ComputeDistance(point1, point2) -> float:

        distance = math.sqrt(
            (point1.X - point2.X) * (point1.X - point2.X) + (point1.Y - point2.Y) * (point1.Y - point2.Y))
        return distance

    @staticmethod
    def TangentVersor(segment: SegmentLibrary.Segment) -> VectorLibrary.Vector2d:

        x_segment = segment.Fine.X - segment.Inizio.X
        y_segment = segment.Fine.Y - segment.Inizio.Y
        return VectorLibrary.Vector2d(x_segment, y_segment)

    @staticmethod
    def NormalVersor(segment: SegmentLibrary.Segment) -> VectorLibrary.Vector2d:
        tang_versor: VectorLibrary.Vector2d = Operation.TangentVersor(segment)
        x_segment = -tang_versor.Y
        y_segment = tang_versor.X
        return VectorLibrary.Vector2d(x_segment, y_segment)

    @staticmethod
    def Concave(edge: SegmentLibrary.Segment, next_edge: SegmentLibrary.Segment, segment: SegmentLibrary.Segment) -> bool:
        concave = False

        tang_versor_segment: VectorLibrary.Vector2d = Operation.TangentVersor(segment)
        normal_versor_edge: VectorLibrary.Vector2d = Operation.NormalVersor(edge)
        normal_versor_next_edge: VectorLibrary.Vector2d = Operation.NormalVersor(next_edge)
        dot_product: float = Operation.DotProduct(tang_versor_segment, normal_versor_edge)
        dot_product_next: float = Operation.DotProduct(tang_versor_segment, normal_versor_next_edge)
        dot_productNormal: float = Operation.DotProduct(normal_versor_edge, normal_versor_next_edge)

        if (dot_product < 0) and (dot_product_next > 0) and (dot_productNormal != 0):
            concave = True

        if dot_product > 0 and dot_product_next < 0 and edge.Inizio.Y == next_edge.Inizio.Y and dot_productNormal != 0:
            concave = True
        return concave

    @staticmethod
    def DotProduct(v1: VectorLibrary.Vector2d, v2: VectorLibrary.Vector2d) -> float:

        dotProduct = v1.X * v2.X + v1.Y * v2.Y
        return dotProduct


import src.points_class as PointsLibrary
import src.segment_class as SegmentLibrary
import src.intersector_class as IntersectionLibrary


class InewItems:

    def NewPoints(self, points: [], segment: SegmentLibrary.Segment) -> []:
        pass

    def NewPolygonVertices(self, points: [], polygon_vertices: [], segment: SegmentLibrary.Segment) -> []:
        pass

class newItems(InewItems):

    @staticmethod
    def NewPoints(points: [], segment: SegmentLibrary.Segment) -> []:
        newpoints = []

        for i in range(0, len(points)):
            newpoints.append(points[i])

        for i in range(0, len(points)):
            if i == (len(points) - 1):
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[0])
            else:
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[i + 1])

            if IntersectionLibrary.Intersector.Intersection(edge, segment) is True:
                if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[i].X or IntersectionLibrary.Intersector.PointIntersection(
                        edge, segment).Y != points[i].Y):
                    intersezione: PointsLibrary.Points = IntersectionLibrary.Intersector.PointIntersection(edge, segment)
                    newpoints.append(intersezione)

        newpoints.append(segment.Fine)
        newpoints.append(segment.Inizio)


        return newpoints

    @staticmethod
    def NewPolygonVertices(points: [], polygon_vertices: [], segment: SegmentLibrary.Segment) -> []:
        newpolygon_vertices = []
        contatore: int = 0
        num_intersezione: int = 0

        for i in range(0, len(polygon_vertices)):
            newpolygon_vertices.append(polygon_vertices[i])

        for i in range(0, len(polygon_vertices)):
            if i == (len(polygon_vertices) - 1):
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[0])

                if IntersectionLibrary.Intersector.Intersection(edge, segment) is True:
                    # se intersezione non nel punto iniziale lato
                    if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                        i].X or IntersectionLibrary.Intersector.PointIntersection(
                            edge, segment).Y != points[i].Y):

                        # se intersezione non nel punto finale lato
                        if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                            0].X or IntersectionLibrary.Intersector.PointIntersection(edge, segment).Y != points[0].Y):

                            num_intersezione = len(newpolygon_vertices) - contatore
                            newpolygon_vertices.append(num_intersezione)
                        else:
                            num_intersezione = 0
                            newpolygon_vertices.append(num_intersezione)
                            contatore = contatore + 1

            else:
                edge: SegmentLibrary.Segment = SegmentLibrary.Segment(points[i], points[i + 1])

                if IntersectionLibrary.Intersector.Intersection(edge, segment) is True:
                    if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                        i].X or IntersectionLibrary.Intersector.PointIntersection(
                            edge, segment).Y != points[i].Y):

                        if (IntersectionLibrary.Intersector.PointIntersection(edge, segment).X != points[
                            i + 1].X or IntersectionLibrary.Intersector.PointIntersection(edge, segment).Y != points[
                            i + 1].Y):

                            num_intersezione = len(newpolygon_vertices) - contatore
                            newpolygon_vertices.append(num_intersezione)
                        else:
                            num_intersezione = i + 1
                            newpolygon_vertices.append(num_intersezione)
                            contatore = contatore + 1

        newpolygon_vertices.append(len(newpolygon_vertices) - contatore)
        newpolygon_vertices.append(len(newpolygon_vertices) - contatore)

        return newpolygon_vertices
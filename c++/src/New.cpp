#include "Polygon.hpp"
#include "Geometry.hpp"
#include "Eigen"

#include <iostream>
#include <iomanip>

using namespace Eigen;
using namespace std;
using namespace PolygonLibrary;


namespace GeometryLibrary {

const vector<Points> NewItems::newPoints(const vector<Points> &points, const Segment &segment) const
{
    vector<Points> newPoints; //creo nuovo vettore dove memorizzo punti di partenza e intersezioni

     newPoints.clear();
     for (unsigned int i = 0; i < points.size(); i ++)
     {
         newPoints.push_back(points[i]);
         newPoints[i].X = points[i].X;
         newPoints[i].Y = points[i].Y;
     }

     for (unsigned int i = 0; i < points.size(); i++)
     {
         if (i == points.size() - 1)  //ultimo vertice
         {
             Segment edge(points[i], points[0]);
             if (_intersector.Intersection(edge, segment) == true)
             {

                 //si considera solamante le intersezioni non nel vertice iniziale del lato
                 if (_intersector.PointIntersection(edge, segment).X != points[i].X || _intersector.PointIntersection(
                         edge, segment).Y != points[i].Y)
                 {
                     Points intersezione = _intersector.PointIntersection(edge, segment);
                     newPoints.push_back(intersezione);
                 }
             }
          }
          else
          {
             Segment edge(points[i], points[i + 1]);
             if (_intersector.Intersection(edge, segment) == true)
             {
                 if (_intersector.PointIntersection(edge, segment).X != points[i].X || _intersector.PointIntersection(
                         edge, segment).Y != points[i].Y)
                 {
                     Points intersezione = _intersector.PointIntersection(edge, segment);
                     newPoints.push_back(intersezione);
                 }
              }
           }

      }

     newPoints.push_back(segment._fine);     //aggiungo coordinate al vettore newpoints

     newPoints.push_back(segment._inizio);   //aggiungo coordinate al vettore newpoints


     for (unsigned int i = 0; i < newPoints.size(); i++)
     {   std::cout << std::setprecision(2);    //per stampare una sola cifra decimale

          if (i == 0)
          {
              std::cout <<" Punti: {("<<newPoints[i].X;
              std::cout <<","<<newPoints[i].Y;
          }

          if (i != 0 && i != newPoints.size() - 1)
          {

              std::cout <<"), ("<<newPoints[i].X;

              std::cout <<","<<newPoints[i].Y;

          }

          if (i == newPoints.size() - 1)
          {
              std::cout <<"), ("<<newPoints[i].X;
              std::cout <<","<<newPoints[i].Y;
              std::cout <<")}";
          }
     }

     std::cout<<"\n";

     return newPoints;
}


const vector<int> NewItems::newPolygonVertices(const vector<Points>& points, const vector<int>& polygonVertices, const Segment& segment) const
{
    vector<int> newpolygonVertices;
    int contatore = 0;          //numero intersezione coincidente con vertice finale
    int num_intersezione = 0;

    for(unsigned int i=0; i < polygonVertices.size(); i++) //copio elementi di polygonvertices in newpolygonvertices
        newpolygonVertices.push_back(polygonVertices[i]);

    for(unsigned int i=0; i < polygonVertices.size(); i++){
        if (i == polygonVertices.size() - 1){
            PolygonLibrary::Segment edge = PolygonLibrary::Segment(points[i], points[0]);

            if(_intersector.Intersection(edge, segment) == true){
                //se intersezione non nel punto iniziale lato
                if (_intersector.PointIntersection(edge, segment).X != points[i].X or _intersector.PointIntersection(edge, segment).Y != points[i].Y){

                    //se intersezione non nel punto finale lato
                    if (_intersector.PointIntersection(edge, segment).X != points[0].X or _intersector.PointIntersection(edge, segment).Y != points[0].Y){
                        num_intersezione = newpolygonVertices.size() - contatore;
                        newpolygonVertices.push_back(num_intersezione);

                    }else{
                        num_intersezione = 0;
                        newpolygonVertices.push_back(num_intersezione);
                        contatore = contatore + 1;

                    }
                }
            }
        }else{
            PolygonLibrary::Segment edge = PolygonLibrary::Segment(points[i], points[i+1]);

            if(_intersector.Intersection(edge, segment) == true){
                if (_intersector.PointIntersection(edge, segment).X != points[i].X or _intersector.PointIntersection(edge, segment).Y != points[i].Y){

                    if (_intersector.PointIntersection(edge, segment).X != points[i + 1].X or _intersector.PointIntersection(edge, segment).Y != points[i + 1].Y){
                        num_intersezione = newpolygonVertices.size() - contatore;
                        newpolygonVertices.push_back(num_intersezione);
                    }else{
                        num_intersezione = i + 1;
                        newpolygonVertices.push_back(num_intersezione);
                        contatore = contatore + 1;
                    }


                }

            }

        }

    }

    //si aggiungono i numeri degli estremi del vertice
    newpolygonVertices.push_back(newpolygonVertices.size() - contatore);
    newpolygonVertices.push_back(newpolygonVertices.size() - contatore);

    return newpolygonVertices;

}
}

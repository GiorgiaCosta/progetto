#include "Geometry.hpp"
#include "Polygon.hpp"
#include "Eigen"

#include <iostream>
#include <iomanip> //per stampare numero di cifre decimali a scelta
#include <math.h>

using namespace Eigen;
using namespace PolygonLibrary;


namespace GeometryLibrary {


const vector<vector<int>> CutPolygon::cuttedConvexPolygons(const vector<Points> &points, const vector<int> &polygon_vertices, const Segment &segment) const  ///con due intersezioni
{
    vector<int> polygon1;
    vector<int> polygon2;

    vector<vector<int>> cutted_polygon;
    vector<int> lato_intersezione;

    vector<Points> new_points = _newItem.newPoints(points, segment);
    vector<int> new_polygonvertices = _newItem.newPolygonVertices(points, polygon_vertices, segment);

    //memorizzo lati in cui trovo intersezione
    for (unsigned int i = 0; i < points.size(); i++)
    {
        if (i == points.size() - 1)
        {   Segment edge(points[i], points[0]);
            if (_intersector.Intersection(edge, segment) == true)
               {
                   if (_intersector.PointIntersection(edge, segment).X != points[i].X || _intersector.PointIntersection(
                    edge, segment).Y != points[i].Y)
                    lato_intersezione.push_back(i);
               }
        }
        else
        {
            Segment edge(points[i], points[i + 1]);
            if (_intersector.Intersection(edge, segment) == true)
            {
                if (_intersector.PointIntersection(edge, segment).X != points[i].X || _intersector.PointIntersection(
                    edge, segment).Y != points[i].Y)
                    lato_intersezione.push_back(i);
            }
        }
    }
   //////////////
                                            //CREO PRIMO POLIGONO
    for (int i = 0; i < lato_intersezione[0] + 1; i++)
        polygon1.push_back(new_polygonvertices[i]);

    polygon1.push_back(new_polygonvertices[(polygon_vertices.size())]); //aggiungo prima intersezione

    if(segment._fine.X > segment._inizio.X)
    {
        polygon1.push_back(new_polygonvertices[new_polygonvertices.size() - 1]);   // aggiungo punto iniziale
        polygon1.push_back(new_polygonvertices[new_polygonvertices.size() - 2]);
    }

    if(segment._fine.X < segment._inizio.X)
    {
        polygon1.push_back(new_polygonvertices[new_polygonvertices.size() - 2]);
        polygon1.push_back(new_polygonvertices[new_polygonvertices.size() - 1]);
    }

    //caso particolare ottagono con segmento verticale
    if (segment._fine.X == segment._inizio.X)
    {
        if (segment._fine.Y < segment._inizio.Y)
        {
            polygon1.push_back(new_polygonvertices[(new_polygonvertices).size() - 1]);
            polygon1.push_back(new_polygonvertices[(new_polygonvertices).size() - 2]);
        }
        else
        {
            polygon1.push_back(new_polygonvertices[(new_polygonvertices).size() - 2]);
            polygon1.push_back(new_polygonvertices[(new_polygonvertices).size() - 1]);
        }
     }

   //se la seconda intersezione non coincide con lo 0
    if (new_points[(polygon_vertices).size() + 1].X != new_points[0].X || new_points[(polygon_vertices).size() + 1].Y != new_points[0].Y)
        polygon1.push_back(new_polygonvertices[(polygon_vertices).size() + 1]);         //aggiungo seconda intersezione

    //se la seconda (e ultima) intersezione  NON è nel lato finale
    if (lato_intersezione[1] != polygon_vertices.size() - 1)
    {
        Segment edge(points[lato_intersezione[1]], points[lato_intersezione[1] + 1]);

        //se intersezione non nel punto finale del lato
        if (_intersector.PointIntersection(edge, segment).X != points[lato_intersezione[1] + 1].X || _intersector.PointIntersection(
                edge, segment).Y != points[lato_intersezione[1] + 1].Y)
        {
            for (int i = lato_intersezione[1]; i < polygon_vertices[(polygon_vertices).size() - 1]; i++)
                polygon1.push_back(new_polygonvertices[i + 1]);
        }
        else //altrimenti non aggiungo quel vertice
        {
            for (int i = lato_intersezione[1]+1; i < polygon_vertices[(polygon_vertices).size() - 1]; i++)
                polygon1.push_back(new_polygonvertices[i + 1]);
        }
    }
    cutted_polygon.push_back(polygon1);


                                      //CREO SECONDO POLIGONO
    // se numero intersezione >= numero vertici cioe intersezione è un vertice
    if (new_polygonvertices[polygon_vertices.size()] >= (polygon_vertices.size()))
        polygon2.push_back(new_polygonvertices[polygon_vertices.size()]);        //aggiungo prima intersezione

    for (int i = lato_intersezione[0]; i < lato_intersezione[1]; i++)
        polygon2.push_back(new_polygonvertices[i+1]);

    polygon2.push_back(new_polygonvertices[(polygon_vertices).size() + 1]);      //aggiungo seconda intersezione

    if (segment._fine.X > segment._inizio.X)
    {
        polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 2]);  // aggiungo punto finale
        polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 1]);
    }
    if (segment._fine.X < segment._inizio.X)
    {
        polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 1]);
        polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 2]);
    }

    //caso particolare segmento verticale (ottagono)
    if (segment._fine.X == segment._inizio.X)
    {
        if (segment._fine.Y < segment._inizio.Y)
        {
            polygon2.push_back(new_polygonvertices[(new_polygonvertices.size()) - 2]);
            polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 1]);
        }
        else
        {
            polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 1]);
            polygon2.push_back(new_polygonvertices[new_polygonvertices.size() - 2]);
        }
     }
    cutted_polygon.push_back(polygon2);
    return cutted_polygon;
}



const vector<vector<int>> CutPolygon::cuttedConcavePolygons(const vector<Points> &points, const vector<int> &polygonVertices, const Segment &segment) const
{
  vector<int> polygon;
  vector<vector<int>> cuttedPolygon;
  vector<Points> new_points = _newItem.newPoints(points, segment);
  vector<int> new_polygonVertices = _newItem.newPolygonVertices(points, polygonVertices, segment);
  vector<int> lato_intersezione;

  PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
  PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
  PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
  GeometryLibrary::Operation _operation(_point, _segment, _vector);

  ///memorizzo lati in cui ho trovato intersezione
  for (unsigned int i = 0; i < points.size(); i++){
    if (i != points.size() - 1)
    {
       Segment edge(points[i], points[i+1]);
       if (_intersector.Intersection(edge, segment) == true){
          if(_intersector.PointIntersection(edge, segment).X == points[i+1].X && _intersector.PointIntersection(edge, segment).Y == points[i+1].Y ){
            lato_intersezione.push_back(i);
            i++;
           }
           else
            lato_intersezione.push_back(i);
        }
        }

        if (i == points.size() - 1)
        {
          Segment edge(points[i], points[0]);
          if (_intersector.Intersection(edge, segment) == true){
             if((_intersector.PointIntersection(edge,segment).X != (points[0].X)) || (_intersector.PointIntersection(edge, segment).Y != (points[0].Y )))
               lato_intersezione.push_back(i);
           }
        }
  }

//////////////////////////////
 int next_vertex;
 int num = 0;
 int third_intersection;
 bool consideroConcavo = true;
 int controllo = 0;

 for(unsigned int i = 0; i < lato_intersezione.size() - 1; i++){

      Segment edge(new_points[lato_intersezione[i] + 1], new_points[lato_intersezione[i]]);
      Segment next_edge(new_points[lato_intersezione[i] + 2], new_points[lato_intersezione[i] + 1]);
      Segment previous_edge(new_points[lato_intersezione[i]], new_points[lato_intersezione[i] - 1]);
      Segment previous_previous_edge(new_points[lato_intersezione[i] - 1], new_points[lato_intersezione[i] - 2]);

      unsigned int verticeDaNonConsiderare = 0;

     if (i > 1)
     {    //per non creare il poligono creato del 5
       if(_operation.concave(previous_edge, edge, segment) == false && _operation.concave(previous_previous_edge, previous_edge, segment) == true && (new_polygonVertices[i + 1] == lato_intersezione[i] - 1))
           verticeDaNonConsiderare = 1;

      }

      //se l'intersezione (8)coincide con un vertice e quel vertice è concavo allora lo considero convesso cosi poi entra in quel metodo
      if(new_points[points.size() + i].X == new_points[lato_intersezione[i] +1].X && new_points[points.size() + i].Y == new_points[lato_intersezione[i] +1].Y && _operation.concave(edge, next_edge, segment) == true){
          consideroConcavo = false;
      }

      if(verticeDaNonConsiderare == 0)
      {
                                            ////////    CASO VERTICE CONCAVO    //////////

        if(_operation.concave(edge, next_edge, segment) == true && consideroConcavo == true){
            polygon.push_back(new_polygonVertices[lato_intersezione[i] +1]);        //aggiungo 3
            polygon.push_back(new_polygonVertices[polygonVertices.size() + i + 1]);

           if (segment._fine.Y > segment._inizio.Y)
           {
            if(new_points[new_points.size() - 2].X < new_points[points.size() +i + 1].X && new_points[new_points.size() - 2].X > new_points[points.size() + i + 2].X && new_points[new_points.size() - 2].Y > new_points[points.size() +i + 2].Y && new_points[new_points.size() - 2].Y < new_points[points.size() + i + 1].Y)
                polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

            if(new_points[new_points.size() - 1].X < new_points[points.size() +i + 1].X && new_points[new_points.size() - 1].X > new_points[points.size() + i + 2].X && new_points[new_points.size() - 1].Y > new_points[points.size() +i + 2].Y && new_points[new_points.size() - 1].Y < new_points[points.size() + i + 1].Y)
                polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
           }

           if (segment._fine.Y < segment._inizio.Y)
           {
              if(new_points[new_points.size() - 2].X < new_points[points.size() +i + 1].X && new_points[new_points.size() - 2].X > new_points[points.size() + i + 2].X && new_points[new_points.size() - 2].Y < new_points[points.size() +i + 2].Y && new_points[new_points.size() - 2].Y > new_points[points.size() + i + 1].Y)
                 polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

              if(new_points[new_points.size() - 1].X < new_points[points.size() +i + 1].X && new_points[new_points.size() - 1].X > new_points[points.size() + i + 2].X && new_points[new_points.size() - 1].Y < new_points[points.size() +i + 2].Y && new_points[new_points.size() - 1].Y > new_points[points.size() + i + 1].Y)
                 polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
            }

            polygon.push_back(new_polygonVertices[polygonVertices.size() + i + 2]); // aggiungo 5

            ///devo aggiungere i punti da 6 a 7 (tra le due intersezioni)

            unsigned int next_next_intersection = new_polygonVertices[polygonVertices.size() + i + 2]; //numero intersezione a cui si è giunti
            num = lato_intersezione[i + 3] - lato_intersezione[i + 2];
            next_vertex = lato_intersezione[i + 2] + 1;
            third_intersection = new_polygonVertices[polygonVertices.size() + i + 3];

            //es Vicini
            if(lato_intersezione[i + 2] == polygonVertices[polygonVertices.size() - 1]){  //se l'intersezione (9) si trova nell'ultimo lato
                num = lato_intersezione[i - 1] + 1;
                next_vertex = 0;
                third_intersection = new_polygonVertices[polygonVertices.size() + i - 1];
                controllo = 1;
            }

            if(next_next_intersection < polygonVertices.size()){  //se l'intesezione (5) coincide con un vertice
                next_vertex = next_vertex + 1;
                num = num - 1;
                controllo = 1;
            }

            if(controllo == 0){
                num = lato_intersezione[0] + points.size() - lato_intersezione[lato_intersezione.size() - 1] - 1;
                next_vertex = 0;
                third_intersection = new_polygonVertices[polygonVertices.size() + i - 1];

                for(unsigned int x = lato_intersezione[lato_intersezione.size() - 1]; x < polygonVertices.size() - 1; x++)
                    polygon.push_back(lato_intersezione[lato_intersezione.size() - 1] + 1);
            }



            int cicli = next_vertex + num;

            for(int n = next_vertex; n < cicli; n++){
               polygon.push_back(n);
            }


            polygon.push_back(third_intersection);

            // Creo questa variabile e questo for per il poligono che parte dal 3 e arriva al 10.
            int numIntersezioniInMezzo = 0;
            for (unsigned int k = points.size(); new_polygonVertices[k] != third_intersection; k++)
            {
                numIntersezioniInMezzo++;
            }

            if (i == 0)
            {
               if (segment._fine.Y > segment._inizio.Y)
               {
                  if(new_points[new_points.size() - 2].X < new_points[points.size()+i].X && new_points[new_points.size() - 2].X > new_points[points.size() + numIntersezioniInMezzo].X && new_points[new_points.size() - 2].Y < new_points[points.size() +i].Y && new_points[new_points.size() - 2].Y > new_points[points.size() + numIntersezioniInMezzo].Y)
                      polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

                  if(new_points[new_points.size() - 1].X < new_points[points.size() +i].X && new_points[new_points.size() - 1].X > new_points[points.size() + numIntersezioniInMezzo].X && new_points[new_points.size() - 1].Y < new_points[points.size() +i].Y && new_points[new_points.size() - 1].Y > new_points[points.size() + numIntersezioniInMezzo].Y)
                      polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);

                }
               if (segment._fine.Y < segment._inizio.Y)
                 {
                   if(new_points[new_points.size() - 2].X < new_points[points.size() +i].X && new_points[new_points.size() - 2].X > new_points[points.size() + i].X && new_points[new_points.size() - 2].Y < new_points[points.size() +i].Y && new_points[new_points.size() - 2].Y > new_points[points.size() + i -1].Y)
                       polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

                   if(new_points[new_points.size() - 1].X < new_points[points.size() +i - 1].X && new_points[new_points.size() - 1].X > new_points[points.size() + i].X && new_points[new_points.size() - 1].Y < new_points[points.size() +i].Y && new_points[new_points.size() - 1].Y > new_points[points.size() + i - 1].Y)
                       polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
                 }
            }
            if (i != 0)
            {
              if (segment._fine.Y > segment._inizio.Y)
              {
                if(new_points[new_points.size() - 2].X < new_points[points.size() +i - 1].X && new_points[new_points.size() - 2].X > new_points[points.size() + i].X && new_points[new_points.size() - 2].Y > new_points[points.size() +i].Y && new_points[new_points.size() - 2].Y < new_points[points.size() + i -1].Y)
                    polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

                if(new_points[new_points.size() - 1].X < new_points[points.size() +i - 1].X && new_points[new_points.size() - 1].X > new_points[points.size() + i].X && new_points[new_points.size() - 1].Y > new_points[points.size() +i].Y && new_points[new_points.size() - 1].Y < new_points[points.size() + i - 1].Y)
                    polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
              }

            if (segment._fine.Y < segment._inizio.Y)
              {
                if(new_points[new_points.size() - 2].X < new_points[points.size() +i - 1].X && new_points[new_points.size() - 2].X > new_points[points.size() + i].X && new_points[new_points.size() - 2].Y < new_points[points.size() +i].Y && new_points[new_points.size() - 2].Y > new_points[points.size() + i -1].Y)
                    polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

                if(new_points[new_points.size() - 1].X < new_points[points.size() +i - 1].X && new_points[new_points.size() - 1].X > new_points[points.size() + i].X && new_points[new_points.size() - 1].Y < new_points[points.size() +i].Y && new_points[new_points.size() - 1].Y > new_points[points.size() + i - 1].Y)
                    polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
              }
            }

            polygon.push_back(new_polygonVertices[polygonVertices.size() + i]);

            cuttedPolygon.push_back(polygon);
            polygon.clear();
        }

                                  //////////// CASO VERTICE CONVESSO /////////////

        if((_operation.concave(edge, next_edge, segment) == false) || (_operation.concave(edge, next_edge, segment) == true && consideroConcavo == false)){

            polygon.push_back(new_polygonVertices[polygonVertices.size() + i]); //prima intersezione
            next_vertex = lato_intersezione[i] + 1;
            num = lato_intersezione[i + 1] - lato_intersezione[i];

            if(new_polygonVertices[polygonVertices.size() + i] == new_polygonVertices[new_polygonVertices.size() - 3]){
                next_vertex = 0;
                num = lato_intersezione[0] + 1; //(poligono 12-0-1-2-10-8)
            }



            int verticeCoincideConInter = 0;
            for (unsigned j = 0; j < polygonVertices.size(); j++)
            {
              if (new_polygonVertices[polygonVertices.size() + i] == polygonVertices[j])  //se intersezione coincide con il vertice (8,9,12) non da ripetere

                  verticeCoincideConInter = 1;
            }

            for(int n = next_vertex + verticeCoincideConInter ; n < next_vertex + num; n++)  //aggiungo punti tra le due intersezioni
                polygon.push_back(new_polygonVertices[n]);


            verticeCoincideConInter = 0;
            polygon.push_back(new_polygonVertices[polygonVertices.size() + i + 1]);  //aggiungo l'intersezione successiva


          //Se il segmento è "inclinato verso destra", allora devo aggiungere prima
          // il punto finale e poi quello iniziale (eventualmente)

          if (segment._fine.Y > segment._inizio.Y)
          {
            if(new_points[new_points.size() - 2].X > new_points[points.size() + i + 1].X && new_points[new_points.size() - 2].X < new_points[points.size() + i].X && new_points[new_points.size() - 2].Y > new_points[points.size() + i +1].Y && new_points[new_points.size() - 2].Y < new_points[points.size() + i].Y)
            {
               polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);
            }

            if(new_points[new_points.size() - 1].X > new_points[points.size() + i + 1].X && new_points[new_points.size() - 1].X < new_points[points.size() + i].X && new_points[new_points.size() - 1].Y > new_points[points.size() + i + 1].Y && new_points[new_points.size() - 1].Y < new_points[points.size() + i].Y)
            {
               polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
            }
          }

          //Se il segmento è "inclinato verso sinistra", allora devo aggiungere prima
          // il punto iniziale e poi quello finale (eventualmente)

          else if(segment._fine.Y < segment._inizio.Y)
          {
             if(new_points[new_points.size() - 1].X > new_points[points.size() + i + 1].X && new_points[new_points.size() - 1].X < new_points[points.size() + i].X && new_points[new_points.size() - 1].Y > new_points[points.size() + i].Y && new_points[new_points.size() - 1].Y < new_points[points.size() + i + 1].Y)
             {
                 polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
             }
             if(new_points[new_points.size() - 2].X > new_points[points.size() + i + 1].X && new_points[new_points.size() - 2].X < new_points[points.size() + i].X && new_points[new_points.size() - 2].Y > new_points[points.size() + i].Y && new_points[new_points.size() - 2].Y < new_points[points.size() + i + 1].Y)
             {
                 polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);
             }
          }
          cuttedPolygon.push_back(polygon);
          polygon.clear();
        }
    }
    }

   ///A questo punto si verifica che effettivamente ci siano tutti i poligoni:
   /// per fare questo controllo, verifico che in almeno un poligono tagliato ci sia lo zero.

    unsigned int fineCiclo = 0;
    for (unsigned int i = 0; i < cuttedPolygon.size(); i++)
    {
       for (unsigned int k = 0; k < cuttedPolygon[i].size(); k++)
       {
          if(cuttedPolygon[i][k] == 0)
          {
            fineCiclo = 1;
          }
        }
    }

    int var = 0;
    unsigned int numPunti = 0;

    if (fineCiclo == 0)
    {
        polygon.push_back(new_polygonVertices[points.size()]);  //aggiungo il 10


        for(var = points.size(); new_points[var + 1].X > new_points[points.size()].X; var++)  //numero intersezioni tra intersezione 10 e 8, cioè 2
        {
            numPunti++;
        }

        //controllo su punto finale
       if (segment._fine.Y > segment._inizio.Y)
       {
         if(new_points[new_points.size() - 2].X < new_points[points.size()].X && new_points[new_points.size() - 2].X > new_points[points.size() + numPunti + 1].X && new_points[new_points.size() - 2].Y > new_points[points.size() + numPunti + 1].Y && new_points[new_points.size() - 2].Y < new_points[points.size()].Y)
           polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);

         if(new_points[new_points.size() - 1].X < new_points[points.size()].X && new_points[new_points.size() - 1].X > new_points[points.size() + numPunti + 1].X && new_points[new_points.size() - 1].Y > new_points[points.size() + 1 + numPunti].Y && new_points[new_points.size() - 1].Y < new_points[points.size()].Y)
           polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
        }

       polygon.push_back(new_polygonVertices[points.size() + 1 + numPunti]); // aggiungo 8

      //controllo su punto iniziale
      if (segment._fine.Y > segment._inizio.Y)
      {
       if(new_points[new_points.size() - 2].X < new_points[points.size() + numPunti + 1].X && new_points[new_points.size() - 2].X > new_points[points.size() + 2 + numPunti].X && new_points[new_points.size() - 2].Y > new_points[points.size() +2 + numPunti].Y && new_points[new_points.size() - 2].Y < new_points[points.size() + 1 + numPunti].Y)
           polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 2]);
       if(new_points[new_points.size() - 1].X < new_points[points.size() + numPunti + 1].X && new_points[new_points.size() - 1].X > new_points[points.size() + 1 + 1 + numPunti].X && new_points[new_points.size() - 1].Y > new_points[points.size() + 1 + 1 + numPunti].Y && new_points[new_points.size() - 1].Y < new_points[points.size() + 1 + numPunti].Y)
           polygon.push_back(new_polygonVertices[new_polygonVertices.size() - 1]);
      }

      polygon.push_back(new_polygonVertices[polygonVertices.size() + numPunti + 2]); // aggiungo 12

      for (var = 0; var < lato_intersezione[0] + 1; var++)
          polygon.push_back(var); // aggiungo 0,1,2



      cuttedPolygon.push_back(polygon);
      polygon.clear();
 }


    std::cout << std::setprecision(2);
    for (unsigned int i = 0; i < cuttedPolygon.size(); i++)
    {
        for (unsigned int k = 0; k < cuttedPolygon[i].size(); k++)
        {
            if (k == 0)
               std::cout <<" - {";

            std::cout <<""<<cuttedPolygon[i][k];

            if (k != cuttedPolygon[i].size() - 1)
                std::cout <<", ";

            if (k == cuttedPolygon[i].size() - 1)
                std::cout <<"}"<<"\n";

        }
    }
    return cuttedPolygon;
}

/*const vector<Points> Mash::traslationOrizzontale(vector<Points> &poly, double t) const
{
    for(unsigned int i = 0; i < poly.size(); i++){
        poly[i].X = poly[i].X + t;
    }
    return poly;
}

const vector<Points> Mash::traslationVerticale(vector<Points> &poly, double t) const
{
    for(unsigned int i = 0; i < poly.size(); i++){
        poly[i].Y = poly[i].Y + t;
    }
    return poly;
}

const vector<Points> Mash::verticiBox(const vector<Points> &poly) const
{
    double x_max = poly[0].X ;
    double x_min = poly[0].X;
    double y_max = poly[0].Y;
    double y_min = poly[0].Y;


    for (unsigned int i = 0; i < poly.size(); i++){
        if (poly[i].X > x_max)
            x_max = poly[i].X;
        if (poly[i].X < x_min)
            x_min = poly[i].X;
        if (poly[i].Y > y_max)
            y_max = poly[i].Y;
        if (poly[i].Y < y_min)
            y_min = poly[i].Y;
    }

    vector<PolygonLibrary::Points> vertici_box;

    vertici_box.push_back(PolygonLibrary::Points(x_min, y_min));
    vertici_box[0] = PolygonLibrary::Points(x_min, y_min);


    vertici_box.push_back(PolygonLibrary::Points(x_max, y_min));
    vertici_box[1] = PolygonLibrary::Points(x_max, y_min);

    vertici_box.push_back(PolygonLibrary::Points(x_max, y_max));
    vertici_box[2] = PolygonLibrary::Points(x_max, y_max);

    vertici_box.push_back(PolygonLibrary::Points(x_min, y_min));
    vertici_box[3] = PolygonLibrary::Points(x_min, y_max);

    return vertici_box;
}



const vector<vector<Points>> Mash::CutMash(const vector<Points> &poly) const
{
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _method(_point, _segment, _vector);
    GeometryLibrary::Intersector _intersector(_method);
    GeometryLibrary::NewItems _new(_intersector, _method);
    GeometryLibrary::CutPolygon _cutter(_intersector, _new);
    GeometryLibrary::Mash _mash(_cutter);

    vector<int> polygonvertices_poly;
    polygonvertices_poly.reserve(poly.size());

    vector<int> polygonvertices_box;
    polygonvertices_box.reserve(4);

    vector<Points> points;
    points.reserve(10);
    vector<vector<Points>> cutted_points;

    vector<int> vertici_intersection;


    for( unsigned int i = 0; i < poly.size(); i++)
        polygonvertices_poly.push_back(i);

    for( unsigned int i = 0; i < 4; i++)
        polygonvertices_box.push_back(i);


    double x_max = poly[0].X ;
    double x_min = poly[0].X;
    double y_max = poly[0].Y;
    double y_min = poly[0].Y;


    for (unsigned int i = 0; i < poly.size(); i++){
        if (poly[i].X > x_max)
            x_max = poly[i].X;
        if (poly[i].X < x_min)
            x_min = poly[i].X;
        if (poly[i].Y > y_max)
            y_max = poly[i].Y;
        if (poly[i].Y < y_min)
            y_min = poly[i].Y;
    }


    vector<PolygonLibrary::Points> intersection;
    vector<PolygonLibrary::Points> vertici_box;

    vertici_box = _mash.verticiBox(poly);

    for ( unsigned int i = 0; i < poly.size(); i++){

        //vettore dei punti di intersezione tra box e poly
        if (poly[i].X == x_min || poly[i].X == x_max || poly[i].Y == y_min || poly[i].Y == y_max){
            intersection.push_back(poly[i]);
            intersection[i].X = poly[i].X;
            intersection[i].Y = poly[i].Y;

            vertici_intersection.push_back(i);

        }
    }

    //scorro sulle intersezioni trovate
    for (unsigned int j = 0; j < intersection.size(); j++){

        //se lato verticale
        int flag = 0;
        if(j != 0 && j != intersection.size() - 1){
            if((intersection[j].X == intersection[j + 1].X && intersection[j].X == intersection[j - 1].X) || (intersection[j].X == intersection[j+1].X && intersection[j].X == intersection[j+2].X) || j==4){
                flag = 1;

            }
            if(j == 3 && flag == 1){
                points.push_back(intersection[j]);
                points[0].X = intersection[j].X;
                points[0].Y = intersection[j].Y;
                points.push_back(intersection[j + 2]);
                points[1].X = intersection[j + 2].X;
                points[1].Y = intersection[j + 2].Y;
                points.push_back(vertici_box[3]);
                points[2].X = vertici_box[3].X;
                points[2].Y = vertici_box[3].Y;

                cutted_points.push_back(points);
                points.clear();

                points.push_back(intersection[j + 2]);
                points[0].X = intersection[j + 2].X;
                points[0].Y = intersection[j + 2].Y;

                points.push_back(intersection[0]);
                points[1].X = intersection[0].X;
                points[1].Y = intersection[0].Y;

                points.push_back(vertici_box[0]);
                points[2].X = vertici_box[0].X;
                points[2].Y = vertici_box[0].Y;

                cutted_points.push_back(points);
                points.clear();

                j = j + 2;

}
        }

        if(flag == 0){
        unsigned int i = j;
        if ( j == intersection.size() - 1){  // se su lato finale
           i =  - 1;   // seconda intersezione su primo lato cosi punto finale edge diventa intersection[0]
        }

        PolygonLibrary::Segment edge(intersection[j], intersection[i+1]);

        points.push_back(intersection[j]);  //aggiungo prima intersezione
        points[0].X = intersection[j].X;
        points[0].Y = intersection[j].Y;


        //aggiungo punti centrali

        if (i == j){  // se non lato finale
           for (int z = polygonvertices_poly[vertici_intersection[j]]; z < polygonvertices_poly[vertici_intersection[j + 1]] - 1; z++){

            points.push_back(poly[z + 1]);
            points[points.size()].X = (poly[z + 1]).X;
            points[points.size()].Y = (poly[z + 1]).Y;

        }
           points.push_back(intersection[j + 1]);  //seconda intersezione
           points[points.size()].X = intersection[j+ 1].X;
           points[points.size()].Y = intersection[j + 1].Y;
        }

        else{
            points.push_back(intersection[0]);     //seconda intersezione
            points[points.size()].X = intersection[0].X;
            points[points.size()].Y = intersection[0].Y;
        }


        if ( j != intersection.size() - 1){  // se non su lato finale
            points.push_back(vertici_box[j + 1]);
            points[points.size()].X = vertici_box[j + 1].X;
            points[points.size()].Y = vertici_box[j + 1].Y;
        }

        if ( j == intersection.size() - 1){  // se su lato finale
            points.push_back(vertici_box[0]);
            points[points.size()].X = vertici_box[0].X;
            points[points.size()].Y = vertici_box[0].Y;
        }

        cutted_points.push_back(points);
        points.clear();
        }
    }
    cutted_points.push_back(poly);
    return cutted_points;

}

const vector<vector<Points>> Mash::CreateMash(vector<Points> &poly) const
{
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _method(_point, _segment, _vector);
    GeometryLibrary::Intersector _intersector(_method);
    GeometryLibrary::NewItems _new(_intersector, _method);
    GeometryLibrary::CutPolygon _cutter(_intersector, _new);
    GeometryLibrary::Mash _mash(_cutter);

    vector<Points> demain;
    int ripetizioni_base = 0;
    int ripetizioni_altezza = 0;
    double resto_base = 0.0;
    double resto_altezza = 0.0;
    double altezza_demain = 0.0;
    double altezza_box = 0.0;
    int box_interi = 0.0;
    double area = 0.0;
    vector<vector<Points>> full_mash;
    full_mash.reserve(1000);
    vector<vector<Points>> box_mash;

    vector<Points> box = _mash.verticiBox(poly); // creo poligono box

    demain.push_back(PolygonLibrary::Points(0.0,0.0));
    demain.push_back(PolygonLibrary::Points(4*(box[1].X - box[0].X) + 1,0.0));
    demain.push_back(PolygonLibrary::Points(4*(box[1].X - box[0].X) + 1, 3*(box[2].Y - box[1].Y) + 1));
    demain.push_back(PolygonLibrary::Points(0.0, 3*(box[2].Y - box[1].Y) + 1));

    //calcolo quante volte nella base
    double base_demain = demain[1].X - demain[0].X;
    double base_box = box[1].X - box[0].X;

    ripetizioni_base = int(base_demain/base_box);
    resto_base = base_demain - ripetizioni_base * base_box ;

    //calcolo quante volte nell'altezza
    altezza_demain = demain[2].Y - demain[1].Y;
    altezza_box = box[2].Y - box[1].Y;

    ripetizioni_altezza = int(altezza_demain/altezza_box);
    resto_altezza = altezza_demain - ripetizioni_altezza * altezza_box;


                             //CALCOLO AREA RETTANGOLI INTERI
    vector<vector<Points>> cutmash = _mash.CutMash(poly);

    for (unsigned int i = 0; i < cutmash.size(); i++){
        area += _method.Area(cutmash[i]);
    }

     box_interi = ripetizioni_altezza * ripetizioni_base;
     area = area * box_interi;


                             //CALCOLO AREA RETTANGOLI A DESTRA
     //creo new poly base
     vector<Points> new_polydx;


     for ( unsigned int i = 0; i < poly.size(); i++){
         new_polydx.push_back(poly[i]);    //memorizzo in new polydx i vertici di poly traslato
         new_polydx[i].X = poly[i].X - 1;
         new_polydx[i].Y = poly[i].Y - 1;

         if(new_polydx[i].X > resto_base){
             new_polydx[i].X = resto_base;
         }
     }

     vector<vector<Points>> polygon_cutted_dx = _mash.CutMash(new_polydx);
     double area_polygon_cutted_dx = 0.0;

     for ( unsigned int i = 0; i < polygon_cutted_dx.size(); i++){ //calcolo area della box sommando le aree dei poligono tagliati
         area_polygon_cutted_dx += _method.Area(polygon_cutted_dx[i]);
     }

     area = area + area_polygon_cutted_dx * ripetizioni_altezza;



                                   //CALCOLO AREA RETTANGOLI IN ALTO
     vector<Points> new_polysup;
     for ( unsigned int i = 0; i < poly.size(); i++){
         new_polysup.push_back(poly[i]);
         new_polysup[i].X = poly[i].X - 1;
         new_polysup[i].Y = poly[i].Y - 1;

         if(new_polysup[i].Y > resto_altezza){
             new_polysup[i].Y = resto_altezza;
         }
     }


     vector<vector<Points>> polygon_cutted_sup = _mash.CutMash(new_polysup);
     double area_polygon_cutted_sup = 0.0;

     for ( unsigned int i = 0; i < polygon_cutted_sup.size(); i++){
         area_polygon_cutted_sup += _method.Area(polygon_cutted_sup[i]);
     }

     area = area + 1.4 + area_polygon_cutted_sup * ripetizioni_base;

                                //CALCOLO AREA RETTANGOLO LIMITE ALTO DESTRA

        vector<Points> new_polylimite;
        for ( unsigned int i = 0; i < poly.size(); i++){
            new_polylimite.push_back(poly[i]);
            new_polylimite[i].X = poly[i].X - 1;
            new_polylimite[i].Y = poly[i].Y - 1;

            if(new_polylimite[i].Y > resto_altezza){
                 new_polylimite[i].Y = resto_altezza;
             }
            if(poly[i].X > resto_base){
                new_polylimite[i].X = resto_base;
         }
     }

     vector<vector<Points>> polygon_cutted_limite = _mash.CutMash(new_polylimite);
     double area_polygon_cutted_limite = 0.0;

     for ( unsigned int i = 0; i < polygon_cutted_limite.size(); i++){
         area_polygon_cutted_limite += _method.Area(polygon_cutted_limite[i]);
     }

     area = area + area_polygon_cutted_limite ;

     vector<Points> poly_traslation;
     vector<Points> poly_traslation_controllo;
     vector<Points> box_traslation;
     double t = 0.0;


     for (unsigned int i = 0; i < poly.size(); i++){  //memorizzo in polytraslation le coordinate dei vertici di poly
         poly_traslation.push_back(poly[i]);
         poly_traslation[i].X = poly[i].X - 1;
         poly_traslation[i].Y = poly[i].Y - 1;
     }

    for (unsigned int i = 0; i < poly.size(); i++){
         poly_traslation_controllo.push_back(poly[i]);
         poly_traslation_controllo[i].X = poly[i].X - 1;
         poly_traslation_controllo[i].Y = poly[i].Y - 1;
     }

     for (unsigned int i = 0; i < box.size(); i++){
         box_traslation.push_back(box[i]);
         box_traslation[i].X = box[i].X - 1;
         box_traslation[i].Y = box[i].Y - 1;
     }

     double toll = 10e-08;
     vector<Points> tmp;

     double x_max = poly_traslation_controllo[0].X ;
     double x_min = poly_traslation_controllo[0].X;
     double y_max = poly_traslation_controllo[0].Y;
     double y_min = poly_traslation_controllo[0].Y;

     //calcolo dei max e min (box)
     for (unsigned int i = 0; i < poly.size(); i++){
         if (poly_traslation_controllo[i].X > x_max)
             x_max = poly_traslation_controllo[i].X;
         if (poly_traslation_controllo[i].X < x_min)
             x_min = poly_traslation_controllo[i].X;
         if (poly_traslation_controllo[i].Y > y_max)
             y_max = poly_traslation_controllo[i].Y;
         if (poly_traslation_controllo[i].Y < y_min)
             y_min = poly_traslation_controllo[i].Y;
     }

     vector<Points> punti_ribaltati;
     punti_ribaltati.reserve(10);

     for(unsigned int i=0; i< poly_traslation_controllo.size(); i++){
         if(poly_traslation_controllo[i].Y == y_min){ //se sul lato inferiore della box
             poly_traslation_controllo[i].Y = y_max;

             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation[i].X;
             punti_ribaltati[i].Y = poly_traslation_controllo[i].Y;
             poly_traslation_controllo[i].Y = y_min;


         }
         if(poly_traslation_controllo[i].X == x_max){ //se sul lato destro della box
             poly_traslation_controllo[i].X = x_min;
             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation_controllo[i].X;
             punti_ribaltati[i].Y = poly_traslation[i].Y;
             poly_traslation_controllo[i].X = x_max;

         }
         if(poly_traslation_controllo[i].Y == y_max){ //se sul lato superiore della box
             poly_traslation_controllo[i].Y = y_min;
             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation[i].X;
             punti_ribaltati[i].Y = poly_traslation_controllo[i].Y;
             poly_traslation_controllo[i].Y = y_max;

         }
         if(poly_traslation_controllo[i].X == x_min){ //se sul lato sinistro della box
             poly_traslation_controllo[i].X = x_max;
             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation_controllo[i].X;
             punti_ribaltati[i].Y = poly_traslation[i].Y;
             poly_traslation_controllo[i].X = x_min;

         }

     }

     int flag = 0;
     vector<int> flag_controllo;
     flag_controllo.reserve(4);
     for(unsigned int i=0; i< punti_ribaltati.size(); i++){
         for(unsigned int j = 0; j < poly_traslation.size(); j++){
             if((punti_ribaltati[i].X == poly_traslation[j].X) && (punti_ribaltati[i].Y == poly_traslation[j].Y )){
                 flag = 1;
             }

        }
        flag_controllo.push_back(flag);

     }



     if(area - base_demain * altezza_demain < 10e-06){

         for(int i = 0; i < ripetizioni_base + 1; i++){ //ciclo sulle righe
             double s = 0.0;


             for(int j = 0; j < ripetizioni_altezza + 1; j++){ //ciclo sulle colonne

                 if(i != ripetizioni_base){
                 poly_traslation = traslationOrizzontale(poly_traslation, t);
                 t = base_box;

                    if (j != ripetizioni_altezza){
                        poly_traslation = traslationVerticale(poly_traslation, s);
                        box_traslation = traslationVerticale(box_traslation, s);
                        s = altezza_box;

                        box_mash.push_back(box_traslation);
                        int cont = 0;

                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){  //ciclo sui punti della mash
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll) && (cont == 0)){  //se i punti non coincidono
                                tmp.push_back(box_traslation[z]);      //li aggiungo a un vettore
                                cont = 1;
                             }


                         }cont =0;

                     }
                     }
                     if(i == 0 && j == 0)
                         full_mash.push_back(box_traslation);
                     else
                         full_mash.push_back(tmp);

                     tmp.clear();



                       //aggiunti a full_mash tutti i poligono tagliati
                     vector<vector<Points>> cutmash_traslation = _mash.CutMash(poly_traslation);

                     int controllo = 0;

                     for(unsigned int x = 0; x < flag_controllo.size(); x++){
                         if(flag_controllo[x]==1)
                             controllo = 1;
                     }

                     if(controllo == 0)  //se punti ribaltati non coincidono con vertici
                         for(unsigned int h = 0; h < cutmash_traslation.size() ; h++)
                            full_mash.push_back(cutmash_traslation[h]);

                     else{
                         vector<Points> polygon;
                         for(unsigned int h = 0; h < cutmash_traslation.size() ; h++){
                             for(unsigned int k=0; k < cutmash_traslation[h].size(); k++){ //ciclo su tutti i punti dei poligoni
                                 if(flag_controllo[1] == 0){  //se flag =0 per quel punto allora lo aggiungo

                                     polygon.push_back(cutmash_traslation[h][k]);
                                 }

                 }
                         }
                     full_mash.push_back(polygon);
                     }}

}
                 if( j == ripetizioni_altezza){

                     vector<Points> poly_alto;
                     poly_alto = traslationVerticale(new_polysup, ripetizioni_altezza * altezza_box);

                     box_traslation = verticiBox(poly_alto);

                     int cont = 0;
                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r ++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll) && (cont == 0)){ //se i punti non coincidono
                                 tmp.push_back(box_traslation[z]);
                                 cont = 1;
                             }
                          }

                      }cont =0;

                     }
                        full_mash.push_back(tmp);
                        tmp.clear();


                     for(unsigned int a = 0; a < polygon_cutted_sup.size(); a++){

                         polygon_cutted_sup[a] = traslationVerticale(polygon_cutted_sup[a], ripetizioni_altezza * altezza_box);
                         full_mash.push_back(polygon_cutted_sup[a]);
                     }

                     poly_traslation = traslationVerticale(poly_traslation, (-ripetizioni_altezza)*altezza_box); //per ritornare al polygono con coordinate X della base
                     box_traslation = traslationVerticale(box_traslation, (-ripetizioni_altezza)*altezza_box);
                }


             if (i == ripetizioni_base){  //se sui poligoni tagliati a destra


                 if( j != ripetizioni_altezza){

                     vector<Points> poly_dx;
                     poly_dx = traslationOrizzontale(new_polydx, ripetizioni_base * base_box);
                     poly_dx = traslationVerticale(poly_dx, altezza_box * j);

                     box_traslation = verticiBox(poly_dx);

                     int cont = 0;
                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r ++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll)  && (cont == 0)){ //se i punti non coincidono
                                 tmp.push_back(box_traslation[z]);      //li aggiungo a un vettore
                                 cont = 1;
                             }
                         }
                     } cont = 0;
                     }
                        full_mash.push_back(tmp);
                        tmp.clear();

                        //aggiungo poligono tagliati
                    for(unsigned int a = 0; a < polygon_cutted_dx.size(); a++){
                        polygon_cutted_dx[a] = traslationOrizzontale(polygon_cutted_dx[a], base_box * ripetizioni_base);
                        full_mash.push_back(polygon_cutted_dx[a]);
                 }

                 }

                 if( j == ripetizioni_altezza){
                     vector<Points> poly_limite;
                     poly_limite = traslationOrizzontale(new_polylimite, ripetizioni_base * base_box);
                     poly_limite = traslationVerticale(new_polylimite, altezza_box * j);

                     box_traslation = verticiBox(poly_limite);

                     int cont = 0;
                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r ++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){  //ciclo sui punti della mash
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll)  && (cont == 0)){  //se i punti non coincidono
                                 tmp.push_back(box_traslation[z]);      //li aggiungo a un vettore
                                 cont= 1;
                             }
                         }
                     } cont = 0;
                     }
                        full_mash.push_back(tmp);
                        tmp.clear();

                      //aggiungo poligoni tagliati
                     for(unsigned int a = 0; a < polygon_cutted_limite.size(); a++){
                         polygon_cutted_limite[a] = traslationOrizzontale(polygon_cutted_limite[a], base_box * ripetizioni_base);
                         polygon_cutted_limite[a] = traslationVerticale(polygon_cutted_limite[a], altezza_box * j);
                         full_mash.push_back(polygon_cutted_limite[a]);
                     }
                 }
             }
             }
         }
     }

     return full_mash;
}*/

}

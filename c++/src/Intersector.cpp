#include "Polygon.hpp"
#include "Geometry.hpp"
#include "Eigen"


using namespace Eigen;
using namespace PolygonLibrary;

namespace GeometryLibrary {

const Points Intersector::PointIntersection(const Segment &edge, const Segment &segment) const
{
    Vec2d ver = _operation.tangent_versor(segment);
    Vec2d ver2 = _operation.tangent_versor(edge);
    Matrix2d mtx_tang;
    Matrix2d mtx_tang_inversa;
    Points p(0, 0);    //creo punto p inizializzato a x=0, y=0 dove memorizzo i punti di intersezione che trovo (da ritronare)


    mtx_tang <<  ver2._x, -ver._x,
                 ver2._y , -ver._y;
    mtx_tang_inversa << -ver._y, ver._x,
                   -ver2._y , ver2._x;
    double det = mtx_tang.determinant();

    Eigen::Vector2d b;                  //A*x = b => matx_tang_inversa * s = b
    b << segment._inizio.X - edge._inizio.X,
         segment._inizio.Y - edge._inizio.Y ;
    Eigen::Vector2d s = Eigen::Vector2d::Zero();
    s = mtx_tang_inversa * b;
    s /= det;                           //soluzione sistema divisa per determinante mtx di partenza = coordinata curvilinea


    float toleranceIntersection = 1e-6;

    if((s(0) > -toleranceIntersection) && (s(0) < toleranceIntersection))       /// punto di intersezione è punto inizio lato
    {
        s(0) = 0.0;
        p = edge._inizio;
     }
     else if ((s(0) > 1.0 - toleranceIntersection) && (s(0) < 1.0 + toleranceIntersection))      /// punto di intersezione è punto fine lato
    {
        s(0) = 1.0;
        p = edge._fine;
    }
     else if ((s(0) > -toleranceIntersection  && s(0)-1.0 < toleranceIntersection))    /// punto di intersezione è interno al lato 0 < s < 1
    {
        p = Points(edge._inizio.X + ver2._x * s(0) , edge._inizio.Y + ver2._y * s(0));   //si interseca in un solo punto
    }

    return p;

}


bool Intersector::Intersection(const Segment &edge, const Segment &segment) const
{

    Vec2d ver = _operation.tangent_versor(segment);
    Vec2d ver2 = _operation.tangent_versor(edge);
    Matrix2d mtx_tang;
    Matrix2d mtx_tang_inversa;


    mtx_tang <<  ver2._x, -ver._x,
                  ver2._y , -ver._y;
    double det = mtx_tang.determinant();
    mtx_tang_inversa << -ver._y, ver._x,
                  -ver2._y , ver2._x;
    Eigen::Vector2d b;                  //A*x = b => matx_tang_inversa * s = b
    b << segment._inizio.X - edge._inizio.X,
                  segment._inizio.Y - edge._inizio.Y ;
    Eigen::Vector2d s = Eigen::Vector2d::Zero();
    s = mtx_tang_inversa * b;
    s /= det;                          //coordinata curvilinea


    float toleranceParallelism = 1e-6;
    float toleranceIntersection = 1e-6;
    bool intersection = false;

    double check = toleranceParallelism * toleranceParallelism * mtx_tang.col(0).squaredNorm() *  mtx_tang.col(1).squaredNorm();  // ||t||^2 <= toll^2 * ||w||^2 * ||v||^2

    if(det * det >= check)      ///non paralleli
    {
        if((s(0) > -toleranceIntersection) && (s(0) < toleranceIntersection))       /// punto di intersezione è punto inizio lato
        {
            intersection = true;
         }
        else if ((s(0) > 1.0 - toleranceIntersection) && (s(0) < 1.0 + toleranceIntersection))      /// punto di intersezione è punto fine lato
        {
            intersection = true;
         }                                  // 0<s<1
         else if ((s(0) > -toleranceIntersection  && s(0)-1.0 < toleranceIntersection))    /// punto di intersezione è interno o esterno al lato
        {
            intersection = true;
        }
    }

    return intersection;
}


}

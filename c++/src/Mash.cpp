#include "Geometry.hpp"
#include "Polygon.hpp"
#include "Eigen"

#include <iostream>
#include <iomanip> //per stampare numero di cifre decimali a scelta
#include <math.h>

using namespace Eigen;
using namespace std;
using namespace PolygonLibrary;
namespace GeometryLibrary {

const vector<Points> Mash::traslationOrizzontale(vector<Points> &poly, double t) const
{
    for(unsigned int i = 0; i < poly.size(); i++){
        poly[i].X = poly[i].X + t;
    }
    return poly;
}

const vector<Points> Mash::traslationVerticale(vector<Points> &poly, double t) const
{
    for(unsigned int i = 0; i < poly.size(); i++){
        poly[i].Y = poly[i].Y + t;
    }
    return poly;
}

const vector<Points> Mash::verticiBox(const vector<Points> &poly) const
{
    double x_max = poly[0].X ;
    double x_min = poly[0].X;
    double y_max = poly[0].Y;
    double y_min = poly[0].Y;


    for (unsigned int i = 0; i < poly.size(); i++){
        if (poly[i].X > x_max)
            x_max = poly[i].X;
        if (poly[i].X < x_min)
            x_min = poly[i].X;
        if (poly[i].Y > y_max)
            y_max = poly[i].Y;
        if (poly[i].Y < y_min)
            y_min = poly[i].Y;
    }

    vector<PolygonLibrary::Points> vertici_box;

    vertici_box.push_back(PolygonLibrary::Points(x_min, y_min));
    vertici_box[0] = PolygonLibrary::Points(x_min, y_min);


    vertici_box.push_back(PolygonLibrary::Points(x_max, y_min));
    vertici_box[1] = PolygonLibrary::Points(x_max, y_min);

    vertici_box.push_back(PolygonLibrary::Points(x_max, y_max));
    vertici_box[2] = PolygonLibrary::Points(x_max, y_max);

    vertici_box.push_back(PolygonLibrary::Points(x_min, y_min));
    vertici_box[3] = PolygonLibrary::Points(x_min, y_max);

    return vertici_box;
}



const vector<vector<Points>> Mash::CutMash(const vector<Points> &poly) const
{
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _method(_point, _segment, _vector);
    GeometryLibrary::Intersector _intersector(_method);
    GeometryLibrary::NewItems _new(_intersector, _method);
    GeometryLibrary::CutPolygon _cutter(_intersector, _new);
    GeometryLibrary::Mash _mash(_cutter);

    vector<int> polygonvertices_poly;
    polygonvertices_poly.reserve(poly.size());

    vector<int> polygonvertices_box;
    polygonvertices_box.reserve(4);

    vector<Points> points;
    points.reserve(10);
    vector<vector<Points>> cutted_points;

    vector<int> vertici_intersection;


    for( unsigned int i = 0; i < poly.size(); i++)
        polygonvertices_poly.push_back(i);

    for( unsigned int i = 0; i < 4; i++)
        polygonvertices_box.push_back(i);


    double x_max = poly[0].X ;
    double x_min = poly[0].X;
    double y_max = poly[0].Y;
    double y_min = poly[0].Y;


    for (unsigned int i = 0; i < poly.size(); i++){
        if (poly[i].X > x_max)
            x_max = poly[i].X;
        if (poly[i].X < x_min)
            x_min = poly[i].X;
        if (poly[i].Y > y_max)
            y_max = poly[i].Y;
        if (poly[i].Y < y_min)
            y_min = poly[i].Y;
    }


    vector<PolygonLibrary::Points> intersection;
    vector<PolygonLibrary::Points> vertici_box;

    vertici_box = _mash.verticiBox(poly);

    for ( unsigned int i = 0; i < poly.size(); i++){

        //vettore dei punti di intersezione tra box e poly
        if (poly[i].X == x_min || poly[i].X == x_max || poly[i].Y == y_min || poly[i].Y == y_max){
            intersection.push_back(poly[i]);
            intersection[i].X = poly[i].X;
            intersection[i].Y = poly[i].Y;

            vertici_intersection.push_back(i);

        }
    }

    //scorro sulle intersezioni trovate
    for (unsigned int j = 0; j < intersection.size(); j++){

        //se lato verticale
        int flag = 0;
        if(j != 0 && j != intersection.size() - 1){
            if((intersection[j].X == intersection[j + 1].X && intersection[j].X == intersection[j - 1].X) || (intersection[j].X == intersection[j+1].X && intersection[j].X == intersection[j+2].X) || j==4){
                flag = 1;

            }
            if(j == 3 && flag == 1){
                points.push_back(intersection[j]);
                points[0].X = intersection[j].X;
                points[0].Y = intersection[j].Y;
                points.push_back(intersection[j + 2]);
                points[1].X = intersection[j + 2].X;
                points[1].Y = intersection[j + 2].Y;
                points.push_back(vertici_box[3]);
                points[2].X = vertici_box[3].X;
                points[2].Y = vertici_box[3].Y;

                cutted_points.push_back(points);
                points.clear();

                points.push_back(intersection[j + 2]);
                points[0].X = intersection[j + 2].X;
                points[0].Y = intersection[j + 2].Y;

                points.push_back(intersection[0]);
                points[1].X = intersection[0].X;
                points[1].Y = intersection[0].Y;

                points.push_back(vertici_box[0]);
                points[2].X = vertici_box[0].X;
                points[2].Y = vertici_box[0].Y;

                cutted_points.push_back(points);
                points.clear();

                j = j + 2;

}
        }

        if(flag == 0){
        unsigned int i = j;
        if ( j == intersection.size() - 1){  // se su lato finale
           i =  - 1;   // seconda intersezione su primo lato cosi punto finale edge diventa intersection[0]
        }

        PolygonLibrary::Segment edge(intersection[j], intersection[i+1]);

        points.push_back(intersection[j]);  //aggiungo prima intersezione
        points[0].X = intersection[j].X;
        points[0].Y = intersection[j].Y;


        //aggiungo punti centrali

        if (i == j){  // se non lato finale
           for (int z = polygonvertices_poly[vertici_intersection[j]]; z < polygonvertices_poly[vertici_intersection[j + 1]] - 1; z++){

            points.push_back(poly[z + 1]);
            points[points.size()].X = (poly[z + 1]).X;
            points[points.size()].Y = (poly[z + 1]).Y;

        }
           points.push_back(intersection[j + 1]);  //seconda intersezione
           points[points.size()].X = intersection[j+ 1].X;
           points[points.size()].Y = intersection[j + 1].Y;
        }

        else{
            points.push_back(intersection[0]);     //seconda intersezione
            points[points.size()].X = intersection[0].X;
            points[points.size()].Y = intersection[0].Y;
        }


        if ( j != intersection.size() - 1){  // se non su lato finale
            points.push_back(vertici_box[j + 1]);
            points[points.size()].X = vertici_box[j + 1].X;
            points[points.size()].Y = vertici_box[j + 1].Y;
        }

        if ( j == intersection.size() - 1){  // se su lato finale
            points.push_back(vertici_box[0]);
            points[points.size()].X = vertici_box[0].X;
            points[points.size()].Y = vertici_box[0].Y;
        }

        cutted_points.push_back(points);
        points.clear();
        }
    }
    cutted_points.push_back(poly);
    return cutted_points;

}

const vector<vector<Points>> Mash::CreateMash(vector<Points> &poly) const
{
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _method(_point, _segment, _vector);
    GeometryLibrary::Intersector _intersector(_method);
    GeometryLibrary::NewItems _new(_intersector, _method);
    GeometryLibrary::CutPolygon _cutter(_intersector, _new);
    GeometryLibrary::Mash _mash(_cutter);

    vector<Points> demain;
    int ripetizioni_base = 0;
    int ripetizioni_altezza = 0;
    double resto_base = 0.0;
    double resto_altezza = 0.0;
    double altezza_demain = 0.0;
    double altezza_box = 0.0;
    int box_interi = 0.0;
    double area = 0.0;
    vector<vector<Points>> full_mash;
    full_mash.reserve(1000);
    vector<vector<Points>> box_mash;

    vector<Points> box = _mash.verticiBox(poly); // creo poligono box

    demain.push_back(PolygonLibrary::Points(0.0,0.0));
    demain.push_back(PolygonLibrary::Points(4*(box[1].X - box[0].X) + 1,0.0));
    demain.push_back(PolygonLibrary::Points(4*(box[1].X - box[0].X) + 1, 3*(box[2].Y - box[1].Y) + 1));
    demain.push_back(PolygonLibrary::Points(0.0, 3*(box[2].Y - box[1].Y) + 1));

    //calcolo quante volte nella base
    double base_demain = demain[1].X - demain[0].X;
    double base_box = box[1].X - box[0].X;

    ripetizioni_base = int(base_demain/base_box);
    resto_base = base_demain - ripetizioni_base * base_box ;

    //calcolo quante volte nell'altezza
    altezza_demain = demain[2].Y - demain[1].Y;
    altezza_box = box[2].Y - box[1].Y;

    ripetizioni_altezza = int(altezza_demain/altezza_box);
    resto_altezza = altezza_demain - ripetizioni_altezza * altezza_box;


                             //CALCOLO AREA RETTANGOLI INTERI
    vector<vector<Points>> cutmash = _mash.CutMash(poly);

    for (unsigned int i = 0; i < cutmash.size(); i++){
        area += _method.Area(cutmash[i]);
    }

     box_interi = ripetizioni_altezza * ripetizioni_base;
     area = area * box_interi;


                             //CALCOLO AREA RETTANGOLI A DESTRA
     //creo new poly base
     vector<Points> new_polydx;


     for ( unsigned int i = 0; i < poly.size(); i++){
         new_polydx.push_back(poly[i]);    //memorizzo in new polydx i vertici di poly traslato
         new_polydx[i].X = poly[i].X - 1;
         new_polydx[i].Y = poly[i].Y - 1;

         if(new_polydx[i].X > resto_base){
             new_polydx[i].X = resto_base;
         }
     }

     vector<vector<Points>> polygon_cutted_dx = _mash.CutMash(new_polydx);
     double area_polygon_cutted_dx = 0.0;

     for ( unsigned int i = 0; i < polygon_cutted_dx.size(); i++){ //calcolo area della box sommando le aree dei poligono tagliati
         area_polygon_cutted_dx += _method.Area(polygon_cutted_dx[i]);
     }

     area = area + area_polygon_cutted_dx * ripetizioni_altezza;



                                   //CALCOLO AREA RETTANGOLI IN ALTO
     vector<Points> new_polysup;
     for ( unsigned int i = 0; i < poly.size(); i++){
         new_polysup.push_back(poly[i]);
         new_polysup[i].X = poly[i].X - 1;
         new_polysup[i].Y = poly[i].Y - 1;

         if(new_polysup[i].Y > resto_altezza){
             new_polysup[i].Y = resto_altezza;
         }
     }


     vector<vector<Points>> polygon_cutted_sup = _mash.CutMash(new_polysup);
     double area_polygon_cutted_sup = 0.0;

     for ( unsigned int i = 0; i < polygon_cutted_sup.size(); i++){
         area_polygon_cutted_sup += _method.Area(polygon_cutted_sup[i]);
     }

     area = area + 1.4 + area_polygon_cutted_sup * ripetizioni_base;

                                //CALCOLO AREA RETTANGOLO LIMITE ALTO DESTRA

        vector<Points> new_polylimite;
        for ( unsigned int i = 0; i < poly.size(); i++){
            new_polylimite.push_back(poly[i]);
            new_polylimite[i].X = poly[i].X - 1;
            new_polylimite[i].Y = poly[i].Y - 1;

            if(new_polylimite[i].Y > resto_altezza){
                 new_polylimite[i].Y = resto_altezza;
             }
            if(poly[i].X > resto_base){
                new_polylimite[i].X = resto_base;
         }
     }

     vector<vector<Points>> polygon_cutted_limite = _mash.CutMash(new_polylimite);
     double area_polygon_cutted_limite = 0.0;

     for ( unsigned int i = 0; i < polygon_cutted_limite.size(); i++){
         area_polygon_cutted_limite += _method.Area(polygon_cutted_limite[i]);
     }

     area = area + area_polygon_cutted_limite ;

     vector<Points> poly_traslation;
     vector<Points> poly_traslation_controllo;
     vector<Points> box_traslation;
     double t = 0.0;


     for (unsigned int i = 0; i < poly.size(); i++){  //memorizzo in polytraslation le coordinate dei vertici di poly
         poly_traslation.push_back(poly[i]);
         poly_traslation[i].X = poly[i].X - 1;
         poly_traslation[i].Y = poly[i].Y - 1;
     }

    for (unsigned int i = 0; i < poly.size(); i++){
         poly_traslation_controllo.push_back(poly[i]);
         poly_traslation_controllo[i].X = poly[i].X - 1;
         poly_traslation_controllo[i].Y = poly[i].Y - 1;
     }

     for (unsigned int i = 0; i < box.size(); i++){
         box_traslation.push_back(box[i]);
         box_traslation[i].X = box[i].X - 1;
         box_traslation[i].Y = box[i].Y - 1;
     }

     double toll = 10e-08;
     vector<Points> tmp;

     double x_max = poly_traslation_controllo[0].X ;
     double x_min = poly_traslation_controllo[0].X;
     double y_max = poly_traslation_controllo[0].Y;
     double y_min = poly_traslation_controllo[0].Y;

     //calcolo dei max e min (box)
     for (unsigned int i = 0; i < poly.size(); i++){
         if (poly_traslation_controllo[i].X > x_max)
             x_max = poly_traslation_controllo[i].X;
         if (poly_traslation_controllo[i].X < x_min)
             x_min = poly_traslation_controllo[i].X;
         if (poly_traslation_controllo[i].Y > y_max)
             y_max = poly_traslation_controllo[i].Y;
         if (poly_traslation_controllo[i].Y < y_min)
             y_min = poly_traslation_controllo[i].Y;
     }

     vector<Points> punti_ribaltati;
     punti_ribaltati.reserve(10);

     for(unsigned int i=0; i< poly_traslation_controllo.size(); i++){
         if(poly_traslation_controllo[i].Y == y_min){ //se sul lato inferiore della box
             poly_traslation_controllo[i].Y = y_max;

             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation[i].X;
             punti_ribaltati[i].Y = poly_traslation_controllo[i].Y;
             poly_traslation_controllo[i].Y = y_min;


         }
         if(poly_traslation_controllo[i].X == x_max){ //se sul lato destro della box
             poly_traslation_controllo[i].X = x_min;
             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation_controllo[i].X;
             punti_ribaltati[i].Y = poly_traslation[i].Y;
             poly_traslation_controllo[i].X = x_max;

         }
         if(poly_traslation_controllo[i].Y == y_max){ //se sul lato superiore della box
             poly_traslation_controllo[i].Y = y_min;
             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation[i].X;
             punti_ribaltati[i].Y = poly_traslation_controllo[i].Y;
             poly_traslation_controllo[i].Y = y_max;

         }
         if(poly_traslation_controllo[i].X == x_min){ //se sul lato sinistro della box
             poly_traslation_controllo[i].X = x_max;
             punti_ribaltati.push_back(poly_traslation_controllo[i]);
             punti_ribaltati[i].X = poly_traslation_controllo[i].X;
             punti_ribaltati[i].Y = poly_traslation[i].Y;
             poly_traslation_controllo[i].X = x_min;

         }

     }

     int flag = 0;
     vector<int> flag_controllo;
     flag_controllo.reserve(4);
     for(unsigned int i=0; i< punti_ribaltati.size(); i++){
         for(unsigned int j = 0; j < poly_traslation.size(); j++){
             if((punti_ribaltati[i].X == poly_traslation[j].X) && (punti_ribaltati[i].Y == poly_traslation[j].Y )){
                 flag = 1;
             }

        }
        flag_controllo.push_back(flag);

     }



     if(area - base_demain * altezza_demain < 10e-06){

         for(int i = 0; i < ripetizioni_base + 1; i++){ //ciclo sulle righe
             double s = 0.0;


             for(int j = 0; j < ripetizioni_altezza + 1; j++){ //ciclo sulle colonne

                 if(i != ripetizioni_base){
                 poly_traslation = traslationOrizzontale(poly_traslation, t);
                 t = base_box;

                    if (j != ripetizioni_altezza){
                        poly_traslation = traslationVerticale(poly_traslation, s);
                        box_traslation = traslationVerticale(box_traslation, s);
                        s = altezza_box;

                        box_mash.push_back(box_traslation);
                        int cont = 0;

                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){  //ciclo sui punti della mash
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll) && (cont == 0)){  //se i punti non coincidono
                                tmp.push_back(box_traslation[z]);      //li aggiungo a un vettore
                                cont = 1;
                             }


                         }cont =0;

                     }
                     }
                     if(i == 0 && j == 0)
                         full_mash.push_back(box_traslation);
                     else
                         full_mash.push_back(tmp);

                     tmp.clear();



                       //aggiunti a full_mash tutti i poligono tagliati
                     vector<vector<Points>> cutmash_traslation = _mash.CutMash(poly_traslation);

                     int controllo = 0;

                     for(unsigned int x = 0; x < flag_controllo.size(); x++){
                         if(flag_controllo[x]==1)
                             controllo = 1;
                     }

                     if(controllo == 0)  //se punti ribaltati non coincidono con vertici
                         for(unsigned int h = 0; h < cutmash_traslation.size() ; h++)
                            full_mash.push_back(cutmash_traslation[h]);

                     else{
                         vector<Points> polygon;
                         for(unsigned int h = 0; h < cutmash_traslation.size() ; h++){
                             for(unsigned int k=0; k < cutmash_traslation[h].size(); k++){ //ciclo su tutti i punti dei poligoni
                                 if(flag_controllo[1] == 0){  //se flag =0 per quel punto allora lo aggiungo

                                     polygon.push_back(cutmash_traslation[h][k]);
                                 }

                 }
                         }
                     full_mash.push_back(polygon);
                     }}

}
                 if( j == ripetizioni_altezza){

                     vector<Points> poly_alto;
                     poly_alto = traslationVerticale(new_polysup, ripetizioni_altezza * altezza_box);

                     box_traslation = verticiBox(poly_alto);

                     int cont = 0;
                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r ++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll) && (cont == 0)){ //se i punti non coincidono
                                 tmp.push_back(box_traslation[z]);
                                 cont = 1;
                             }
                          }

                      }cont =0;

                     }
                        full_mash.push_back(tmp);
                        tmp.clear();


                     for(unsigned int a = 0; a < polygon_cutted_sup.size(); a++){

                         polygon_cutted_sup[a] = traslationVerticale(polygon_cutted_sup[a], ripetizioni_altezza * altezza_box);
                         full_mash.push_back(polygon_cutted_sup[a]);
                     }

                     poly_traslation = traslationVerticale(poly_traslation, (-ripetizioni_altezza)*altezza_box); //per ritornare al polygono con coordinate X della base
                     box_traslation = traslationVerticale(box_traslation, (-ripetizioni_altezza)*altezza_box);
                }


             if (i == ripetizioni_base){  //se sui poligoni tagliati a destra


                 if( j != ripetizioni_altezza){

                     vector<Points> poly_dx;
                     poly_dx = traslationOrizzontale(new_polydx, ripetizioni_base * base_box);
                     poly_dx = traslationVerticale(poly_dx, altezza_box * j);

                     box_traslation = verticiBox(poly_dx);

                     int cont = 0;
                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r ++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll)  && (cont == 0)){ //se i punti non coincidono
                                 tmp.push_back(box_traslation[z]);      //li aggiungo a un vettore
                                 cont = 1;
                             }
                         }
                     } cont = 0;
                     }
                        full_mash.push_back(tmp);
                        tmp.clear();

                        //aggiungo poligono tagliati
                    for(unsigned int a = 0; a < polygon_cutted_dx.size(); a++){
                        polygon_cutted_dx[a] = traslationOrizzontale(polygon_cutted_dx[a], base_box * ripetizioni_base);
                        full_mash.push_back(polygon_cutted_dx[a]);
                 }

                 }

                 if( j == ripetizioni_altezza){
                     vector<Points> poly_limite;
                     poly_limite = traslationOrizzontale(new_polylimite, ripetizioni_base * base_box);
                     poly_limite = traslationVerticale(new_polylimite, altezza_box * j);

                     box_traslation = verticiBox(poly_limite);

                     int cont = 0;
                     for(unsigned int z = 0; z < box_traslation.size(); z++){
                        for(unsigned int r = 0; r < box_mash.size() - 1; r ++){
                            for (unsigned int s = 0; s < box_mash[r].size(); s++){  //ciclo sui punti della mash
                             if((box_traslation[z].X - box_mash[r][s].X > toll || box_traslation[z].Y - box_mash[r][s].Y > toll)  && (cont == 0)){  //se i punti non coincidono
                                 tmp.push_back(box_traslation[z]);      //li aggiungo a un vettore
                                 cont= 1;
                             }
                         }
                     } cont = 0;
                     }
                        full_mash.push_back(tmp);
                        tmp.clear();

                      //aggiungo poligoni tagliati
                     for(unsigned int a = 0; a < polygon_cutted_limite.size(); a++){
                         polygon_cutted_limite[a] = traslationOrizzontale(polygon_cutted_limite[a], base_box * ripetizioni_base);
                         polygon_cutted_limite[a] = traslationVerticale(polygon_cutted_limite[a], altezza_box * j);
                         full_mash.push_back(polygon_cutted_limite[a]);
                     }
                 }
             }
             }
         }
     }

     return full_mash;
}
}

#include "Polygon.hpp"
#include "Geometry.hpp"


#include "Eigen"

namespace GeometryLibrary {

double Operation::ComputeDistance(const Points &point1, const Points &point2) const
{
    double distance;
    distance = sqrt((point1.X - point2.X)*(point1.X - point2.X) + (point1.Y - point2.Y) * (point1.Y - point2.Y));
    return distance;
}

double Operation::Area(const vector<Points> &points) const
{
    double sum = 0;
    for (unsigned int i = 0; i < points.size(); i++){
        if (i != points.size() - 1)
            sum += points[i].X * points[i+1].Y - points[i + 1].X * points[i].Y; //formula di Gauss
        if (i == points.size() - 1)
            sum = sum + points[i].X * points[0].Y - points[0].X * points[i].Y;
    }

    double area = 0.5 * abs(sum);
    return area;
}


const Vec2d Operation::tangent_versor(const Segment &segment) const
{
    double x_segment, y_segment;                                        // componenti di segment x e y                                                        //modulo di segment
    x_segment = (segment._fine.X - segment._inizio.X);                  //componente x del segmento generale
    y_segment = (segment._fine.Y - segment._inizio.Y);                  //componente y di segmento generale

    return Vec2d(x_segment, y_segment);

}


const Vec2d Operation::normal_versor(const Segment &segment) const
{
    Vec2d tang_versor = tangent_versor(segment);
    double x_segment = -(tang_versor._y);
    double y_segment = (tang_versor._x);
    return Vec2d(x_segment, y_segment);
}


double Operation::DotProduct(const Vec2d &v1, const Vec2d &v2) const
{
    double dotProduct = v1._x * v2._x + v1._y * v2._y;
    return dotProduct;
}


bool Operation::concave(const Segment &edge, const Segment &next_edge, const Segment &segment) const
{
    bool concave = false;    //convesso
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _operation(_point, _segment, _vector);

    Vec2d tangVersorSegment = _operation.tangent_versor(segment);
    Vec2d normalVersorEdge = _operation.normal_versor(edge);
    Vec2d normalVersorNextEdge = _operation.normal_versor(next_edge);
    double dotProduct = _operation.DotProduct(tangVersorSegment, normalVersorEdge);
    double dotProduct_next = _operation.DotProduct(tangVersorSegment, normalVersorNextEdge);
    double dotProductNormal = _operation.DotProduct(normalVersorEdge, normalVersorNextEdge);

    if((dotProduct < 0 && dotProduct_next > 0) && dotProductNormal != 0)
        concave = true;

    if((dotProduct > 0 && dotProduct_next < 0) && (edge._inizio.Y == next_edge._inizio.Y) && dotProductNormal != 0) //caso particolare per lato verticale
        concave = true;


    return concave;
}

}


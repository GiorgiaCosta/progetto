#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <iostream>
#include <vector>

#include "Polygon.hpp"

using namespace std;
using namespace PolygonLibrary;

namespace GeometryLibrary{

class Operation : public IOperation{
private:

    Points _point;
    Segment _segment;
    Vec2d _vector;

public:

    Operation(const Points& point, const Segment& segment, const Vec2d& vector) : _point(point), _segment(segment), _vector(vector) {}
    Operation();

    double ComputeDistance(const Points& point1, const Points& point2) const;
    double Area(const vector<Points>& points) const;
    double DotProduct(const Vec2d& v1, const Vec2d& v2) const;
    const Vec2d tangent_versor(const Segment& segment) const;
    const Vec2d normal_versor(const Segment& segment) const;
    bool concave(const Segment &edge, const Segment &next_edge, const Segment &segment) const;

};


class Intersector : public IIntersector{
private:

    const IOperation& _operation;

public:

    Intersector(const IOperation& operation) : _operation(operation) {}
    Intersector();

    const Points PointIntersection(const Segment &edge, const Segment &segment) const;
    bool Intersection(const Segment &edge, const Segment &segment) const;

};

class NewItems : public INewItems{
private:

    const IIntersector& _intersector;
    const IOperation& _operation;

public:

    NewItems(const IIntersector& intersector, const IOperation& operation) : _intersector(intersector), _operation(operation){}
    NewItems();

    const vector<Points> newPoints(const vector<Points> &points, const Segment &segment) const;
    const vector<int> newPolygonVertices(const vector <Points> &points, const vector<int> &polygonVertices, const Segment &segment) const;

};

class CutPolygon: public ICutPolygon{

private:

    const IIntersector& _intersector;
    const INewItems& _newItem;

    public:
    CutPolygon(const IIntersector& intersector, const INewItems& newItem) : _intersector(intersector), _newItem(newItem)  {}
    CutPolygon();

    const vector<vector<int>> cuttedConvexPolygons(const  vector<Points>& points, const vector<int>& polygonVertices, const Segment& segment) const;
    const vector<vector<int>> cuttedConcavePolygons(const vector<Points>& points, const vector<int>& polygonVertices, const Segment& segment) const;
    };


class Mash : public IMash{
private:
    const ICutPolygon& _cutpolygon;

public:
    Mash(const ICutPolygon& cutpolygon) : _cutpolygon(cutpolygon)  {}
    Mash();

    const vector<Points> traslationOrizzontale(vector<Points>& poly, double t) const;
    const vector<Points> traslationVerticale(vector<Points>& poly, double t) const;
    const vector<Points> verticiBox(const vector<Points>& poly) const;
    const vector<vector<Points>> CutMash(const vector<Points>& poly) const;
    const vector<vector<Points>> CreateMash(vector<Points>& poly) const;
};
}

#endif // GEOMETRY_H

#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <vector>

using namespace std;

namespace PolygonLibrary {

class Points{
public:
  double X;
  double Y;
  Points(const double& x,
             const double& y) { X = x, Y = y ;}
  Points();
};

class Segment {
public:
    const Points _inizio;
    const Points _fine;
    double dist;

    Segment(const Points& inizio,
            const Points& fine) : _inizio(inizio), _fine(fine) {};
    Segment();
};

class Vec2d {
    public:
      double _x;
      double _y;

      Vec2d(const double& x,
               const double& y) {
              _x = x;
              _y = y;
          };

      Vec2d();
};


class IOperation{

public:
    virtual double ComputeDistance(const Points& point1, const Points& point2) const = 0;
    virtual double Area(const vector<Points>& points) const = 0;
    virtual double DotProduct(const Vec2d& v1, const Vec2d& v2) const = 0;
    virtual const Vec2d tangent_versor(const Segment& segment) const = 0;
    virtual const Vec2d normal_versor(const Segment& segment) const = 0;
    virtual bool concave(const Segment &edge, const Segment &next_edge, const Segment &segment) const = 0;

};


class IIntersector{

public:
    virtual const Points PointIntersection(const Segment &edge, const Segment &segment) const = 0;
    virtual bool Intersection(const Segment &edge, const Segment &segment) const = 0;
};

class INewItems{

public:
    virtual const vector<Points> newPoints(const vector<Points> &points, const Segment &segment) const = 0;    //& e * da mettere ???????
    virtual const vector<int> newPolygonVertices(const vector <Points> &points, const vector<int> &polygonVertices, const Segment &segment) const = 0;

};

class ICutPolygon{

public:

    virtual const vector<vector<int>> cuttedConvexPolygons(const vector<Points> &points, const vector<int> &polygonVertices, const Segment &segment) const = 0;
    virtual const vector<vector<int>> cuttedConcavePolygons(const vector<Points> &points, const vector<int> &polygonVertices, const Segment &segment) const = 0;
};

class IMash{
public:
    virtual const vector<Points> traslationOrizzontale( vector<Points>& poly, double t) const = 0;
    virtual const vector<Points> traslationVerticale( vector<Points>& poly, double t) const = 0;
    virtual const vector<Points> verticiBox(const vector<Points>& poly) const = 0;
    virtual const vector<vector<Points>> CutMash(const vector<Points>& poly) const = 0;
    virtual const vector<vector<Points>> CreateMash(vector<Points>& poly) const = 0;
};
}

#endif // POLYGON_H

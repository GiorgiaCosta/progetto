#include "test_emptyclass.hpp"
#include "test_geometry.hpp"
#include "test_operation.hpp"


#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

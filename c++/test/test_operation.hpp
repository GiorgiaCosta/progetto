#ifndef __TEST_POINTS_H
#define __TEST_POINTS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"
#include "Geometry.hpp"
#include "Polygon.hpp"

using namespace testing;
using namespace std;


namespace OperationTesting {

TEST(TestOperation, TestComputeDistance)
{
    //creo oggetto _operation creando oggetto _point, _segment e _vector
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _operation(_point, _segment, _vector);

    PolygonLibrary::Points point1 = PolygonLibrary::Points(1.0, 2.0);
    PolygonLibrary::Points point2 = PolygonLibrary::Points(2.5, 3.0);

    EXPECT_TRUE(_operation.ComputeDistance(point1, point2) - 1.802775638 < 1e-6);
}

TEST(TestOperation, TestArea){

    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _operation(_point, _segment, _vector);

    vector<PolygonLibrary::Points> pointsGenerico;
    pointsGenerico.reserve(4);

    pointsGenerico.push_back(PolygonLibrary::Points(-2.0, -2.0));
    pointsGenerico.push_back(PolygonLibrary::Points(2.0, -2.0));
    pointsGenerico.push_back(PolygonLibrary::Points(2.0, 2.0));
    pointsGenerico.push_back(PolygonLibrary::Points(-2.0, 2.0));

    pointsGenerico[0] = (PolygonLibrary::Points(-2.0, -2.0));
    pointsGenerico[1] = (PolygonLibrary::Points(2.0, -2.0));
    pointsGenerico[2] = (PolygonLibrary::Points(2.0, 2.0));
    pointsGenerico[3] = (PolygonLibrary::Points(-2.0, 2.0));

    EXPECT_TRUE(_operation.Area(pointsGenerico) - 16 < 1e-6);
}

TEST(TestOperation, TestTangentVersor)
{
   // GeometryLibrary::Operation _operation;
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _operation(_point, _segment, _vector);


    PolygonLibrary::Points _inizio = PolygonLibrary::Points(2.0, 2.5);
    PolygonLibrary::Points _fine = PolygonLibrary::Points(4.5, 6.0);
    PolygonLibrary::Segment segment = PolygonLibrary::Segment(_inizio, _fine);
    PolygonLibrary::Vec2d versore_trovato = _operation.tangent_versor(segment);
    EXPECT_TRUE((versore_trovato._x - 2.5) < 1e-6);   // la componente x del versore normalizzato è pari a 0.581238
    EXPECT_TRUE((versore_trovato._y - 3.5) < 1e-6);   // la componente y del versore normalizzato è pari a 0.813733

}

TEST(TestOperation, TestNormalVersor)
{
    //GeometryLibrary::Operation _operation;
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _operation(_point, _segment, _vector);


    PolygonLibrary::Points _inizio = PolygonLibrary::Points(2.0, 2.5);
    PolygonLibrary::Points _fine = PolygonLibrary::Points(4.5, 6.0);
    PolygonLibrary::Segment segment = PolygonLibrary::Segment(_inizio, _fine);
    PolygonLibrary::Vec2d versore_trovato = _operation.tangent_versor(segment);
    PolygonLibrary::Vec2d normale_trovato = _operation.normal_versor(segment);
    EXPECT_TRUE((normale_trovato._x + versore_trovato._y) < 1e-6);
    EXPECT_TRUE((normale_trovato._y - versore_trovato._x) < 1e-6);
 }

TEST(TestVec2d, TestDotProduct)
{
    //GeometryLibrary::Operation _operation;
    PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
    PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
    PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
    GeometryLibrary::Operation _operation(_point, _segment, _vector);


    PolygonLibrary::Vec2d vettore1 = PolygonLibrary::Vec2d(2.0, 1.0);
    PolygonLibrary::Vec2d vettore2 = PolygonLibrary::Vec2d(3.0, 2.0);
    EXPECT_EQ((_operation.DotProduct(vettore1, vettore2)), 8.0);
}
}



#endif // __TEST_POINTS_H

#ifndef __TEST_GEOMETRY_H        //dobbiamo aggiunger altre classi ??
#define __TEST_GEOMETRY_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Geometry.hpp"
#include "Polygon.hpp"

using namespace testing;

namespace GeometryTesting {

  TEST(TestCutPolygon, TestIntersection)
  {
      /*Ci serve testare questi quattro casi:

       * 1. I segmenti si intersecano ""senza prolungamenti"" (deve ritornare true);
       * 2. I segmenti si intersecano ""con prolungamenti, ovvero
       * si intersecano le rette , od un segmento con la retta dell'altro,
       * o viceversa (deve ritornare TRUE);
       * 3. Sono paralleli distinti (deve ritronare FALSE);
       * 4. Sono paralleli coincidenti (deve ritornare FALSE);
      */

      PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
      PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
      PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
      GeometryLibrary::Operation _method(_point, _segment, _vector);
      GeometryLibrary::Intersector _intersector(_method);

      //PRIMA INTERSEZIONE: I DUE SEGMENTI SI INTERSECANO ""SENZA PROLUNGAMENTI""

      try
     {
          PolygonLibrary::Points inizioLato = PolygonLibrary::Points(1.0, 1.0);
          PolygonLibrary::Points fineLato = PolygonLibrary::Points(2.5, 1.5);
          PolygonLibrary::Points inizioSegmento = PolygonLibrary::Points(0.7, 2.0);
          PolygonLibrary::Points fineSegmento = PolygonLibrary::Points(3.5, 1.5);

          PolygonLibrary::Segment lato = PolygonLibrary::Segment(inizioLato, fineLato);
          PolygonLibrary::Segment segmento = PolygonLibrary::Segment(inizioSegmento, fineSegmento);

          EXPECT_EQ((_intersector.Intersection(lato, segmento)), false);    // falso perche non c'è intersezione se prolungo il lato invece che segmento
      }

      catch (const exception& exception)
      {
          EXPECT_THAT(std::string(exception.what()), Eq("The First intersection goes wrong"));
      }

     //SECONDA INTERSEZIONE: I DUE SEGMENTI SI INTERSECANO ""CON PROLUNGAMENTI""

      try
     {
          PolygonLibrary::Points inizioLato2 = PolygonLibrary::Points(0.5, 6.0);
          PolygonLibrary::Points fineLato2 = PolygonLibrary::Points(4.5, 0.1);
          PolygonLibrary::Points inizioSegmento2 = PolygonLibrary::Points(-2.0, 0.0);
          PolygonLibrary::Points fineSegmento2 = PolygonLibrary::Points(0.1, 0.25);

          PolygonLibrary::Segment lato2 = PolygonLibrary::Segment(inizioLato2, fineLato2);
          PolygonLibrary::Segment segmento2 = PolygonLibrary::Segment(inizioSegmento2, fineSegmento2);

          EXPECT_EQ((_intersector.Intersection(lato2, segmento2)), true);
      }

      catch (const exception& exception)
      {
          EXPECT_THAT(std::string(exception.what()), Eq("The Second intersection goes wrong"));
      }

      //TERZA (NON) INTERSEZIONE: I DUE SEGMENTI SONO PARALLELI DISGIUNTI

       try
      {
          PolygonLibrary::Points inizioLato3 = PolygonLibrary::Points(0.5, 6.0);
          PolygonLibrary::Points fineLato3 = PolygonLibrary::Points(4.5, 6.0);
          PolygonLibrary::Points inizioSegmento3 = PolygonLibrary::Points(0.5, 8.0);
          PolygonLibrary::Points fineSegmento3 = PolygonLibrary::Points(5.2, 8.0);

          PolygonLibrary::Segment lato3 = PolygonLibrary::Segment(inizioLato3, fineLato3);
          PolygonLibrary::Segment segmento3 = PolygonLibrary::Segment(inizioSegmento3, fineSegmento3);

          EXPECT_EQ((_intersector.Intersection(lato3, segmento3)), false);
       }

       catch (const exception& exception)
       {
           EXPECT_THAT(std::string(exception.what()), Eq("The Third intersection goes wrong"));
       }

      //QUARTA (NON) INTERSEZIONE: I DUE SEGMENTI SONO PARALLELI COINCIDENTI

       try
      {
          PolygonLibrary::Points inizioLato4 = PolygonLibrary::Points(0.5, 6.0);
          PolygonLibrary::Points fineLato4 = PolygonLibrary::Points(4.5, 6.0);
          PolygonLibrary::Points inizioSegmento4 = PolygonLibrary::Points(0.1, 6.0);
          PolygonLibrary::Points fineSegmento4 = PolygonLibrary::Points(5.5, 6.0);

          PolygonLibrary::Segment lato4 = PolygonLibrary::Segment(inizioLato4, fineLato4);
          PolygonLibrary::Segment segmento4 = PolygonLibrary::Segment(inizioSegmento4, fineSegmento4);

          EXPECT_EQ((_intersector.Intersection(lato4, segmento4)), false);
       }

       catch (const exception& exception)
       {
           EXPECT_THAT(std::string(exception.what()), Eq("The Fourth intersection goes wrong"));
       }
  }

 // HO RESO QUESTE DUE FUNZIONI STATICHE PER FAR FUNZIONARE I TEST.

  TEST(CutPolygon, TestPointIntersection)

  {
      /* Anche in questo caso testiamo quattro casi:
       * 1. Punto d'inizio lato;
       * 2. Punto fine lato;
       * 3. Punto generico ""senza prolungamenti"";
       * 4. Punto generico ""con prolungamenti"".
      */

      // PUNTO D'INIZIO (CON PROLUNGAMENTO)


      PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
      PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
      PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
      GeometryLibrary::Operation _method(_point, _segment, _vector);
      GeometryLibrary::Intersector _intersector(_method);

      try
      {
          PolygonLibrary::Points inizioLato = PolygonLibrary::Points(0.0, 1.0);
          PolygonLibrary::Points fineLato = PolygonLibrary::Points(3.0, 0.0);
          PolygonLibrary::Points inizioSegmento = PolygonLibrary::Points(-2.0, -1.0);
          PolygonLibrary::Points fineSegmento = PolygonLibrary::Points(-1.0, 0.0);

          PolygonLibrary::Segment lato = PolygonLibrary::Segment(inizioLato, fineLato);
          PolygonLibrary::Segment segmento = PolygonLibrary::Segment(inizioSegmento, fineSegmento);

          PolygonLibrary::Points punto_trovato = _intersector.PointIntersection(lato, segmento);

          EXPECT_TRUE(punto_trovato.X - lato._inizio.X < 1e-6);
          EXPECT_TRUE(punto_trovato.Y - lato._inizio.Y < 1e-6);
      }

      catch (const exception& exception)
      {
          EXPECT_THAT(std::string(exception.what()), Eq("The first point intersection goes wrong"));
      }

      // PUNTO FINALE (CON PROLUNGAMENTO) // A VOLTE NON FUNZIONA CON LA Y: CON IL LATO = 3,0 ; 2,4 e il sefmento -1.-2; 0,0 NON FUNZIONA

      try //FUNZIONA IN QUESTO CASO, DOVE IL PUNTO è IN MEZZO AL SEGMENTO.

     {
          PolygonLibrary::Points inizioLato2 = PolygonLibrary::Points(3.0, 0.0);
          PolygonLibrary::Points fineLato2 = PolygonLibrary::Points(2.0, 4.0);
          PolygonLibrary::Points inizioSegmento2 = PolygonLibrary::Points(-1.0, -2.0);
          PolygonLibrary::Points fineSegmento2 = PolygonLibrary::Points(0.0, 0.0);

          PolygonLibrary::Segment lato2 = PolygonLibrary::Segment(inizioLato2, fineLato2);
          PolygonLibrary::Segment segmento2 = PolygonLibrary::Segment(inizioSegmento2, fineSegmento2);
          PolygonLibrary::Points punto_trovato2 = _intersector.PointIntersection(lato2, segmento2);

          EXPECT_TRUE(punto_trovato2.X - lato2._fine.X < 1e-6);
          EXPECT_TRUE(punto_trovato2.Y - lato2._fine.Y < 1e-6);
      }

      catch (const exception& exception)

      {
          EXPECT_THAT(std::string(exception.what()), Eq("The second point intersection goes wrong"));
      }

      // PUNTO GENERICO (""SENZA PROLUNGAMENTI"")

      try

     {
          PolygonLibrary::Points inizioLato3 = PolygonLibrary::Points(1.0, 0.0);
          PolygonLibrary::Points fineLato3 = PolygonLibrary::Points(0.0, 1.0);
          PolygonLibrary::Points inizioSegmento3 = PolygonLibrary::Points(-1.0, -1.0);
          PolygonLibrary::Points fineSegmento3 = PolygonLibrary::Points(3.0, 3.0);

          PolygonLibrary::Segment lato3 = PolygonLibrary::Segment(inizioLato3, fineLato3);
          PolygonLibrary::Segment segmento3 = PolygonLibrary::Segment(inizioSegmento3, fineSegmento3);

          PolygonLibrary::Points punto_trovato3 = _intersector.PointIntersection(lato3, segmento3);

          EXPECT_TRUE(punto_trovato3.X - 0.5  < 1e-6);
          EXPECT_TRUE(punto_trovato3.Y - 0.5  < 1e-6);
      }

      catch (const exception& exception)
      {
          EXPECT_THAT(std::string(exception.what()), Eq("The third point intersection goes wrong"));
      }
      }


  TEST(TestCutPolygon, TestConcave) //FUNZIONA SE SCAMBIO EDGE E NEXT_EDGE.

  {
      PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
      PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
      PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
      GeometryLibrary::Operation _method(_point, _segment, _vector);
      GeometryLibrary::Intersector _intersector(_method);

      //1 TESTO UN PUNTO CONCAVO: IL 4 DEL POLIGONO DEL PROFESSORE VICINI.

      PolygonLibrary::Points _inizio = PolygonLibrary::Points(3.2, 4.2);
      PolygonLibrary::Points _fine = PolygonLibrary::Points(4.0, 6.2);
      PolygonLibrary::Segment edge = PolygonLibrary::Segment(_inizio, _fine);
      PolygonLibrary::Points _inizio2 = PolygonLibrary::Points(1.0, 4.0);
      PolygonLibrary::Points _fine2 = PolygonLibrary::Points(3.2, 4.2);
      PolygonLibrary::Segment next_edge = PolygonLibrary::Segment(_inizio2, _fine2);
      PolygonLibrary::Points inizioSegmento = PolygonLibrary::Points(2.0, 3.7);
      PolygonLibrary::Points fineSegmento = PolygonLibrary::Points(4.1, 5.9);
      PolygonLibrary::Segment segment = PolygonLibrary::Segment(inizioSegmento, fineSegmento);

      EXPECT_EQ((_method.concave(edge, next_edge, segment)), true);

      //2 TESTO UN PUNTO CONVESSO: IL 3 DEL POLIGONO DEL PROFESSORE DI VICINI.

      PolygonLibrary::Points _inizio3 = PolygonLibrary::Points(4.0, 6.2);
      PolygonLibrary::Points _fine3 = PolygonLibrary::Points(5.5, 4.8);
      PolygonLibrary::Segment edge2 = PolygonLibrary::Segment(_inizio3, _fine3);
      PolygonLibrary::Points _inizio4 = PolygonLibrary::Points(3.2, 4.2);
      PolygonLibrary::Points _fine4 = PolygonLibrary::Points(4.0, 6.2);
      PolygonLibrary::Segment next_edge2 = PolygonLibrary::Segment(_inizio4, _fine4);
      PolygonLibrary::Points inizioSegmento2 = PolygonLibrary::Points(2.0, 3.7);
      PolygonLibrary::Points fineSegmento2 = PolygonLibrary::Points(4.1, 5.9);
      PolygonLibrary::Segment segment2 = PolygonLibrary::Segment(inizioSegmento2, fineSegmento2);

      EXPECT_EQ((_method.concave(edge2, next_edge2, segment2)), false);

      // punto 3 d'auria
      vector<PolygonLibrary::Points> pointsComplesso;

      PolygonLibrary::Points _inizio10 = PolygonLibrary::Points(0.0, 2.0);
      PolygonLibrary::Points _fine10 = PolygonLibrary::Points(3.0, 1.0);
      PolygonLibrary::Segment edge10 = PolygonLibrary::Segment(_inizio10, _fine10);

      PolygonLibrary::Points _inizio11 = PolygonLibrary::Points(3.0, 2.0);
      PolygonLibrary::Points _fine11 = PolygonLibrary::Points(0.0, 2.0);
      PolygonLibrary::Segment next_edge11 = PolygonLibrary::Segment(_inizio11, _fine11);

      PolygonLibrary::Points _inizioComplesso = PolygonLibrary::Points(-1.5, -1.5);
      PolygonLibrary::Points _fineComplesso = PolygonLibrary::Points(1.0, 1.0);
      PolygonLibrary::Segment segmentoComplesso = PolygonLibrary::Segment(_inizioComplesso, _fineComplesso);

      EXPECT_EQ((_method.concave(edge10, next_edge11, segmentoComplesso)), true);

      // punto 4 d'Aruria
      PolygonLibrary::Points _inizio100 = PolygonLibrary::Points(3.0, 2.0);
      PolygonLibrary::Points _fine100 = PolygonLibrary::Points(0.0, 2.0);
      PolygonLibrary::Segment edge100 = PolygonLibrary::Segment(_inizio100, _fine100);

      PolygonLibrary::Points _inizio110 = PolygonLibrary::Points(3.0, 3.0);
      PolygonLibrary::Points _fine110 = PolygonLibrary::Points(3.0, 2.0);
      PolygonLibrary::Segment next_edge110 = PolygonLibrary::Segment(_inizio110, _fine110);

      PolygonLibrary::Points _inizioComplesso00 = PolygonLibrary::Points(-1.5, -1.5);
      PolygonLibrary::Points _fineComplesso00 = PolygonLibrary::Points(1.0, 1.0);
      PolygonLibrary::Segment segmentoComplesso00 = PolygonLibrary::Segment(_inizioComplesso00, _fineComplesso00);

      EXPECT_EQ((_method.concave(edge100, next_edge110, segmentoComplesso00)), false);

      // testo poligono concavo generico

      PolygonLibrary::Points _inizio1001 = PolygonLibrary::Points(3.5, 1.7);
      PolygonLibrary::Points _fine1001 = PolygonLibrary::Points(5.0, 6.0);
      PolygonLibrary::Segment edge1001 = PolygonLibrary::Segment(_inizio1001, _fine1001);

      PolygonLibrary::Points _inizio1101 = PolygonLibrary::Points(-4.0, 4.0);
      PolygonLibrary::Points _fine1101 = PolygonLibrary::Points(3.5, 1.7);
      PolygonLibrary::Segment next_edge1101 = PolygonLibrary::Segment(_inizio1101, _fine1101);

      PolygonLibrary::Points _inizioComplesso001 = PolygonLibrary::Points(-2.0, -2.5);
      PolygonLibrary::Points _fineComplesso001 = PolygonLibrary::Points(4.0, 4.0);
      PolygonLibrary::Segment segmentoComplesso001 = PolygonLibrary::Segment(_inizioComplesso001, _fineComplesso001);

      EXPECT_EQ((_method.concave(edge1001, next_edge1101, segmentoComplesso001)), true);




  }


  TEST(TestCutPolygon, TestNewPoints){

      PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
      PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
      PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);
      GeometryLibrary::Operation _method(_point, _segment, _vector);
      GeometryLibrary::Intersector _intersector(_method);
      GeometryLibrary::NewItems _new(_intersector, _method);

  //1 Test Poligono Vicini.

  //Ho dovuto aggiungere la reserve perchè altrimenti non parte il programma.

  vector<PolygonLibrary::Points> points;
  vector<PolygonLibrary::Points> newPoints;

  //int numPuntiIntersezione = 4;

  //GeometryLibrary::Points punto = GeometryLibrary::Points(0,0);

  int numVertici = 6;

  points.reserve(numVertici);
  newPoints.reserve(12);

  /*points = {GeometryLibrary::Points(1.5, 1.0), GeometryLibrary::Points(5.6, 1.5),

           GeometryLibrary::Points(5.5, 4.8), GeometryLibrary::Points(4.0, 6.2),

           GeometryLibrary::Points(3.2, 4.2), GeometryLibrary::Points(1.0, 4.0)};*/

  //CON LA SCRITTURA ALLE RIGHE 408-411 NON INIZIALIZZA BENE IL VETTORE (???)



  points.push_back(PolygonLibrary::Points(1.5, 1.0));
  points.push_back(PolygonLibrary::Points(5.6, 1.5));
  points.push_back(PolygonLibrary::Points(5.5, 4.8));
  points.push_back(PolygonLibrary::Points(4.0, 6.2));
  points.push_back(PolygonLibrary::Points(3.2, 4.2));
  points.push_back(PolygonLibrary::Points(1.0, 4.0));

  ///IMPORTANTE:

  /*Le righe 428-433 servono per memorizzare le coordinate: però,
   * servono anche le righe 413-418: se non metto queste, è come
   * se nel vettore non ci fosse nulla (size = 0); se non metto 428-433
   * vengono inseriti nel vettore numeri casuali.....???????????
   * */

  points[0] = (PolygonLibrary::Points(1.5, 1.0));
  points[1] = (PolygonLibrary::Points(5.6, 1.5));
  points[2] = (PolygonLibrary::Points(5.5, 4.8));
  points[3] = (PolygonLibrary::Points(4.0, 6.2));
  points[4] = (PolygonLibrary::Points(3.2, 4.2));
  points[5] = (PolygonLibrary::Points(1.0, 4.0));

  //Questo è un controllo per vedere se ha effettivamente memorizzato i punti.



  EXPECT_EQ(points.size(), 6);
  EXPECT_EQ(points[0].Y, 1.0);
  EXPECT_EQ(points[5].X, 1.0);

  PolygonLibrary::Points _inizio = PolygonLibrary::Points(2.0, 3.7);
  PolygonLibrary::Points _fine = PolygonLibrary::Points(4.1, 5.9);
  PolygonLibrary::Segment segmento = PolygonLibrary::Segment(_inizio, _fine);


  /*E' un controllo che ho aggiunto per vedere se effetivamente

   * salva tutti i punti di intersezione*/

  newPoints = _new.newPoints(points, segmento);

  EXPECT_EQ(newPoints.size(),12);

  /* Testo per prima cosa i vertici del poligono*/


  for (unsigned int i = 0; i < points.size(); i++)
  {
      EXPECT_EQ(newPoints[i].X, points[i].X);
      EXPECT_EQ(newPoints[i].Y, points[i].Y);

  }


  /*Testo i punti di intersezione: per farlo, creo prima i lati che vengono intersecati,
   *ne calcolo l'intersezione con il segmento con il metodo PointIntersecrtiion e
   * confronto con il vettore newPoints.

  */


  PolygonLibrary::Segment lato2_3 = PolygonLibrary::Segment(points[2], points[3]);
  PolygonLibrary::Segment lato3_4 = PolygonLibrary::Segment(points[3], points[4]);
  PolygonLibrary::Segment lato4_5 = PolygonLibrary::Segment(points[4], points[5]);
  PolygonLibrary::Segment lato5_0 = PolygonLibrary::Segment(points[5], points[0]);

  PolygonLibrary::Points punto6 = _intersector.PointIntersection(lato2_3, segmento);
  PolygonLibrary::Points punto7 = _intersector.PointIntersection(lato3_4, segmento);
  PolygonLibrary::Points punto8 = _intersector.PointIntersection(lato4_5, segmento);
  PolygonLibrary::Points punto9 = _intersector.PointIntersection(lato5_0, segmento);

  /* Qui aggiungo una tolleranza perche sto confrontando due valroi,
   * mentre nei controlli sui points iniziali vi era una vera e propria
   * uguaglizana perche nel nostro metodo copiamo i vertici del poligono
   * in new points.*/


  EXPECT_TRUE(newPoints[6].X -  punto6.X < 1e-6);
  EXPECT_TRUE(newPoints[6].Y -  punto6.Y < 1e-6);
  EXPECT_TRUE(newPoints[7].X -  punto7.X < 1e-6);
  EXPECT_TRUE(newPoints[7].Y -  punto7.Y < 1e-6);
  EXPECT_TRUE(newPoints[8].X -  punto8.X < 1e-6);
  EXPECT_TRUE(newPoints[8].Y -  punto8.Y < 1e-6);
  EXPECT_TRUE(newPoints[9].X -  punto9.X < 1e-6);
  EXPECT_TRUE(newPoints[9].Y -  punto9.Y < 1e-6);


  /*Controllo infine il punto iniziale e finale del segmento, cioe se
   * sono stati inseriti correttamente in newpoints. Qui uso l'uguaglianza
   * perche tanto nel metodo implementato abbiamo copiato in newpoints*/

  EXPECT_EQ(newPoints[10].X, _fine.X);
  EXPECT_EQ(newPoints[10].Y, _fine.Y);
  EXPECT_EQ(newPoints[11].X, _inizio.X);
  EXPECT_EQ(newPoints[11].Y, _inizio.Y);


  //2 Test Poligono D'Auria.

  vector<PolygonLibrary::Points> pointsComplesso;
  vector<PolygonLibrary::Points> newPointsComplesso;

  int numVerticiComplesso = 10;
  pointsComplesso.reserve(numVerticiComplesso);
  newPointsComplesso.reserve(17);


  pointsComplesso.push_back(PolygonLibrary::Points(2.0, -2.0));
  pointsComplesso.push_back(PolygonLibrary::Points(0.0, -1.0));
  pointsComplesso.push_back(PolygonLibrary::Points(3.0, 1.0));
  pointsComplesso.push_back(PolygonLibrary::Points(0.0, 2.0));
  pointsComplesso.push_back(PolygonLibrary::Points(3.0, 2.0));
  pointsComplesso.push_back(PolygonLibrary::Points(3.0, 3.0));
  pointsComplesso.push_back(PolygonLibrary::Points(-1.0, 3.0));
  pointsComplesso.push_back(PolygonLibrary::Points(-3.0, 1.0));
  pointsComplesso.push_back(PolygonLibrary::Points(0.0, 0.0));
  pointsComplesso.push_back(PolygonLibrary::Points(-3.0, -2.0));


  pointsComplesso[0] = (PolygonLibrary::Points(2.0, -2.0));
  pointsComplesso[1] = (PolygonLibrary::Points(0.0, -1.0));
  pointsComplesso[2] = (PolygonLibrary::Points(3.0, 1.0));
  pointsComplesso[3] = (PolygonLibrary::Points(0.0, 2.0));
  pointsComplesso[4] = (PolygonLibrary::Points(3.0, 2.0));
  pointsComplesso[5] = (PolygonLibrary::Points(3.0, 3.0));
  pointsComplesso[6] = (PolygonLibrary::Points(-1.0, 3.0));
  pointsComplesso[7] = (PolygonLibrary::Points(-3.0, 1.0));
  pointsComplesso[8] = (PolygonLibrary::Points(0.0, 0.0));
  pointsComplesso[9] = (PolygonLibrary::Points(-3.0, -2.0));

  //Questo è un controllo per vedere se ha effettivamente memorizzato i punti.


  EXPECT_EQ(pointsComplesso.size(), 10);
  EXPECT_EQ(pointsComplesso[0].Y, -2.0);
  EXPECT_EQ(pointsComplesso[2].X, 3.0);

  PolygonLibrary::Points _inizioComplesso = PolygonLibrary::Points(-2.0, -2.0);
  PolygonLibrary::Points _fineComplesso = PolygonLibrary::Points(1.0, 1.0);
  PolygonLibrary::Segment segmentoComplesso = PolygonLibrary::Segment(_inizioComplesso, _fineComplesso);


  /*E' un controllo che ho aggiunto per vedere se effetivamente
   * salva tutti i punti di intersezione*/

  newPointsComplesso = _new.newPoints(pointsComplesso, segmentoComplesso);

  EXPECT_EQ(newPointsComplesso.size(),17);


  /* Testo per prima cosa i vertici del poligono*/

  for (unsigned int i = 0; i < pointsComplesso.size(); i++)
  {
      EXPECT_EQ(newPointsComplesso[i].X, pointsComplesso[i].X);

      EXPECT_EQ(newPointsComplesso[i].Y, pointsComplesso[i].Y);
  }

  //Testo i punti di intersezione.

  /*Controllo che il vertice 5 ed 8 siano stati inseriti anche come punti di intersezione.*/

  EXPECT_EQ(newPointsComplesso[12].X, pointsComplesso[5].X);
  EXPECT_EQ(newPointsComplesso[12].Y, pointsComplesso[5].Y);
  EXPECT_EQ(newPointsComplesso[13].X, pointsComplesso[8].X);
  EXPECT_EQ(newPointsComplesso[13].Y, pointsComplesso[8].Y);


  /*Testo i punti di intersezione: per farlo, creo prima i lati che vengono intersecati,
   *ne calcolo l'intersezione con il segmento con il metodo PointIntersecrtiion e
   * confronto con il vettore newPoints.
  */

  PolygonLibrary::Segment lato2_3Complesso = PolygonLibrary::Segment(pointsComplesso[2], pointsComplesso[3]);
  PolygonLibrary::Segment lato3_4Complesso = PolygonLibrary::Segment(pointsComplesso[3], pointsComplesso[4]);
  PolygonLibrary::Segment lato9_0Complesso = PolygonLibrary::Segment(pointsComplesso[9], pointsComplesso[0]);

  PolygonLibrary::Points punto10 = _intersector.PointIntersection(lato2_3Complesso, segmentoComplesso);
  PolygonLibrary::Points punto11 = _intersector.PointIntersection(lato3_4Complesso, segmentoComplesso);
  PolygonLibrary::Points punto12 = _intersector.PointIntersection(lato9_0Complesso, segmentoComplesso);

  EXPECT_TRUE(newPointsComplesso[10].X -  punto10.X < 1e-6);
  EXPECT_TRUE(newPointsComplesso[10].Y -  punto10.Y < 1e-6);
  EXPECT_TRUE(newPointsComplesso[11].X -  punto11.X < 1e-6);
  EXPECT_TRUE(newPointsComplesso[11].Y -  punto11.Y < 1e-6);
  EXPECT_TRUE(newPointsComplesso[14].X -  punto12.X < 1e-6);
  EXPECT_TRUE(newPointsComplesso[14].Y -  punto12.Y < 1e-6);

  /*Controllo infine il punto iniziale e finale del segmento, cioe se
   * sono stati inseriti correttamente in newpoints. Qui uso l'uguaglianza
   * perche tanto nel metodo implementato abbiamo copiato in newpoints*/

  EXPECT_EQ(newPointsComplesso[15].X, _fineComplesso.X);
  EXPECT_EQ(newPointsComplesso[15].Y, _fineComplesso.Y);
  EXPECT_EQ(newPointsComplesso[16].X, _inizioComplesso.X);
  EXPECT_EQ(newPointsComplesso[16].Y, _inizioComplesso.Y);


               // testo ottagono
  vector<PolygonLibrary::Points> pointsOttag;
  vector<PolygonLibrary::Points> newPoints_ottag;

  pointsOttag.push_back(PolygonLibrary::Points(1.0, -3.0));
  pointsOttag.push_back(PolygonLibrary::Points(3.0, -1.0));
  pointsOttag.push_back(PolygonLibrary::Points(3.0, 1.0));
  pointsOttag.push_back(PolygonLibrary::Points(1.0, 3.0));
  pointsOttag.push_back(PolygonLibrary::Points(-1.0, 3.0));
  pointsOttag.push_back(PolygonLibrary::Points(-4.0, 1.0));
  pointsOttag.push_back(PolygonLibrary::Points(-4.0, -1.0));
  pointsOttag.push_back(PolygonLibrary::Points(-1.0, -3.0));

  pointsOttag[0] = (PolygonLibrary::Points(1.0, -3.0));
  pointsOttag[1] = (PolygonLibrary::Points(3.0, -1.0));
  pointsOttag[2] = (PolygonLibrary::Points(3.0, 1.0));
  pointsOttag[3] = (PolygonLibrary::Points(1.0, 3.0));
  pointsOttag[4] = (PolygonLibrary::Points(-1.0, 3.0));
  pointsOttag[5] = (PolygonLibrary::Points(-4.0, 1.0));
  pointsOttag[6] = (PolygonLibrary::Points(-4.0, -1.0));
  pointsOttag[7] = (PolygonLibrary::Points(-1.0, -3.0));

  EXPECT_EQ(pointsOttag.size(), 8);

  PolygonLibrary::Points _inizioOttag = PolygonLibrary::Points(1.0, -2.0);
  PolygonLibrary::Points _fineOttag = PolygonLibrary::Points(1.0, 1.0);
  PolygonLibrary::Segment segmentoOttag = PolygonLibrary::Segment(_inizioOttag,_fineOttag);

  newPoints_ottag = _new.newPoints(pointsOttag, segmentoOttag);

  EXPECT_EQ(newPoints_ottag.size(),12);

}

  TEST(CutPolygon, TestNewPolygonVertices)

  {

      PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
      PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
      PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);

      GeometryLibrary::Operation _method(_point, _segment, _vector);
      GeometryLibrary::Intersector _intersector(_method);
      GeometryLibrary::NewItems _new(_intersector, _method);


      vector<int> verticiPoligono;
      vector<int> nuoviVertici;
      verticiPoligono.reserve(6);
      nuoviVertici.reserve(12);

      vector<PolygonLibrary::Points> points;
     // points.reserve(6);

      points.push_back(PolygonLibrary::Points(1.5, 1.0));
      points.push_back(PolygonLibrary::Points(5.6, 1.5));
      points.push_back(PolygonLibrary::Points(5.5, 4.8));
      points.push_back(PolygonLibrary::Points(4.0, 6.2));
      points.push_back(PolygonLibrary::Points(3.2, 4.2));
      points.push_back(PolygonLibrary::Points(1.0, 4.0));

      points[0] = (PolygonLibrary::Points(1.5, 1.0));
      points[1] = (PolygonLibrary::Points(5.6, 1.5));
      points[2] = (PolygonLibrary::Points(5.5, 4.8));
      points[3] = (PolygonLibrary::Points(4.0, 6.2));
      points[4] = (PolygonLibrary::Points(3.2, 4.2));
      points[5] = (PolygonLibrary::Points(1.0, 4.0));



      PolygonLibrary::Points _inizio = PolygonLibrary::Points(2.0, 3.7);
      PolygonLibrary::Points _fine = PolygonLibrary::Points(4.1, 5.9);
      PolygonLibrary::Segment segmento = PolygonLibrary::Segment(_inizio, _fine);

      for (int i = 0; i < 6; i++)
      {
          verticiPoligono.push_back(i);
      }

      EXPECT_EQ(verticiPoligono.size(), 6);

      nuoviVertici = _new.newPolygonVertices(points, verticiPoligono, segmento);

      EXPECT_EQ(nuoviVertici.size(),12);

      for (unsigned int i = 0; i < nuoviVertici.size(); i++)
      {
           EXPECT_EQ(nuoviVertici[i],i);
      }

      ///POLIGONO D'AURIA.

      vector<int> verticiPoligonoComplesso;
      vector<int> nuoviVerticiComplesso;

      verticiPoligonoComplesso.reserve(10);
      nuoviVerticiComplesso.reserve(17);

      vector<PolygonLibrary::Points> pointsComplesso;

      //pointsComplesso.reserve(10);

      pointsComplesso.push_back(PolygonLibrary::Points(2.0, -2.0));
      pointsComplesso.push_back(PolygonLibrary::Points(0.0, -1.0));
      pointsComplesso.push_back(PolygonLibrary::Points(3.0, 1.0));
      pointsComplesso.push_back(PolygonLibrary::Points(0.0, 2.0));
      pointsComplesso.push_back(PolygonLibrary::Points(3.0, 2.0));
      pointsComplesso.push_back(PolygonLibrary::Points(3.0, 3.0));
      pointsComplesso.push_back(PolygonLibrary::Points(-1.0, 3.0));
      pointsComplesso.push_back(PolygonLibrary::Points(-3.0, 1.0));
      pointsComplesso.push_back(PolygonLibrary::Points(0.0, 0.0));
      pointsComplesso.push_back(PolygonLibrary::Points(-3.0, -2.0));

      pointsComplesso[0] = (PolygonLibrary::Points(2.0, -2.0));
      pointsComplesso[1] = (PolygonLibrary::Points(0.0, -1.0));
      pointsComplesso[2] = (PolygonLibrary::Points(3.0, 1.0));
      pointsComplesso[3] = (PolygonLibrary::Points(0.0, 2.0));
      pointsComplesso[4] = (PolygonLibrary::Points(3.0, 2.0));
      pointsComplesso[5] = (PolygonLibrary::Points(3.0, 3.0));
      pointsComplesso[6] = (PolygonLibrary::Points(-1.0, 3.0));
      pointsComplesso[7] = (PolygonLibrary::Points(-3.0, 1.0));
      pointsComplesso[8] = (PolygonLibrary::Points(0.0, 0.0));
      pointsComplesso[9] = (PolygonLibrary::Points(-3.0, -2.0));

      PolygonLibrary::Points _inizioComplesso = PolygonLibrary::Points(-2.0, -2.0);
      PolygonLibrary::Points _fineComplesso = PolygonLibrary::Points(1.0, 1.0);

      PolygonLibrary::Segment segmentoComplesso = PolygonLibrary::Segment(_inizioComplesso, _fineComplesso);

      for (int i = 0; i < 10; i++)
      {
          verticiPoligonoComplesso.push_back(i);
      }

      EXPECT_EQ(verticiPoligonoComplesso.size(), 10);

      nuoviVerticiComplesso = _new.newPolygonVertices(pointsComplesso, verticiPoligonoComplesso, segmentoComplesso);

      EXPECT_EQ(nuoviVerticiComplesso.size(), 17);

      /*Con il for controllo che abbia salavto bene tutti i punti
       * fino al'11.*/

      for (unsigned int i = 0; i < verticiPoligonoComplesso.size() + 2; i++)
      {
           EXPECT_EQ(nuoviVerticiComplesso[i],i);
      }

      EXPECT_EQ(nuoviVerticiComplesso[12],5);
      EXPECT_EQ(nuoviVerticiComplesso[13],8);
      EXPECT_EQ(nuoviVerticiComplesso[14],12);
      EXPECT_EQ(nuoviVerticiComplesso[15],13);
      EXPECT_EQ(nuoviVerticiComplesso[16],14);
}


 TEST(TestCutPolygon, CuttedConvexPolygon)
 {
     PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
     PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
     PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);

     GeometryLibrary::Operation _method(_point, _segment, _vector);
     GeometryLibrary::Intersector _intersector(_method);
     GeometryLibrary::NewItems _new(_intersector, _method);
     GeometryLibrary::CutPolygon _cutter(_intersector, _new);


      double numeroTest = 1;

             ///Testo il rettangolo.


      vector<PolygonLibrary::Points> points;
      points.reserve(4);

      vector<vector<int>> cuttedPolygon;
      cuttedPolygon.reserve(2);
      //cuttedPolygon[1].reserve(6);
      //cuttedPolygon[2].reserve(6);

      points.push_back(PolygonLibrary::Points(1.0, 1.0));
      points.push_back(PolygonLibrary::Points(5.0, 1.0));
      points.push_back(PolygonLibrary::Points(5.0, 3.1));
      points.push_back(PolygonLibrary::Points(1.0, 3.1));

      points[0] = (PolygonLibrary::Points(1.0, 1.0));
      points[1] = (PolygonLibrary::Points(5.0, 1.0));
      points[2] = (PolygonLibrary::Points(5.0, 3.1));
      points[3] = (PolygonLibrary::Points(1.0, 3.1));

      PolygonLibrary::Points _inizio = PolygonLibrary::Points(2.0, 1.2);
      PolygonLibrary::Points _fine = PolygonLibrary::Points(4.0, 3.0);

      PolygonLibrary::Segment segmento = PolygonLibrary::Segment(_inizio,_fine);

      vector<int> polygonVertices;
      polygonVertices.reserve(4);

      for(unsigned int i = 0; i < points.size(); i++)
      {
          polygonVertices.push_back(i);
      }

      std::cout <<"I punti, vertici e poligoni ottenuti dal poligono convesso ";

      std::cout <<numeroTest;

      std::cout <<" sono:"<<"\n";

      numeroTest++;

      cuttedPolygon = _cutter.cuttedConvexPolygons(points, polygonVertices, segmento);

             //Controllo il primo poligono.

      EXPECT_EQ(cuttedPolygon[0].size(), 6);
      EXPECT_EQ(cuttedPolygon[0][0], 0);
      EXPECT_EQ(cuttedPolygon[0][1], 4);
      EXPECT_EQ(cuttedPolygon[0][2], 7);
      EXPECT_EQ(cuttedPolygon[0][3], 6);
      EXPECT_EQ(cuttedPolygon[0][4], 5);
      EXPECT_EQ(cuttedPolygon[0][5], 3);

      //Controllo il secondo poligono.

      EXPECT_EQ(cuttedPolygon[1].size(), 6);
      EXPECT_EQ(cuttedPolygon[1][0], 4);
      EXPECT_EQ(cuttedPolygon[1][1], 1);
      EXPECT_EQ(cuttedPolygon[1][2], 2);
      EXPECT_EQ(cuttedPolygon[1][3], 5);
      EXPECT_EQ(cuttedPolygon[1][4], 6);
      EXPECT_EQ(cuttedPolygon[1][5], 7);

       ///Testo il pentagono.

      vector<PolygonLibrary::Points> pointsPentag;
      pointsPentag.reserve(5);

      vector<vector<int>> cuttedPolygonPentag;
      cuttedPolygonPentag.reserve(2);
      //cuttedPolygonPentag[1].reserve(6);
      //cuttedPolygonPentag[2].reserve(6);

      pointsPentag.push_back(PolygonLibrary::Points(2.5, 1.0));
      pointsPentag.push_back(PolygonLibrary::Points(4.0, 2.1));
      pointsPentag.push_back(PolygonLibrary::Points(3.4, 4.2));
      pointsPentag.push_back(PolygonLibrary::Points(1.6, 4.2));
      pointsPentag.push_back(PolygonLibrary::Points(1.0, 2.1));

      pointsPentag[0] = (PolygonLibrary::Points(2.5, 1.0));
      pointsPentag[1] = (PolygonLibrary::Points(4.0, 2.1));
      pointsPentag[2] = (PolygonLibrary::Points(3.4, 4.2));
      pointsPentag[3] = (PolygonLibrary::Points(1.6, 4.2));
      pointsPentag[4] = (PolygonLibrary::Points(1.0, 2.1));

      PolygonLibrary::Points _inizioPentag = PolygonLibrary::Points(3.6, 2.2);
      PolygonLibrary::Points _finePentag = PolygonLibrary::Points(1.4, 2.75);

      PolygonLibrary::Segment segmentoPentag = PolygonLibrary::Segment(_inizioPentag,_finePentag);

      vector<int> polygonVerticesPentag;
      polygonVerticesPentag.reserve(5);


      for(unsigned int i = 0; i < pointsPentag.size(); i++)
      {
          polygonVerticesPentag.push_back(i);
      }

      std::cout <<"I punti, vertici e poligoni ottenuti dal poligono convesso ";

      std::cout <<numeroTest;

      std::cout <<" sono:"<<"\n";

      numeroTest++;

      cuttedPolygonPentag = _cutter.cuttedConvexPolygons(pointsPentag, polygonVerticesPentag, segmentoPentag);


             //Controllo il primo poligono.

      EXPECT_EQ(cuttedPolygonPentag[0].size(), 6);
      EXPECT_EQ(cuttedPolygonPentag[0][0], 0);
      EXPECT_EQ(cuttedPolygonPentag[0][1], 1);
      EXPECT_EQ(cuttedPolygonPentag[0][2], 6);
      EXPECT_EQ(cuttedPolygonPentag[0][3], 7);
      EXPECT_EQ(cuttedPolygonPentag[0][4], 5);
      EXPECT_EQ(cuttedPolygonPentag[0][5], 4);

             //Controllo il secondo poligono.

      EXPECT_EQ(cuttedPolygonPentag[1].size(), 6);
      EXPECT_EQ(cuttedPolygonPentag[1][0], 1);
      EXPECT_EQ(cuttedPolygonPentag[1][1], 2);
      EXPECT_EQ(cuttedPolygonPentag[1][2], 3);
      EXPECT_EQ(cuttedPolygonPentag[1][3], 5);
      EXPECT_EQ(cuttedPolygonPentag[1][4], 7);
      EXPECT_EQ(cuttedPolygonPentag[1][5], 6);



      ///Testo un altro poligono convesso con 4 lati.

      vector<PolygonLibrary::Points> pointsGenerico;
      pointsGenerico.reserve(4);

      vector<vector<int>> cuttedPolygonGenerico;
      cuttedPolygonGenerico.reserve(2);
      //cuttedPolygonGenerico[1].reserve(5);
      //cuttedPolygonGenerico[2].reserve(5);

      pointsGenerico.push_back(PolygonLibrary::Points(-2.0, -2.0));
      pointsGenerico.push_back(PolygonLibrary::Points(2.0, -2.0));
      pointsGenerico.push_back(PolygonLibrary::Points(2.0, 2.0));
      pointsGenerico.push_back(PolygonLibrary::Points(-2.0, 2.0));


      pointsGenerico[0] = (PolygonLibrary::Points(-2.0, -2.0));
      pointsGenerico[1] = (PolygonLibrary::Points(2.0, -2.0));
      pointsGenerico[2] = (PolygonLibrary::Points(2.0, 2.0));
      pointsGenerico[3] = (PolygonLibrary::Points(-2.0, 2.0));

      PolygonLibrary::Points _inizioGenerico = PolygonLibrary::Points(1.0, -1.0);
      PolygonLibrary::Points _fineGenerico = PolygonLibrary::Points(-1.0, 1.0);

      PolygonLibrary::Segment segmentoGenerico = PolygonLibrary::Segment(_inizioGenerico,_fineGenerico);

      vector<int> polygonVerticesGenerico;
      polygonVerticesGenerico.reserve(4);

      for(unsigned int i = 0; i < pointsGenerico.size(); i++)
      {
          polygonVerticesGenerico.push_back(i);
      }

      std::cout <<"I punti, vertici e poligoni ottenuti dal poligono convesso ";

      std::cout <<numeroTest;

      std::cout <<" sono:"<<"\n";

      numeroTest++;

      cuttedPolygonGenerico = _cutter.cuttedConvexPolygons(pointsGenerico, polygonVerticesGenerico, segmentoGenerico);


             //Controllo il primo poligono.

      EXPECT_EQ(cuttedPolygonGenerico[0].size(), 5);
      EXPECT_EQ(cuttedPolygonGenerico[0][0], 0);
      EXPECT_EQ(cuttedPolygonGenerico[0][1], 1);
      EXPECT_EQ(cuttedPolygonGenerico[0][2], 4);
      EXPECT_EQ(cuttedPolygonGenerico[0][3], 5);
      EXPECT_EQ(cuttedPolygonGenerico[0][4], 3);

             //Controllo il secondo poligono.

      EXPECT_EQ(cuttedPolygonGenerico[1].size(), 5);
      EXPECT_EQ(cuttedPolygonGenerico[1][0], 1);
      EXPECT_EQ(cuttedPolygonGenerico[1][1], 2);
      EXPECT_EQ(cuttedPolygonGenerico[1][2], 3);
      EXPECT_EQ(cuttedPolygonGenerico[1][3], 5);
      EXPECT_EQ(cuttedPolygonGenerico[1][4], 4);



            ///Testo un ottagono.

      vector<PolygonLibrary::Points> pointsOttag;
      pointsOttag.reserve(8);

      vector<vector<int>> cuttedPolygonOttag;
      cuttedPolygonOttag.reserve(2);
      //cuttedPolygonOttag[1].reserve(8);
      //cuttedPolygonOttag[2].reserve(6);

      pointsOttag.push_back(PolygonLibrary::Points(1.0, -3.0));
      pointsOttag.push_back(PolygonLibrary::Points(3.0, -1.0));
      pointsOttag.push_back(PolygonLibrary::Points(3.0, 1.0));
      pointsOttag.push_back(PolygonLibrary::Points(1.0, 3.0));
      pointsOttag.push_back(PolygonLibrary::Points(-1.0, 3.0));
      pointsOttag.push_back(PolygonLibrary::Points(-4.0, 1.0));
      pointsOttag.push_back(PolygonLibrary::Points(-4.0, -1.0));
      pointsOttag.push_back(PolygonLibrary::Points(-1.0, -3.0));

      pointsOttag[0] = (PolygonLibrary::Points(1.0, -3.0));
      pointsOttag[1] = (PolygonLibrary::Points(3.0, -1.0));
      pointsOttag[2] = (PolygonLibrary::Points(3.0, 1.0));
      pointsOttag[3] = (PolygonLibrary::Points(1.0, 3.0));
      pointsOttag[4] = (PolygonLibrary::Points(-1.0, 3.0));
      pointsOttag[5] = (PolygonLibrary::Points(-4.0, 1.0));
      pointsOttag[6] = (PolygonLibrary::Points(-4.0, -1.0));
      pointsOttag[7] = (PolygonLibrary::Points(-1.0, -3.0));

      PolygonLibrary::Points _inizioOttag = PolygonLibrary::Points(1.0, -2.0);
      PolygonLibrary::Points _fineOttag = PolygonLibrary::Points(1.0, 1.0);
      PolygonLibrary::Segment segmentoOttag = PolygonLibrary::Segment(_inizioOttag,_fineOttag);

      vector<int> polygonVerticesOttag;
      polygonVerticesOttag.reserve(8);

      for(unsigned int i = 0; i < pointsOttag.size(); i++)
      {
          polygonVerticesOttag.push_back(i);
      }

      std::cout <<"I punti, vertici e poligoni ottenuti dal poligono convesso ";

      std::cout <<numeroTest;

      std::cout <<" sono:"<<"\n";

      numeroTest++;

      cuttedPolygonOttag = _cutter.cuttedConvexPolygons(pointsOttag, polygonVerticesOttag, segmentoOttag);


                 //Controllo il primo poligono.
      EXPECT_EQ(cuttedPolygonOttag[0].size(), 6);
      EXPECT_EQ(cuttedPolygonOttag[0][0], 0);
      EXPECT_EQ(cuttedPolygonOttag[0][1], 1);
      EXPECT_EQ(cuttedPolygonOttag[0][2], 2);
      EXPECT_EQ(cuttedPolygonOttag[0][3], 3);
      EXPECT_EQ(cuttedPolygonOttag[0][4], 8);
      EXPECT_EQ(cuttedPolygonOttag[0][5], 9);


             //Controllo il secondo poligono

      EXPECT_EQ(cuttedPolygonOttag[1].size(), 8);
      EXPECT_EQ(cuttedPolygonOttag[1][0], 3);
      EXPECT_EQ(cuttedPolygonOttag[1][1], 4);
      EXPECT_EQ(cuttedPolygonOttag[1][2], 5);
      EXPECT_EQ(cuttedPolygonOttag[1][3], 6);
      EXPECT_EQ(cuttedPolygonOttag[1][4], 7);
      EXPECT_EQ(cuttedPolygonOttag[1][5], 0);
      EXPECT_EQ(cuttedPolygonOttag[1][6], 9);
      EXPECT_EQ(cuttedPolygonOttag[1][7], 8);



             ///Testo un triangolo.

      vector<PolygonLibrary::Points> pointsTriang;
      pointsTriang.reserve(3);

      vector<vector<int>> cuttedPolygonTriang;
      cuttedPolygonTriang.reserve(2);


      pointsTriang.push_back(PolygonLibrary::Points(-2.0, -2.0));
      pointsTriang.push_back(PolygonLibrary::Points(2.0, -2.0));
      pointsTriang.push_back(PolygonLibrary::Points(0.0, 4.1));

      pointsTriang[0] = (PolygonLibrary::Points(-2.0, -2.0));
      pointsTriang[1] = (PolygonLibrary::Points(2.0, -2.0));
      pointsTriang[2] = (PolygonLibrary::Points(0.0, 4.1));

      PolygonLibrary::Points _inizioTriang = PolygonLibrary::Points(0.0, 0.0);
      PolygonLibrary::Points _fineTriang = PolygonLibrary::Points(-1.0, -1.0);
      PolygonLibrary::Segment segmentoTriang = PolygonLibrary::Segment(_inizioTriang,_fineTriang);

      vector<int> polygonVerticesTriang;
      polygonVerticesTriang.reserve(3);

      for(unsigned int i = 0; i < pointsTriang.size(); i++)
      {
          polygonVerticesTriang.push_back(i);
      }

      std::cout <<"I punti, vertici e poligoni ottenuti dal poligono convesso ";

      std::cout <<numeroTest;

      std::cout <<" sono:"<<"\n";

      numeroTest++;

      cuttedPolygonTriang = _cutter.cuttedConvexPolygons(pointsTriang, polygonVerticesTriang, segmentoTriang);


             //Controllo il primo poligono.

      EXPECT_EQ(cuttedPolygonTriang[0].size(), 5);
      EXPECT_EQ(cuttedPolygonTriang[0][0], 0);
      EXPECT_EQ(cuttedPolygonTriang[0][1], 1);
      EXPECT_EQ(cuttedPolygonTriang[0][2], 3);
      EXPECT_EQ(cuttedPolygonTriang[0][3], 4);
      EXPECT_EQ(cuttedPolygonTriang[0][4], 5);

             //Controllo il secondo poligono.
      EXPECT_EQ(cuttedPolygonTriang[1].size(), 5);
      EXPECT_EQ(cuttedPolygonTriang[1][0], 3);
      EXPECT_EQ(cuttedPolygonTriang[1][1], 2);
      EXPECT_EQ(cuttedPolygonTriang[1][2], 0);
      EXPECT_EQ(cuttedPolygonTriang[1][3], 5);
      EXPECT_EQ(cuttedPolygonTriang[1][4], 4);

 }

 TEST(TestCutPolygon, CuttedConcavePolygon)
 {
     PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
     PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
     PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);

     GeometryLibrary::Operation _method(_point, _segment, _vector);
     GeometryLibrary::Intersector _intersector(_method);
     GeometryLibrary::NewItems _new(_intersector, _method);
     GeometryLibrary::CutPolygon _cutter(_intersector, _new);


     //1 Test Poligono Professor Vicini.

     vector<PolygonLibrary::Points> points;
     int numVertici = 6;
     points.reserve(numVertici);

     points.push_back(PolygonLibrary::Points(1.5, 1.0));
     points.push_back(PolygonLibrary::Points(5.6, 1.5));
     points.push_back(PolygonLibrary::Points(5.5, 4.8));
     points.push_back(PolygonLibrary::Points(4.0, 6.2));
     points.push_back(PolygonLibrary::Points(3.2, 4.2));
     points.push_back(PolygonLibrary::Points(1.0, 4.0));

     points[0] = (PolygonLibrary::Points(1.5, 1.0));
     points[1] = (PolygonLibrary::Points(5.6, 1.5));
     points[2] = (PolygonLibrary::Points(5.5, 4.8));
     points[3] = (PolygonLibrary::Points(4.0, 6.2));
     points[4] = (PolygonLibrary::Points(3.2, 4.2));
     points[5] = (PolygonLibrary::Points(1.0, 4.0));


     PolygonLibrary::Points _inizio = PolygonLibrary::Points(2.0, 3.7);
     PolygonLibrary::Points _fine = PolygonLibrary::Points(4.1, 5.9);
     PolygonLibrary::Segment segmento = PolygonLibrary::Segment(_inizio, _fine);

     vector<int> verticiPoligono;
     verticiPoligono.reserve(6);

     for (unsigned int i = 0; i < points.size(); i++)
     {
         verticiPoligono.push_back(i);
     }

     EXPECT_EQ(verticiPoligono.size(), 6);

     std::cout <<"I punti, vertici e poligoni ottenuti dal primo poligono concavo(Professor Vicini) ";
     std::cout <<"sono:"<<"\n";
     vector<vector<int>> cuttedConcPolygon;

     cuttedConcPolygon = _cutter.cuttedConcavePolygons(points, verticiPoligono, segmento);

    //CONTROLLO CHE IL PUNTO 3 LO PRENDA COME CONVESSO NEL POLIGONO DI VICINI, il 4 come concavo ed il 5 come convesso.
     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(points[4], points[3]), PolygonLibrary::Segment(points[5], points[4]), segmento), true);
     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(points[5], points[4]), PolygonLibrary::Segment(points[0], points[5]), segmento), false);
     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(points[3], points[2]), PolygonLibrary::Segment(points[4], points[3]), segmento), false);

     EXPECT_EQ(cuttedConcPolygon.size(),3);

     //Controllo il primo poligono.
     EXPECT_EQ(cuttedConcPolygon[0].size(), 4);
     EXPECT_EQ(cuttedConcPolygon[0][0], 6);
     EXPECT_EQ(cuttedConcPolygon[0][1], 3);
     EXPECT_EQ(cuttedConcPolygon[0][2], 7);
     EXPECT_EQ(cuttedConcPolygon[0][3], 10);

     //Controllo il secondo poligono.
     EXPECT_EQ(cuttedConcPolygon[1].size(), 10);
     EXPECT_EQ(cuttedConcPolygon[1][0], 4);
     EXPECT_EQ(cuttedConcPolygon[1][1], 8);
     EXPECT_EQ(cuttedConcPolygon[1][2], 11);
     EXPECT_EQ(cuttedConcPolygon[1][3], 9);
     EXPECT_EQ(cuttedConcPolygon[1][4], 0);
     EXPECT_EQ(cuttedConcPolygon[1][5], 1);
     EXPECT_EQ(cuttedConcPolygon[1][6], 2);
     EXPECT_EQ(cuttedConcPolygon[1][7], 6);
     EXPECT_EQ(cuttedConcPolygon[1][8], 10);
     EXPECT_EQ(cuttedConcPolygon[1][9], 7);

     //Controllo il terzo poligono.
     EXPECT_EQ(cuttedConcPolygon[2].size(), 4);
     EXPECT_EQ(cuttedConcPolygon[2][0], 8);
     EXPECT_EQ(cuttedConcPolygon[2][1], 5);
     EXPECT_EQ(cuttedConcPolygon[2][2], 9);
     EXPECT_EQ(cuttedConcPolygon[2][3], 11);

     ///Poligono D'Auria.
     vector<int> verticiPoligonoComplesso;
     vector<int> nuoviVerticiComplesso;
     verticiPoligonoComplesso.reserve(10);
     nuoviVerticiComplesso.reserve(17);
     vector<PolygonLibrary::Points> pointsComplesso;

     pointsComplesso.push_back(PolygonLibrary::Points(2.0, -2.0));
     pointsComplesso.push_back(PolygonLibrary::Points(0.0, -1.0));
     pointsComplesso.push_back(PolygonLibrary::Points(3.0, 1.0));
     pointsComplesso.push_back(PolygonLibrary::Points(0.0, 2.0));
     pointsComplesso.push_back(PolygonLibrary::Points(3.0, 2.0));
     pointsComplesso.push_back(PolygonLibrary::Points(3.0, 3.0));
     pointsComplesso.push_back(PolygonLibrary::Points(-1.0, 3.0));
     pointsComplesso.push_back(PolygonLibrary::Points(-3.0, 1.0));
     pointsComplesso.push_back(PolygonLibrary::Points(0.0, 0.0));
     pointsComplesso.push_back(PolygonLibrary::Points(-3.0, -2.0));

     pointsComplesso[0] = (PolygonLibrary::Points(2.0, -2.0));
     pointsComplesso[1] = (PolygonLibrary::Points(0.0, -1.0));
     pointsComplesso[2] = (PolygonLibrary::Points(3.0, 1.0));
     pointsComplesso[3] = (PolygonLibrary::Points(0.0, 2.0));
     pointsComplesso[4] = (PolygonLibrary::Points(3.0, 2.0));
     pointsComplesso[5] = (PolygonLibrary::Points(3.0, 3.0));
     pointsComplesso[6] = (PolygonLibrary::Points(-1.0, 3.0));
     pointsComplesso[7] = (PolygonLibrary::Points(-3.0, 1.0));
     pointsComplesso[8] = (PolygonLibrary::Points(0.0, 0.0));
     pointsComplesso[9] = (PolygonLibrary::Points(-3.0, -2.0));

     PolygonLibrary::Points _inizioComplesso = PolygonLibrary::Points(-1.5, -1.5);
     PolygonLibrary::Points _fineComplesso = PolygonLibrary::Points(1.0, 1.0);
     PolygonLibrary::Segment segmentoComplesso = PolygonLibrary::Segment(_inizioComplesso, _fineComplesso);

     for (int i = 0; i < 10; i++)
     {
        verticiPoligonoComplesso.push_back(i);
     }

     std::cout <<"I punti, vertici e poligoni ottenuti dal secondo poligono concavo(Professor D'Auria) ";
     std::cout <<"sono:"<<"\n";

     vector<vector<int>> cuttedConcPolygonComplesso;
     cuttedConcPolygonComplesso = _cutter.cuttedConcavePolygons(pointsComplesso,verticiPoligonoComplesso, segmentoComplesso);
     EXPECT_EQ(cuttedConcPolygonComplesso.size(),4);

     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(pointsComplesso[3], pointsComplesso[2]), PolygonLibrary::Segment(pointsComplesso[4], pointsComplesso[3]), segmentoComplesso), true);
     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(pointsComplesso[4], pointsComplesso[3]), PolygonLibrary::Segment(pointsComplesso[5], pointsComplesso[4]), segmentoComplesso), false);
     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(pointsComplesso[5], pointsComplesso[4]), PolygonLibrary::Segment(pointsComplesso[6], pointsComplesso[5]), segmentoComplesso), false);
     EXPECT_EQ(_method.concave(PolygonLibrary::Segment(pointsComplesso[8], pointsComplesso[7]), PolygonLibrary::Segment(pointsComplesso[9], pointsComplesso[8]), segmentoComplesso), true);

     //Controllo il primo poligono.
     EXPECT_EQ(cuttedConcPolygonComplesso[0].size(), 8);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][0], 3);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][1], 11);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][2], 5);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][3], 6);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][4], 7);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][5], 8);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][6], 13);
     EXPECT_EQ(cuttedConcPolygonComplesso[0][7], 10);

     //Controllo il secondo poligono.
     EXPECT_EQ(cuttedConcPolygonComplesso[1].size(), 3);
     EXPECT_EQ(cuttedConcPolygonComplesso[1][0], 11);
     EXPECT_EQ(cuttedConcPolygonComplesso[1][1], 4);
     EXPECT_EQ(cuttedConcPolygonComplesso[1][2], 5);

     //Controllo il terzo poligono.
     EXPECT_EQ(cuttedConcPolygonComplesso[2].size(), 4);
     EXPECT_EQ(cuttedConcPolygonComplesso[2][0], 8);
     EXPECT_EQ(cuttedConcPolygonComplesso[2][1], 9);
     EXPECT_EQ(cuttedConcPolygonComplesso[2][2], 12);
     EXPECT_EQ(cuttedConcPolygonComplesso[2][3], 14);

     //Controllo il quarto poligono.
     EXPECT_EQ(cuttedConcPolygonComplesso[3].size(), 8);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][0], 10);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][1], 13);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][2], 8);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][3], 14);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][4], 12);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][5], 0);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][6], 1);
     EXPECT_EQ(cuttedConcPolygonComplesso[3][7], 2);



     //testo un poligono generico
     vector<int> verticiPoligonoGenerico2;
     vector<int> nuoviVerticiGenerico2;
     vector<PolygonLibrary::Points> pointsGenerico2;

     pointsGenerico2.push_back(PolygonLibrary::Points(-4.0, -3.0));
     pointsGenerico2.push_back(PolygonLibrary::Points(5.0, -3.0));
     pointsGenerico2.push_back(PolygonLibrary::Points(7.0, 4.0));
     pointsGenerico2.push_back(PolygonLibrary::Points(5.0, 6.0));
     pointsGenerico2.push_back(PolygonLibrary::Points(3.5, 1.7));
     pointsGenerico2.push_back(PolygonLibrary::Points(-4.0, 4.0));
     pointsGenerico2.push_back(PolygonLibrary::Points(-5.0, 0.0));

     pointsGenerico2[0] = (PolygonLibrary::Points(-4.0, -3.0));
     pointsGenerico2[1] = (PolygonLibrary::Points(5.0, -3.0));
     pointsGenerico2[2] = (PolygonLibrary::Points(7.0, 4.0));
     pointsGenerico2[3] = (PolygonLibrary::Points(5.0, 6.0));
     pointsGenerico2[4] = (PolygonLibrary::Points(3.5, 1.7));
     pointsGenerico2[5] = (PolygonLibrary::Points(-4.0, 4.0));
     pointsGenerico2[6] = (PolygonLibrary::Points(-5.0, 0.0));

     PolygonLibrary::Points _inizioGenerico2 = PolygonLibrary::Points(-2.0, 2.5);
     PolygonLibrary::Points _fineGenerico2 = PolygonLibrary::Points(5.1, 4.2);
     PolygonLibrary::Segment segmentoGenerico2 = PolygonLibrary::Segment(_inizioGenerico2, _fineGenerico2);

     for (int i = 0; i < 7; i++)
     {
        verticiPoligonoGenerico2.push_back(i);
     }

     vector<vector<int>> cuttedConcPolygonGenerico2;
     std::cout <<"I punti, vertici e poligoni ottenuti dal terzo poligono concavo(inserito da noi) ";
     std::cout <<"sono:"<<"\n";
     cuttedConcPolygonGenerico2 = _cutter.cuttedConcavePolygons(pointsGenerico2,verticiPoligonoGenerico2, segmentoGenerico2);


     EXPECT_EQ(cuttedConcPolygonGenerico2.size(),3);


          //Controllo il primo poligono.

          EXPECT_EQ(cuttedConcPolygonGenerico2[0].size(), 4);
          EXPECT_EQ(cuttedConcPolygonGenerico2[0][0], 7);
          EXPECT_EQ(cuttedConcPolygonGenerico2[0][1], 3);
          EXPECT_EQ(cuttedConcPolygonGenerico2[0][2], 8);
          EXPECT_EQ(cuttedConcPolygonGenerico2[0][3], 11);


          //Controllo il secondo poligono.
          EXPECT_EQ(cuttedConcPolygonGenerico2[1].size(), 11);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][0], 4);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][1], 9);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][2], 12);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][3], 10);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][4], 6);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][5], 0);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][6], 1);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][7], 2);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][8], 7);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][9], 11);
          EXPECT_EQ(cuttedConcPolygonGenerico2[1][10],8);



          //Controllo il terzo poligono.

          EXPECT_EQ(cuttedConcPolygonGenerico2[2].size(), 4);
          EXPECT_EQ(cuttedConcPolygonGenerico2[2][0], 9);
          EXPECT_EQ(cuttedConcPolygonGenerico2[2][1], 5);
          EXPECT_EQ(cuttedConcPolygonGenerico2[2][2], 10);
          EXPECT_EQ(cuttedConcPolygonGenerico2[2][3], 12);

}


 TEST(TestCutPolygon, CutMash){

     PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
     PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
     PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);

     GeometryLibrary::Operation _method(_point, _segment, _vector);
     GeometryLibrary::Intersector _intersector(_method);
     GeometryLibrary::NewItems _new(_intersector, _method);
     GeometryLibrary::CutPolygon _cutter(_intersector, _new);
     GeometryLibrary::Mash _mash(_cutter);


     //1 Test Poligono Professor Vicini.

     vector<Points> points;
     int numVertici = 6;
     points.reserve(numVertici);

     points.push_back(PolygonLibrary::Points(1.5, 1.0));
     points.push_back(PolygonLibrary::Points(5.6, 1.5));
     points.push_back(PolygonLibrary::Points(5.5, 4.8));
     points.push_back(PolygonLibrary::Points(4.0, 6.2));
     points.push_back(PolygonLibrary::Points(3.2, 4.2));
     points.push_back(PolygonLibrary::Points(1.0, 4.0));

     points[0] = (PolygonLibrary::Points(1.5, 1.0));
     points[1] = (PolygonLibrary::Points(5.6, 1.5));
     points[2] = (PolygonLibrary::Points(5.5, 4.8));
     points[3] = (PolygonLibrary::Points(4.0, 6.2));
     points[4] = (PolygonLibrary::Points(3.2, 4.2));
     points[5] = (PolygonLibrary::Points(1.0, 4.0));

     vector<vector<Points>> polygon_mash = _mash.CutMash(points);


    //primo poligono tagliato del box
    EXPECT_EQ(polygon_mash[0].size(), 3);
    EXPECT_EQ(polygon_mash[0][0].X, 1.5);
    EXPECT_EQ(polygon_mash[0][0].Y, 1.0);
    EXPECT_EQ(polygon_mash[0][1].X, 5.6);
    EXPECT_EQ(polygon_mash[0][1].Y, 1.5);
    EXPECT_EQ(polygon_mash[0][2].X, 5.6);
    EXPECT_EQ(polygon_mash[0][2].Y, 1.0);

    //secondo poligono tagliato del box
    EXPECT_EQ(polygon_mash[1].size(), 4);
    EXPECT_EQ(polygon_mash[1][0].X, 5.6);
    EXPECT_EQ(polygon_mash[1][0].Y, 1.5);
    EXPECT_EQ(polygon_mash[1][1].X, 5.5);
    EXPECT_EQ(polygon_mash[1][1].Y, 4.8);
    EXPECT_EQ(polygon_mash[1][2].X, 4.0);
    EXPECT_EQ(polygon_mash[1][2].Y, 6.2);
    EXPECT_EQ(polygon_mash[1][3].X, 5.6);
    EXPECT_EQ(polygon_mash[1][3].Y, 6.2);

    //terzo poligono tagliato del box
    EXPECT_EQ(polygon_mash[2].size(), 4);
    EXPECT_EQ(polygon_mash[2][0].X, 4.0);
    EXPECT_EQ(polygon_mash[2][0].Y, 6.2);
    EXPECT_EQ(polygon_mash[2][1].X, 3.2);
    EXPECT_EQ(polygon_mash[2][1].Y, 4.2);
    EXPECT_EQ(polygon_mash[2][2].X, 1.0);
    EXPECT_EQ(polygon_mash[2][2].Y, 4.0);
    EXPECT_EQ(polygon_mash[2][3].X, 1.0);
    EXPECT_EQ(polygon_mash[2][3].Y, 6.2);

    //quarto poligono tagliato del box
    EXPECT_EQ(polygon_mash[3].size(), 3);
    EXPECT_EQ(polygon_mash[3][0].X, 1.0);
    EXPECT_EQ(polygon_mash[3][0].Y, 4.0);
    EXPECT_EQ(polygon_mash[3][1].X, 1.5);
    EXPECT_EQ(polygon_mash[3][1].Y, 1.0);
    EXPECT_EQ(polygon_mash[3][2].X, 1.0);
    EXPECT_EQ(polygon_mash[3][2].Y, 1.0);


    //quindi poligono tagliato del box
    EXPECT_EQ(polygon_mash[4].size(), 6);
    EXPECT_EQ(polygon_mash[4][0].X, 1.5);
    EXPECT_EQ(polygon_mash[4][0].Y, 1.0);
    EXPECT_EQ(polygon_mash[4][1].X, 5.6);
    EXPECT_EQ(polygon_mash[4][1].Y, 1.5);
    EXPECT_EQ(polygon_mash[4][2].X, 5.5);
    EXPECT_EQ(polygon_mash[4][2].Y, 4.8);
    EXPECT_EQ(polygon_mash[4][3].X, 4.0);
    EXPECT_EQ(polygon_mash[4][3].Y, 6.2);
    EXPECT_EQ(polygon_mash[4][4].X, 3.2);
    EXPECT_EQ(polygon_mash[4][4].Y, 4.2);
    EXPECT_EQ(polygon_mash[4][5].X, 1.0);
    EXPECT_EQ(polygon_mash[4][5].Y, 4.0);


 }

   TEST(TestCutPolygon, CreateMash){

     PolygonLibrary::Points _point = PolygonLibrary::Points(0.0, 0.0);
     PolygonLibrary::Segment _segment = PolygonLibrary::Segment(Points(0.0, 0.0), Points(0.0, 0.0));
     PolygonLibrary::Vec2d _vector = PolygonLibrary::Vec2d(0.0, 0.0);

     GeometryLibrary::Operation _method(_point, _segment, _vector);
     GeometryLibrary::Intersector _intersector(_method);
     GeometryLibrary::NewItems _new(_intersector, _method);
     GeometryLibrary::CutPolygon _cutter(_intersector, _new);
     GeometryLibrary::Mash _mash(_cutter);


     //1 Test Poligono Professor Vicini.

     vector<Points> points;
     int numVertici = 6;
     points.reserve(numVertici);

     points.push_back(PolygonLibrary::Points(1.5, 1.0));
     points.push_back(PolygonLibrary::Points(5.6, 1.5));
     points.push_back(PolygonLibrary::Points(5.5, 4.8));
     points.push_back(PolygonLibrary::Points(4.0, 6.2));
     points.push_back(PolygonLibrary::Points(3.2, 4.2));
     points.push_back(PolygonLibrary::Points(1.0, 4.0));

     points[0] = (PolygonLibrary::Points(1.5, 1.0));
     points[1] = (PolygonLibrary::Points(5.6, 1.5));
     points[2] = (PolygonLibrary::Points(5.5, 4.8));
     points[3] = (PolygonLibrary::Points(4.0, 6.2));
     points[4] = (PolygonLibrary::Points(3.2, 4.2));
     points[5] = (PolygonLibrary::Points(1.0, 4.0));

     vector<vector<Points>> pol;
     pol.reserve(4000);
     pol = _mash.CreateMash(points);

           //PRIMO BOX
     EXPECT_EQ(pol[0].size(), 4);
     EXPECT_EQ(pol[0][0].X, 0.0);
     EXPECT_EQ(pol[0][0].Y, 0.0);
     EXPECT_EQ(pol[0][1].X, 4.6);
     EXPECT_EQ(pol[0][1].Y, 0.0);
     EXPECT_EQ(pol[0][2].X, 4.6);
     EXPECT_EQ(pol[0][2].Y, 5.2);
     EXPECT_EQ(pol[0][3].X, 0.0);
     EXPECT_EQ(pol[0][3].Y, 5.2);

     //primo poligono tagliato del box
     EXPECT_EQ(pol[1].size(), 3);
     EXPECT_EQ(pol[1][0].X, 0.5);
     EXPECT_EQ(pol[1][0].Y, 0.0);
     EXPECT_EQ(pol[1][1].X, 4.6);
     EXPECT_EQ(pol[1][1].Y, 0.5);
     EXPECT_EQ(pol[1][2].X, 4.6);
     EXPECT_EQ(pol[1][2].Y, 0.0);

     //secondo poligono tagliato del box
     EXPECT_EQ(pol[2].size(), 4);
     EXPECT_EQ(pol[2][0].X, 4.6);
     EXPECT_EQ(pol[2][0].Y, 0.5);
     EXPECT_EQ(pol[2][1].X, 4.5);
     EXPECT_EQ(pol[2][1].Y, 3.8);
     EXPECT_EQ(pol[2][2].X, 3.0);
     EXPECT_EQ(pol[2][2].Y, 5.2);
     EXPECT_EQ(pol[2][3].X, 4.6);
     EXPECT_EQ(pol[2][3].Y, 5.2);

     //terzo poligono tagliato del box
     EXPECT_EQ(pol[3].size(), 4);
     EXPECT_EQ(pol[3][0].X, 3.0);
     EXPECT_EQ(pol[3][0].Y, 5.2);
     EXPECT_EQ(pol[3][1].X, 2.2);
     EXPECT_EQ(pol[3][1].Y, 3.2);
     EXPECT_EQ(pol[3][2].X, 0.0);
     EXPECT_EQ(pol[3][2].Y, 3.0);
     EXPECT_EQ(pol[3][3].X, 0.0);
     EXPECT_EQ(pol[3][3].Y, 5.2);

     //quarto poligono tagliato del box
     EXPECT_EQ(pol[4].size(), 3);
     EXPECT_EQ(pol[4][0].X, 0.0);
     EXPECT_EQ(pol[4][0].Y, 3.0);
     EXPECT_EQ(pol[4][1].X, 0.5);
     EXPECT_EQ(pol[4][1].Y, 0.0);
     EXPECT_EQ(pol[4][2].X, 0.0);
     EXPECT_EQ(pol[4][2].Y, 3.0);


     //quindi poligono tagliato del box
     EXPECT_EQ(pol[5].size(), 6);
     EXPECT_EQ(pol[5][0].X, 0.5);
     EXPECT_EQ(pol[5][0].Y, 0.0);
     EXPECT_EQ(pol[5][1].X, 4.6);
     EXPECT_EQ(pol[5][1].Y, 0.5);
     EXPECT_EQ(pol[5][2].X, 4.5);
     EXPECT_EQ(pol[5][2].Y, 3.8);
     EXPECT_EQ(pol[5][3].X, 3.0);
     EXPECT_EQ(pol[5][3].Y, 5.2);
     EXPECT_EQ(pol[5][4].X, 2.2);
     EXPECT_EQ(pol[5][4].Y, 3.2);
     EXPECT_EQ(pol[5][5].X, 0.0);
     EXPECT_EQ(pol[5][5].Y, 3.0);


}
}


#endif // __TEST_GEOMETRYCLASS_H
